#include "ConfigParcer.h"
#include <sstream>
#include <string>

// This is an independent project of an individual developer. Dear PVS-Studio,
// please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java:
// http://www.viva64.com

ConfigParcer::ConfigParcer(void) { _IsConfigFileOpen = false; }
/**************************************************************************************************************/
ConfigParcer::ConfigParcer(const char *FileName) {
  _IsConfigFileOpen = false;
  Open(FileName);
}
/**************************************************************************************************************/
bool ConfigParcer::Open(const char *Filename) {

  _IsConfigFileOpen = false;
  _ConfigFileNamel = Filename;
  _File.open(Filename, std::ios::in | std::ios::out);
  if (!_File.fail()) {
    _IsConfigFileOpen = true;
  }

  return _IsConfigFileOpen;
}
/**************************************************************************************************************/
ConfigParcer::~ConfigParcer(void) { _File.close(); }
/**************************************************************************************************************/
void ConfigParcer::SetDelimetrToken(const char *DelimToken) {
  _Delimetr = DelimToken;
}
/**************************************************************************************************************/
template <typename T>
void ConfigParcer::SetTemplateParam(const char *ParamName, T &Value) {

  size_t Curent_Position;
  _File.close();
  Open(_ConfigFileNamel.c_str());
  bool Seted = false;
  do {
    _File >> _Token;
    Curent_Position = static_cast<size_t>(_File.tellg());
    size_t _TID = static_cast<size_t>(_Token.find(_Delimetr));
    if (_TID != -1) {
      string _ParamVa(_Token, 0, _TID);
      string _ValueVa(_Token, _TID + 1, _Token.length());

      if (_ParamVa == ParamName && Seted == false) {
        _File.seekg(Curent_Position - _ValueVa.size());
        _File << Value << endl;
        _File.close();
        Seted = true;
      }
    }

  } while (!Seted || !_File.eof());
}
/**************************************************************************************************************/
template <typename T>
void ConfigParcer::GetTemplateParam(const char *ParamName, T &Value) {

  bool Founded = false;
  size_t Curent_Position;
  stringstream _SS;
  _File.close();
  Open(_ConfigFileNamel.c_str());
  do {
    _File >> _Token;
    Curent_Position = static_cast<size_t>(_File.tellg());

    size_t _TID = static_cast<size_t>(_Token.find(_Delimetr));
    string _ParamVa(_Token, 0, _TID);
    string _ValueVa(_Token, _TID + 1, _Token.length());
    if (_TID != -1) {

      if (_ParamVa == ParamName) {

        _SS << _ValueVa;
        _SS >> Value;
        Founded = true;
      }
    }

  } while (!Founded || !_File.eof());
  _File.close();
}

/**************************************************************************************************************/
template <typename T>
void ConfigParcer::GetTemplateParamHex(const char *ParamName, T &Value) {

  bool Founded = false;
  size_t Curent_Position;
  stringstream _SS;
  _File.close();
  Open(_ConfigFileNamel.c_str());
  do {
    _File >> _Token;
    Curent_Position = static_cast<size_t>(_File.tellg());
    size_t _TID = static_cast<size_t>(_Token.find(_Delimetr));
    string _ParamVa(_Token, 0, _TID);
    string _ValueVa(_Token, _TID + 1, _Token.length());
    if (_TID != -1) {
      if (_ParamVa == ParamName) {
        _SS << _ValueVa << std::hex;
        _SS >> Value;
        Founded = true;
      }
    }

  } while (!Founded || !_File.eof());

  _File.close();
}

/**************************************************************************************************************/
void ConfigParcer::SetIntParam(const char *ParamName, int &Value) {
  SetTemplateParam(ParamName, Value);
}
/**************************************************************************************************************/
void ConfigParcer::SetFloatParam(const char *ParamName, float &Value) {
  SetTemplateParam(ParamName, Value);
}
/**************************************************************************************************************/
void ConfigParcer::SetDoubleParam(const char *ParamName, double &Value) {
  SetTemplateParam(ParamName, Value);
}
/**************************************************************************************************************/
void ConfigParcer::SetBoolParam(const char *ParamName, bool &Value) {
  SetTemplateParam(ParamName, Value);
}
/**************************************************************************************************************/
void ConfigParcer::GetIntParam(const char *ParamName, int &Value) {
  GetTemplateParam(ParamName, Value);
}
/**************************************************************************************************************/
void ConfigParcer::GetFloatParam(const char *ParamName, float &Value) {
  GetTemplateParam(ParamName, Value);
}
/**************************************************************************************************************/
void ConfigParcer::GetDoubleParam(const char *ParamName, double &Value) {
  GetTemplateParam(ParamName, Value);
}
/**************************************************************************************************************/
void ConfigParcer::GetBoolParam(const char *ParamName, bool &Value) {
  GetTemplateParam(ParamName, Value);
}
/**************************************************************************************************************/
void ConfigParcer::Getuint16_tParam(const char *ParamName, uint16_t &Value) {
  GetTemplateParam(ParamName, Value);
}
/**************************************************************************************************************/
bool ConfigParcer::GetIsConfigFileOpen(void) { return _IsConfigFileOpen; }
/**************************************************************************************************************/
void ConfigParcer::GetIntParamHex(const char *ParamName, int &Value) {

  GetTemplateParamHex(ParamName, Value);
}
