#include "Timer_TDI_Impulse.h"
Timer_TDI_Impulse_GPIO _IGPIO;
bool ImpulseEnd;
bool Latch = false;
/******************************************************************************/
void Timer_TDI_Impulse_Init(void) {
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);
  TIM_TimeBaseInitTypeDef _TimerSetup;
  _TimerSetup.TIM_ClockDivision = TIM_CKD_DIV1;
  _TimerSetup.TIM_CounterMode = TIM_CounterMode_Up;
  _TimerSetup.TIM_Period = 0;
  _TimerSetup.TIM_Prescaler = 42000;
  _TimerSetup.TIM_RepetitionCounter = 0;
  TIM_TimeBaseInit(TIM4, &_TimerSetup);
  TIM_ClearITPendingBit(TIM4, TIM_IT_Update);
  TIM_Cmd(TIM4, DISABLE);
  NVIC_InitTypeDef NVIC_InitStructure;
  // NVIC_PriorityGroupConfig (NVIC_PriorityGroup_1);
  NVIC_InitStructure.NVIC_IRQChannel = TIM4_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0; // 1;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);

  Timer_TDI_Impulse_InitGPIODefault();
}
/******************************************************************************/
void Timer_TDI_Impulse_StartImpulse(uint32_t Time) {
  if (!Timer_TDI_Impulse_GetLatch()) {
    GPIO_ResetBits(GPIOA, GPIO_Pin_12);
    Timer_TDI_Impulse_SetLatch();
    TIM_SetAutoreload(TIM4, Time);
    ImpulseEnd = false;
    TIM_ClearITPendingBit(TIM4, TIM_IT_Update);
    TIM_ITConfig(TIM4, TIM_IT_Update, ENABLE);
    NVIC_EnableIRQ(TIM4_IRQn);

    TIM_Cmd(TIM4, ENABLE);
  }
}
/******************************************************************************/
bool Timer_TDI_Impulse_ImpulseRnded(void) { return ImpulseEnd; }
/******************************************************************************/
void TIM4_IRQHandler(void) {
  if (TIM_GetITStatus(TIM4, TIM_IT_Update) != RESET) {
    TIM_ClearITPendingBit(TIM4, TIM_IT_Update);
    ImpulseEnd = true;
    GPIO_SetBits(GPIOA, GPIO_Pin_12);
    TIM_Cmd(TIM4, DISABLE);
    TIM_SetCounter(TIM4, 0);
  };
}
/******************************************************************************/
void Timer_TDI_Impulse_InitGPIO(Timer_CCD_Impulse_GPIO GPIO) {
  RCC_AHB1PeriphClockCmd(GPIO.Clock_pin, ENABLE);
  GPIO_InitTypeDef _GPIO;
  GPIO_StructInit(&_GPIO);
  _GPIO.GPIO_Mode = GPIO_Mode_OUT;
  _GPIO.GPIO_Pin = GPIO.Pin;
  _GPIO.GPIO_PuPd = GPIO_PuPd_DOWN;
  _GPIO.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(GPIO.Port, &_GPIO);
  _IGPIO = GPIO;
}
/******************************************************************************/
void Timer_TDI_Impulse_InitGPIODefault(void) {
  Timer_TDI_Impulse_GPIO _GPIO;
  _GPIO.Clock_pin = RCC_AHB1Periph_GPIOA;
  _GPIO.Pin = GPIO_Pin_12;
  _GPIO.Port = GPIOA;
  Timer_TDI_Impulse_InitGPIO(_GPIO);
  GPIO_SetBits(_IGPIO.Port, _IGPIO.Pin);
}
/******************************************************************************/
inline void Timer_TDI_Impulse_SetLatch(void) {
  if (!Latch) {
    Latch = true;
  }
}
/******************************************************************************/
void Timer_TDI_Impulse_ResetLatch(void) {
  if (Latch) {
    Latch = false;
  }
}
/******************************************************************************/
bool Timer_TDI_Impulse_GetLatch(void) { return Latch; }
/******************************************************************************/
