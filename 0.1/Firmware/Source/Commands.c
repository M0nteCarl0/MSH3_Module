
#include "Commands.h"
#include "Encoder.h"
#include "Indep_WD.h"
#include "KYInput.h"
#include "LED.h"
#include "MSH_Core.h"
#include "MySPIN.h"
#include "Sensors.h"
#include "SteperConversion.h"
#include "Timer.h"
const unsigned char Name[] = {'M', 'S', 0, 0};
const unsigned char VersSW[2] = {1, 0};
const unsigned char VersHW[2] = {3, 5};
static uint8_t Buff[NCOMD];
uint8_t BuffS[30];
static int Ret = 0;
static int N = 0;
static int StepsToWrite = 0;
static uint8_t StateWR = 0;
uint32_t TimeToSendAns = 0; 
uint16_t Command_MakeCRC16(uint8_t *Block, int Len) {
  uint16_t crc = 0xFFFF;
  int i;
  while (Len--) {
    crc ^= *Block++ << 8;
    for (i = 0; i < 8; i++)
      crc = crc & 0x8000 ? (crc << 1) ^ 0x1021 : crc << 1;
  }
  crc &= 0x7F7F; //  !!! 16, 8-bits Set NULL
  return crc;
};

void Command_ReadModuleNumber(uint8_t *Buff, int N) {
  if (Buff[3] != 5)
    return;
  __disable_irq();
  N = Buff[3] = 39;
  __enable_irq();
  int Ret = SendCommand(Buff, N);
  EnableRS485();
}

void Command_ReadVersion(uint8_t *Buff, int N) {
  if (Buff[3] != 5)
    return;
  __disable_irq();
  uint8_t SB = 0x01;
  Buff[7] = Buff[8] = 0;
  Buff[5] = Name[0];
  if (Name[0] & 0x80)
    Buff[8] |= SB;
  SB = SB << 1;
  Buff[6] = Name[1];
  if (Name[1] & 0x80)
    Buff[8] |= SB;
  Buff[9] = VersHW[0];
  Buff[10] = VersHW[1];
  Buff[11] = VersSW[0];
  Buff[12] = VersSW[1];
  N = Buff[3] = 14;
  __enable_irq();
  int Ret = SendCommand(Buff, N);
  EnableRS485();
}

void Command_WriteSetupDriver(uint8_t *Buff, int N) {
  if (Buff[3] != 77)
    return;
  __disable_irq();
  cSPIN_RegsStruct_TypeDef cSPIN;
  ConertcSPIN_ByteToRegsStruct_TypeDef(&cSPIN, &Buff[5]);
  MSH_Core_SetLookAtSensor(cSPIN.LookAtSwitch);
  cSPIN_Registers_Set(&cSPIN);
  MSH_Core_WriteSetupInFlash(cSPIN);
  __enable_irq();
}

void Command_DriveMotion(uint8_t *Buff, int N) {
  if (Buff[3] != 10)
    return;
  __disable_irq();
  uint32_t _Steps = 0;
  ByteToDword(&_Steps, &Buff[5], &Buff[6], &Buff[7]);
  uint8_t Dir = Buff[8];
  cSPIN_Move((cSPIN_Direction_TypeDef)Dir, _Steps);
  __enable_irq();
}

void Command_ReadSetupDriver(uint8_t *Buff, int N) {
  if (Buff[3] != 5)
    return;
  __disable_irq();
  cSPIN_RegsStruct_TypeDef cSPIN;
  cSPIN.LookAtSwitch = MSH_Core_GetLookAtSensor();
  MSH_Core_ReadSetupFromFlash(cSPIN);
  ConertcSPIN_RegsStruct_TypeDefToByte(&cSPIN, &Buff[5]);
  __enable_irq();
  N = Buff[3] = 77;
  int Ret = SendCommand(Buff, N);
  EnableRS485();
}

void Command_RebootDevice(uint8_t *Buff, int N) {
  if (Buff[3] != 5)
    return;
  NVIC_SystemReset();
}

void Command_ProcessCommands(void) {
  do {
     LED_ModuleHeartBeat();
     iWD_RefreshCountet();
    Ret = ReceiveCommand(Buff, &N);
    if (Ret) {
      if (N > 2) {
       Command_CommandParser(Buff, Buff[2], N);
      }
      if (N == 2) {
        Command_TwoCommandParser(Buff, N);
      }
    }
      MSH_Core_HighVoltageOffLogic();
      MSH_Core_ControlLogic();
  } while (1);
}

void ByteToDword(uint32_t &Destination, uint8_t &Source1, uint8_t &Source2,
                 uint8_t &Source3) {
  Destination = ((uint32_t)Source1 & 0x7F) | (((uint32_t)Source2 & 0x7F) << 7) |
                (((uint32_t)Source3 & 0x7F) << 14);
};

void DwordToByte(uint32_t &Source, uint8_t &B1, uint8_t &B2, uint8_t &B3) {

  B1 = Source & 0x7f;
  B2 = Source >> 7 & 0x7f;
  B3 = Source >> 14 & 0x7f;
}

void Command_CommandParser(uint8_t *Buff, uint8_t ComandID, int N) {
  switch (ComandID) {
  case CommadID_READ_HW_SW: {
    Command_ReadVersion(Buff, N);
    break;
  }
  case CommadID_WRITE_DRIVER_SETUP: {
    Command_WriteSetupDriver(Buff, N);
    break;
  }
  case CommadID_REBOOT_MODULE: {
    Command_RebootDevice(Buff, N);
    break;
  }
  case CommadID_DriveSteps: {
    Command_DriveMotion(Buff, N);
    break;
  }
  case CommadID_READ_DRIVER_SETUP: {
    Command_ReadSetupDriver(Buff, N);
    break;
  }
  case CommadID_READ_DRIVER_ABS_POS: {
    break;
  }
  case CommadID_READ_EVENT_TIMES: {
    break;
  }
  };
};

void Command_TwoCommandParser(uint8_t *Buff, int N) {

  switch (Buff[0]) {
  case CommadID_Two_Byte_TERMINAL: {
    __disable_irq();
    MSH_CoreSetTerminal(Buff[1]);
    __enable_irq();
    break;
  }
  case CommadID_Two_Byte_Status: {
    __disable_irq();
    MSH_CoreSetState(Buff[1]);
    MSH_Core_DriveCommands(Buff[1]);
    __enable_irq();
  
    break;
  }
  case CommadID_Two_Byte_StepsHi: {
    __disable_irq();
    MSH_Core_SetStepLimiterHigh(Buff[1]);
    __enable_irq();
    break;
  }
  case CommadID_Two_Byte_StepsLo: {
    __disable_irq();
    MSH_Core_SetStepLimiterLow(Buff[1]);
    __enable_irq();
    break;
  }
  case CommadID_Two_Byte_RampHi: {
    __disable_irq();
    MSH_CoreSetRampHigh(Buff[1]);
    __enable_irq();
    break;
  }
  case CommadID_Two_Byte_RampLo: {
    __disable_irq();
    MSH_CoreSetRampLow(Buff[1]);
    __enable_irq();
    break;
  }
  case CommadID_Two_Byte_CounterHi: {
    __disable_irq();
    MSH_CoreSetCounterHi(Buff[1]);
    __enable_irq();
    break;
  }
  case CommadID_Two_Byte_CounterLo: {
    __disable_irq();
    MSH_CoreSetCounterLo(Buff[1]);
    __enable_irq();
    break;
  }
  case CommadID_Two_Byte_TimeDelayLo: {
    __disable_irq();
    MSH_Core_SetDellayAcc(Buff[1]);
    __enable_irq();
    break;
  }
  case CommadID_Two_Byte_VelocityHi: {
    __disable_irq();
    MSH_Core_SetVelocityHigh(Buff[1]);
    __enable_irq();
    break;
  }
  case CommadID_Two_Byte_VelocityLo: {
    __disable_irq();
    MSH_Core_SetVelocityLow(Buff[1]);
    __enable_irq();
    break; 
  }
  case CommadID_Two_Byte_InitialVLo: {
    __disable_irq();
    MSH_CoreSetVelocityInit(Buff[1]);
    __enable_irq();
    break;
  }
  case CommadID_Two_Byte_TurnOffHVLo: {
    __disable_irq();
    MSH_SetTurnOffHVLo(Buff[1]);
    __enable_irq();
    break;
  }
  case CommadID_Two_Byte_READ: {
    __disable_irq();
    Command_TwoBytesRead(Buff);
    __enable_irq();
  } break;
  }
}

inline void Command_MakeCRC_LegacyProtocol(uint8_t *Buff, int N) {
  if (Buff == (void *)0 || N == 0) {
    return;
  }
  uint8_t SummaryBytes = N;
  uint8_t ItMax = SummaryBytes - 2;
  int SCRC = 0;
  uint8_t Csr[2] = {0};
  for (uint8_t i = 0; i < ItMax; i++) {
    SCRC += Buff[i] & 0xFF;
  }
  Csr[0] = SCRC & 0xFF;
  Csr[1] = (SCRC >> 8) & 0xFF;
  Buff[SummaryBytes - 2] = Csr[0];
  Buff[SummaryBytes - 1] = Csr[1];
}

void Command_TwoBytesRead(uint8_t *Buff) { 
  int DeviceAdress = MSH_Core_GetDeviceAddres();
  if (Buff[1] == MSH_Core_GetDeviceAddres()) {
    __disable_irq();
    short StepsConv = Encoder_GetStepsCount();
    StepsToWrite = StepsConv;
    uint8_t StepL = StepsConv & 0x7f;
    uint8_t StepH = (StepsConv >> 7) & 0x7f;
    if (StepsConv < 0) {
      MSH_Core_SetNegativeSign();
    }
    if (StepsConv >= 0) {
      MSH_Core_SetPositiveign();
    }
    MSH_CoreSetCounterLo(StepL);
    MSH_CoreSetCounterHi(StepH);
    Buff[0] = MSH_Core_GetDeviceAddres(); // CommadID_Two_Byte_READ;
    Buff[1] = 0x1E;
    // Buff[1];
    Buff[2] = CommadID_Two_Byte_Status;
    Buff[3] = MSH_CoreGetState();
    Buff[4] = CommadID_Two_Byte_TERMINAL;
    Buff[5] = MSH_CoreGetTerminal();
    Buff[6] = CommadID_Two_Byte_StepsHi;
    Buff[7] = MSH_Core_GetStepLimiterHigh();
    Buff[8] = CommadID_Two_Byte_StepsLo;
    Buff[9] = MSH_Core_GetStepLimiterLow();
    Buff[10] = CommadID_Two_Byte_VelocityHi;
    Buff[11] = MSH_Core_GetVelocityHigh();
    Buff[12] = CommadID_Two_Byte_VelocityLo;
    Buff[13] = MSH_Core_GetVelocityLow();
    Buff[14] = CommadID_Two_Byte_RampHi;
    Buff[15] = MSH_CoreGetRampHigh();
    Buff[16] = CommadID_Two_Byte_RampLo;
    Buff[17] = MSH_CoreGetRampLow();
    Buff[18] = CommadID_Two_Byte_CounterHi;
    Buff[19] = MSH_CoreGetCounterHi();
    Buff[20] = CommadID_Two_Byte_CounterLo;
    Buff[21] = MSH_CoreGetCounterLo();
    Buff[22] = CommadID_Two_Byte_InitialVLo;
    Buff[23] = MSH_CoreGetVelocityInit();
    Buff[24] = CommadID_Two_Byte_TimeDelayLo;
    Buff[25] = MSH_Core_GetDellayAcc();
    Buff[26] = CommadID_Two_Byte_TurnOffHVLo;
    Buff[27] = MSH_GetTurnOffHVLo();    
    Command_MakeCRC_LegacyProtocol(Buff, 30);
    SendCommandLegacy(Buff, 30);
    Timer_TraceEnd();
    __enable_irq();
    EnableRS485();
    }
}

void Command_IntToByte(int *Source, uint8_t *B1, uint8_t *B2, uint8_t *B3,
                       uint8_t *B4, uint8_t *B5) {
  *B1 = *Source & 0x7f;
  *B2 = (*Source >> 7) & 0x7f;
  *B3 = (*Source >> 14) & 0x7f;
  *B4 = (*Source >> 21) & 0x7f;
  *B5 = (*Source >> 28) & 0x7f;
}

void Command_ByteToInt(int *Dest, uint8_t *B1, uint8_t *B2, uint8_t *B3,
                       uint8_t *B4, uint8_t *B5) {
  *Dest = *B1 | *B2 << 7 | *B3 << 14 | *B4 << 21 | *B5 << 28;
}
