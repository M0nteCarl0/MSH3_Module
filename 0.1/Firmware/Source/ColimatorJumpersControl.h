#include "stm32f4xx.h"
#pragma once
typedef enum ColimatorJumpersSState
{
  ColimatorJumpersState_MainDirection,
  ColimatorJumpersState_ReveceDirection,
}ColimatorJumpersSState;

void ColimatorJumpersControl_Init(void);
ColimatorJumpersSState ColimatorJumpersControl_GetJumperState(void);