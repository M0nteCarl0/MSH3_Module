#include "stm32f4xx.h"
#pragma once
#ifdef __cplusplus
 extern "C" {
 #endif   
typedef  struct  Timer_TDI_Impulse_GPIO
{
uint32_t Clock_pin;
GPIO_TypeDef* Port;
uint32_t Pin;  
}Timer_CCD_Impulse_GPIO;

void Timer_TDI_Impulse_Init(void);
void Timer_TDI_Impulse_InitGPIO(Timer_TDI_Impulse_GPIO GPIO);
void Timer_TDI_Impulse_InitGPIODefault (void);
void Timer_TDI_Impulse_StartImpulse( uint32_t Time);

void Timer_TDI_Impulse_SetLatch(void);
void Timer_TDI_Impulse_ResetLatch(void);
bool Timer_TDI_Impulse_GetLatch(void);
bool Timer_TDI_Impulse_ImpulseRnded(void);

void TIM4_IRQHandler(void);

#ifdef __cplusplus
 }
 #endif   