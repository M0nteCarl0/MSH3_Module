#include "stm32f4xx.h"
#include "Sensors.h"
#include "MySPIN.h"
#ifdef __cplusplus
 extern "C" {
 #endif   

typedef struct MSH_Core_Params_StateBF
{
  uint8_t DeviceType:1;
  uint8_t CCD_Mode_TDI:1;
  uint8_t CCD_Mode_CCD:1;
  uint8_t External_Trigger:1;
  uint8_t SyncMotionStepers:1;
  uint8_t DriveEndFrame:1;
  uint8_t DriveBeginFrame:1;
  uint8_t Resrved:1; 
}MSH_Core_Params_StateBF;


typedef struct MSH_Core_Params_MotionWordBF
{
  uint8_t DriveEndFrame:1;
  uint8_t DriveBeginFrame:1;
}MSH_Core_Params_MotionWordBF;

typedef union MSH_Core_Params_MotionWordBU
{
  MSH_Core_Params_MotionWordBF _Bits;
  uint8_t Raw;
}MSH_Core_Params_MotionWordBU;
 


enum   MSH_Core_DeviceAdress
{
  MSH_Core_DeviceAdress_Collimator = 0xd7,
  MSH_Core_DeviceAdress_Scanner    = 0xd6
  
};
 
typedef union MSH_Core_Params_StateBU
{
  MSH_Core_Params_StateBF _Bits;
  uint8_t Raw;
}MSH_Core_Params_StateBU;
 

typedef struct MSH_Core_ParckingParametrs
{
  float SpeedBegin;
  float SpeedEnd;
  int TimeAcceleration;
  int TimeBrake;
  int StepCount;
  int Direction;
  
}MSH_Core_ParckingParametrs;

typedef struct MSH_Core_Params
{
  uint16_t Velocity;
  uint8_t VelocityInit;
  uint8_t VelocityLow;
  uint8_t VelocityHigh;
  uint8_t RampHigh;
  uint8_t RampLow;  
  uint16_t TimeAcc;
  uint8_t TimeAccLow;
  uint8_t TimeAccLHigh;
  uint16_t DellayAcc;
  uint16_t CounterCoordinate;
  uint16_t StepLimiter;
  uint8_t StepLimiterLow;
  uint8_t StepLimiterHigh;
  uint8_t TimeOnBN;
  uint8_t TimeDellayBN;
  uint8_t  SensorsState;
  uint8_t  TurnOffHVLo;
  uint8_t  CounterLo;
  uint8_t  CounterHi;
  uint8_t  State;
  uint8_t  Terminal;
}MSH_Core_Params;
/**********************************************************/
enum  MSH_Core_TerminalState
{
  MSH_Core_TerminalState_TERM_ON          = 0x1,//01
  MSH_Core_TerminalState_TERM_BEG         = 0x2,//10
  MSH_Core_TerminalState_TERM_END         = 0x4,//100
  MSH_Core_TerminalState_NEGATIVE_COUNTER = 0x10,//1000
  MSH_Core_TerminalState_POSITIVE_COUNTER = 0x20,//10000
  MSH_Core_TerminalState_ERROR_COUNTER    = 0x30,//110000
  
};
/**********************************************************/
typedef struct TerminalS
{
  uint8_t  Terminal_ON:1;//0
  uint8_t  Terminal_BEG:1;//1
  uint8_t  Terminal_END:1;//2
  uint8_t  Terminal_RESERV0:1;//3
  uint8_t  Terminal_CNT:2;//4..5
  uint8_t  Terminal_RESERVED1:2;//6..7  
}TerminalS;
/**********************************************************/
typedef union Terminalu
{
  TerminalS _Bits;
  uint8_t  _Raw;
  
}Terminalu;
/**********************************************************/
enum  MSH_Core_mode_module
{
  MSH_Core_mode_module_Scaner     = 0x0,
  MSH_Core_mode_module_Collimator = 0x1,
};
/**********************************************************/

 typedef enum  MSH_Core_module_ID_Number
{
  MSH_Core_module_ID_Number_Scaner     = 0xA,
  MSH_Core_module_ID_Number_Collimator = 0xB,
}MSH_Core_module_ID_Number;
/**********************************************************/



void        MSH_Core_HighVoltageOffLogic(void);
void        MSH_Core_PollingSensosrs(void);
void        MSH_Core_PollingHVImpulse(void);

void        MSH_Core_SetVelocityLow(uint8_t Low);
void        MSH_Core_SetVelocityHigh(uint8_t High);
void        MSH_SetTurnOffHVLo(uint8_t Low);
void        MSH_Core_SetTimeAccLow(uint8_t Low);
void        MSH_Core_SetTimeAccHigh(uint8_t High);
void        MSH_Core_SetDellayAcc( uint8_t DellayAcc);
void        MSH_Core_SetStepLimiterLow(uint8_t Low);
void        MSH_Core_SetStepLimiterHigh(uint8_t High);
void        MSH_CoreSetState(uint8_t State);
void        MSH_CoreSetRampHigh(uint8_t High);
void        MSH_CoreSetRampLow( uint8_t Low);
void        MSH_CoreSetVelocityInit(uint8_t InitVel);
void        MSH_CoreSetCounterLo(uint8_t Lo);
void        MSH_CoreSetCounterHi(uint8_t Hi);
void        MSH_CoreSetTimeOnBN( uint8_t  TimeOnBN);
void        MSH_CoreSetTerminal(uint8_t Term);

void        MSH_Core_Init(void);
void        MSH_Core_ControlLogic(void);

void        MSH_Core_ControlTerminaleDriveState(void);
void        MSH_Core_ControlTerminaleBeginFramState(void);
void        MSH_Core_ControlTerminaleEndFramState(void);

uint8_t     MSH_Core_GetDellayAcc(void);
uint8_t     MSH_Core_GetStepLimiterLow(void);
uint8_t     MSH_Core_GetStepLimiterHigh(void);
uint8_t     MSH_CoreGetVelocityInit(void);
uint8_t     MSH_CoreGetCounterLo(void);
uint8_t     MSH_CoreGetCounterHi(void);
uint8_t     MSH_CoreGetTerminal(void);
uint8_t     MSH_CoreGetRampHigh(void);
uint8_t     MSH_CoreGetRampLow(void); 
uint8_t     MSH_GetTurnOffHVLo(void);
uint8_t     MSH_CoreGetSensorsState(void);
uint8_t     MSH_CoreGetState(void);

uint8_t     MSH_Core_GetVelocityLow(void);
uint8_t     MSH_Core_GetVelocityHigh(void);

uint16_t    MSH_Core_GetVelocityValue(void);
uint16_t    MSH_Core_GetStepsValue(void);
float       MSH_Core_ComputeAcceleraion(void);
void        MSH_Core_WriteComputedAcceleraion(void);

void        MSH_Core_PreepareForRun(void);
bool        MSH_Core_HVsSignalCaptured(void);

void        MSH_Core_DriveCommands(uint8_t StateWordW);
void        MSH_Core_CheckInitilaSensor(void);
void        MSH_Core_ExecutDellayHV(void);
void        MSH_Core_TurnOffHV(void);
void        MSH_Core_TurnOnHV(void);
void        MSH_Core_WriteInitialAndTargetSpeeds(void);
bool        MSH_Core_EncoderIsTrigered(void);
void        MSH_Core_DriveSteperEngine(void);
void        MSH_Core_ResetEncoderStepsEvent(void);
void        MSH_Core_SensorTriggerEvent(SensorWord  Sense);
void        MSH_Core_PrepareModule(void);
void        MSH_Core_WriteSetupInFlash(cSPIN_RegsStruct_TypeDef& cSPIN);
bool        MSH_Core_ReadSetupFromFlash(cSPIN_RegsStruct_TypeDef& cSPIN);

void        MSH_Core_SetTerminalState_TERM_ON(void);
void        MSH_Core_SetTerminalState_TERM_BEG(void);
void        MSH_Core_SetTerminalState_TERM_END(void);


void        MSH_Core_ResetTerminalState_TERM_ON(void);
void        MSH_Core_ResetTerminalState_TERM_BEG(void);
void        MSH_Core_ResetTerminalState_TERM_END(void);
void        MSH_Core_CaptureFrameExternalTrigger(void);

void        MSH_Core_ControlTerminaleDriveStateStoped(void);
void        MSH_Core_DellayDriveTimerInit(void);
void        MSH_Core_DellayDriveEvent(void);
void        MSH_Core_DellayDriveEventAction(void);
void        MSH_Core_SetNegativeSign(void);
void        MSH_Core_SetPositiveign(void);


void        MSH_Core_SetLookAtSensor(uint8_t  LookAt);
uint8_t     MSH_Core_GetLookAtSensor(void);
void        EXTI9_5_IRQHandler(void);
void        TIM7_IRQHandler(void);
void        MSH_Core_ResetCnt(void);

MSH_Core_mode_module MSH_Core_GetModeModule(void);
void MSH_Core_CheckModePins(void);
void MSH_Core_ModluleModeSelectorInit(void);
//void MSH_Core_SoftStopByCalcualtedPosition(void);
void TIM3_IRQHandler(void);
void MSH_Core_ExternalTriggerEvent(void);
void MSH_Core_Init_Polling_Timer(void);
void MSH_Core_Drive_TDI_Mode(void);
MSH_Core_DeviceAdress MSH_Core_GetDeviceAddres(void);

void MSH_Core_EmeragancyStopMotor(void);
void MSH_Core_ModuleParking(void);
void  MSH_Core_ModuleParkingBeginFrame(void);
void  MSH_Core_ModuleParkingEndFrame(void);
void MSH_Core_MotionToSensor(MSH_Core_ParckingParametrs* Params);
void MSH_Core_SetupParkingSpeed(void);
void MSH_Core_MotionControl(void);
void MSH_Core_ResetEncoderStepsEventMotionFromBackward(void);
int CheckModeBits(uint8_t Control);
void MSH_Core_CheckColimatorJumpers(void);
#ifdef __cplusplus
 }
 #endif   
