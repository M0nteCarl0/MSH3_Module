#pragma once
#include <stdint.h>
#define   NST         1024
#ifndef __MICROSTACK
#define __MICROSTACK
typedef struct
{
  uint8_t  Data[NST];
  int       Begin;
  int       End;
  int       Flg;
} MyStack;
#ifdef __cplusplus
extern "C" {
#endif
void  InitStack (MyStack *Stack);
int   PutCharStack (uint8_t Data, MyStack *Stack);
int   GetCharStack (uint8_t *Data, MyStack *Stack);
int   GetRecivedBytesCount(void);
void  ResetRecivedBytesCount(void);
void ClearCharStack(MyStack *Stack);
int GetPositionFirstByte(MyStack *Stack);
int GetPositionOffsetFirstByte(MyStack *Stack, uint8_t Offset);
void  AllocateStackBuffer(MyStack *Stack,uint16_t Size);
#ifdef __cplusplus
}
#endif


#endif