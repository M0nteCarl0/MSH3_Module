#include "stm32f4xx.h"
#ifdef __cplusplus
 extern "C" {
 #endif   
typedef enum Mode_selector_Mode
{
  Mode_selector_Mode_TDI = 0x2,
  Mode_selector_Mode_CCD = 0x4,
}Mode_selector_Mode;

typedef struct Mode_selector_GPIO_asigment
{
uint32_t Clock_pin;
GPIO_TypeDef* Port;
uint32_t PinTDI;
uint32_t PinCCD;
}Mode_selector_GPIO_asigment;

void  Mode_selector_SetMode( Mode_selector_Mode Mode);
void  Mode_selector_GPIO_Init( Mode_selector_GPIO_asigment* Assigment);
void  Mode_selector_Init(void);
Mode_selector_Mode Mode_selector_GetMode(void); 
#ifdef __cplusplus
 }
 #endif   