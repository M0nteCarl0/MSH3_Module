#include "Encoder.h"
#include "ByteConversion.h"
#include "MSH_Core.h"
#include <math.h>
static int ItCount = 0;
static int ItCountA = 0;
static int ItCountB = 0;
static int ItCountZ = 0;
Encoder_Mode EncMode;
Encoder_InternalRegisters InternalRegs;
using namespace Conversion;
/******************************************************************************/
void Encoder_Init(Encoder_InitTypedef *Encoder_InitType) {
  GPIO_InitTypeDef _EncoderPinout;
  if (Encoder_InitType->ReadMode == Encoder_ReadMode_HW) {
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM8, ENABLE);
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
    TIM_TimeBaseInitTypeDef _EncoderTimer;
    _EncoderTimer.TIM_ClockDivision = TIM_CKD_DIV1;
    _EncoderTimer.TIM_CounterMode = TIM_CounterMode_Up | TIM_CounterMode_Down;
    _EncoderTimer.TIM_Period = 1;
    _EncoderTimer.TIM_Prescaler = 4200;
    _EncoderTimer.TIM_RepetitionCounter = 0;
    _EncoderPinout.GPIO_Mode = GPIO_Mode_AF;
    _EncoderPinout.GPIO_OType = GPIO_OType_PP;
    _EncoderPinout.GPIO_Pin =
        GPIO_Pin_7 |
        GPIO_Pin_8; // Encoder_InitType->Pin_A|Encoder_InitType->Pin_B;
    _EncoderPinout.GPIO_PuPd = GPIO_PuPd_DOWN;
    _EncoderPinout.GPIO_Speed = GPIO_Speed_2MHz;
    GPIO_PinAFConfig(GPIOC, GPIO_AF_TIM8, GPIO_PinSource7);
    GPIO_PinAFConfig(GPIOC, GPIO_AF_TIM8, GPIO_PinSource8);
    TIM_EncoderInterfaceConfig(TIM8, TIM_EncoderMode_TI1,
                               TIM_ICPolarity_Falling, TIM_ICPolarity_Falling);

    GPIO_Init(GPIOC, &_EncoderPinout);
    TIM_TimeBaseInit(TIM8, &_EncoderTimer);
    TIM_Cmd(TIM8, ENABLE);
    TIM_SetCounter(TIM8, 0);
  } else {
    RCC_APB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
    RCC_APB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
    EXTI_InitTypeDef _EXTIPINs;
    NVIC_InitTypeDef _EXTIIRQ;
    _EncoderPinout.GPIO_Mode = GPIO_Mode_IN;
    _EncoderPinout.GPIO_Pin =
        GPIO_Pin_15; // Encoder_InitType->Pin_A|Encoder_InitType->Pin_B;
    _EncoderPinout.GPIO_PuPd = GPIO_PuPd_NOPULL; // GPIO_PuPd_NOPULL;
    _EncoderPinout.GPIO_Speed = GPIO_Speed_2MHz;
    SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOB, EXTI_PinSource15);
    GPIO_Init(GPIOB, &_EncoderPinout);
    _EncoderPinout.GPIO_Mode = GPIO_Mode_IN;
    _EncoderPinout.GPIO_Pin =
        GPIO_Pin_7 |
        GPIO_Pin_9; // Encoder_InitType->Pin_A|Encoder_InitType->Pin_B;
    _EncoderPinout.GPIO_PuPd = GPIO_PuPd_NOPULL; // GPIO_PuPd_NOPULL;
    _EncoderPinout.GPIO_Speed = GPIO_Speed_2MHz;
    GPIO_Init(GPIOC, &_EncoderPinout);
    SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOC, EXTI_PinSource7);
    SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOC, EXTI_PinSource8);
    SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOC, EXTI_PinSource9);
    _EXTIPINs.EXTI_Mode = EXTI_Mode_Interrupt;
    _EXTIPINs.EXTI_Line = EXTI_Line7; // Reference
    _EXTIPINs.EXTI_Trigger = EXTI_Trigger_Rising;
    _EXTIPINs.EXTI_LineCmd = ENABLE;
    EXTI_Init(&_EXTIPINs);
    _EXTIPINs.EXTI_Mode = EXTI_Mode_Interrupt;
    _EXTIPINs.EXTI_Line = EXTI_Line9; //
    _EXTIPINs.EXTI_Trigger = EXTI_Trigger_Rising_Falling;
    _EXTIPINs.EXTI_LineCmd = ENABLE;
    EXTI_Init(&_EXTIPINs);
    _EXTIPINs.EXTI_Mode = EXTI_Mode_Interrupt;
    _EXTIPINs.EXTI_Line = EXTI_Line15;
    _EXTIPINs.EXTI_Trigger = EXTI_Trigger_Rising_Falling;
    _EXTIPINs.EXTI_LineCmd = ENABLE;
    EXTI_Init(&_EXTIPINs);
    _EXTIIRQ.NVIC_IRQChannel = EXTI9_5_IRQn;
    _EXTIIRQ.NVIC_IRQChannelCmd = ENABLE;
    _EXTIIRQ.NVIC_IRQChannelPreemptionPriority = 1;
    _EXTIIRQ.NVIC_IRQChannelSubPriority = 1;
    NVIC_Init(&_EXTIIRQ);
    _EXTIIRQ.NVIC_IRQChannel = EXTI15_10_IRQn;
    _EXTIIRQ.NVIC_IRQChannelCmd = ENABLE;
    _EXTIIRQ.NVIC_IRQChannelPreemptionPriority = 1;
    _EXTIIRQ.NVIC_IRQChannelSubPriority = 1;
    NVIC_Init(&_EXTIIRQ);
  }
  InternalRegs.ReferenceEdgeControl = 0x1;
  Encoder_CheckFiniteState();
}

int Encoder_GetSteps(void) {
  int Steps = 0;
  if (InternalRegs.Pins->ReadMode == Encoder_ReadMode_HW) {
    Steps = TIM_GetCounter(TIM8);
    InternalRegs.Steps = Steps;
  }
  if (InternalRegs.Pins->ReadMode == Encoder_ReadMode_EXTI_Steps) {
    Steps = InternalRegs.Steps;
  }
  if (InternalRegs.Pins->ReadMode == Encoder_ReadMode_EXTI_Ticks) {
    Steps = ItCountZ;
  }
  return Steps;
}

Encoder_Direction Encoder_GetDirection(void) { return InternalRegs.Direction; }

inline uint32_t Encoder_ReadPins(void) {
  InternalRegs.LState.Bits.Pin_A =
      GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_9); // int0
  InternalRegs.LState.Bits.Pin_B =
      GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_15); // imp bb
  InternalRegs.LState.Bits.Reserved = 0;
  return InternalRegs.LState.Raw;
}

void Encoder_CheckFiniteState(void) {
  InternalRegs.State.CurrentState = Encoder_ReadPins();
  if(EncMode == Encoder_Mode_Normal)
  {
  switch (InternalRegs.State.CurrentState) {
  case Encoder_FiniteMachine_stetes_1: {
    if (InternalRegs.State.LastState == Encoder_FiniteMachine_stetes_2) {
      InternalRegs.Steps--;
      InternalRegs.Direction = Encoder_Direction_CWW;
    }
    break;
  }
  case Encoder_FiniteMachine_stetes_2: {
    if (InternalRegs.State.LastState == Encoder_FiniteMachine_stetes_1) {
      InternalRegs.Steps++;
      InternalRegs.Direction = Encoder_Direction_CW;
    }
    break;
  };
  case Encoder_FiniteMachine_stetes_3: {
    if (InternalRegs.State.LastState == Encoder_FiniteMachine_stetes_4) {
      InternalRegs.Steps--;
      InternalRegs.Direction = Encoder_Direction_CWW;
    }
    break;
  }
  case Encoder_FiniteMachine_stetes_4: {
    if (InternalRegs.State.LastState == Encoder_FiniteMachine_stetes_3) {
      InternalRegs.Steps++;
      InternalRegs.Direction = Encoder_Direction_CW;
    }
    break;
  }
  };
  }else
  {
    
     switch (InternalRegs.State.CurrentState) {
  case Encoder_FiniteMachine_stetes_1: {
    if (InternalRegs.State.LastState == Encoder_FiniteMachine_stetes_2) {
      InternalRegs.Steps++;
      InternalRegs.Direction = Encoder_Direction_CW;
    }
    break;
  }
  case Encoder_FiniteMachine_stetes_2: {
    if (InternalRegs.State.LastState == Encoder_FiniteMachine_stetes_1) {
      InternalRegs.Steps--;
      InternalRegs.Direction = Encoder_Direction_CWW;
    }
    break;
  };
  case Encoder_FiniteMachine_stetes_3: {
    if (InternalRegs.State.LastState == Encoder_FiniteMachine_stetes_4) {
      InternalRegs.Steps++;
      InternalRegs.Direction = Encoder_Direction_CW;
    }
    break;
  }
  case Encoder_FiniteMachine_stetes_4: {
    if (InternalRegs.State.LastState == Encoder_FiniteMachine_stetes_3) {
      InternalRegs.Steps--;
      InternalRegs.Direction = Encoder_Direction_CWW;
    }
    break;
  }
  };
    
  }
    
  InternalRegs.State.LastState = InternalRegs.State.CurrentState;
}

inline void Encoder_LegacyRead(void) {
  Encoder_ReadPins();
  if (InternalRegs.LState.Bits.Pin_A) {
    if (!InternalRegs.LState.Bits.Pin_B){
      InternalRegs.Steps++; }
    if (InternalRegs.LState.Bits.Pin_B) {
      InternalRegs.Steps--;
    }
  }
  if (!InternalRegs.LState.Bits.Pin_A) {
    if (!InternalRegs.LState.Bits.Pin_B) {
      InternalRegs.Steps++;
    }
    if (InternalRegs.LState.Bits.Pin_B) {
      InternalRegs.Steps--;
    }
  }
}

void Encoder_CheckReferenceSatte(void) { InternalRegs.ReferenceCount++; }

void EXTI15_10_IRQHandler(void) {
  if (EXTI_GetITStatus(EXTI_Line15)) {
    EXTI_ClearITPendingBit(EXTI_Line15);

    Encoder_CheckFiniteState();
  }
}

void Encoder_CheckTiks(void) {}

void Encoder_InitHWRead(void) {
  Encoder_InitTypedef Encoder_InitType;
  Encoder_InitType.ReadMode = Encoder_ReadMode_EXTI_Ticks;
  Encoder_Init(&Encoder_InitType);
}

inline void Encoder_Feel_Counter(void) {}

uint16_t Encoder_GetReferenceCount(void) { return InternalRegs.ReferenceCount; }

void Encoder_ResetReferenceCount(void) { InternalRegs.ReferenceCount = 0; }

void Encoder_ResetStepsCount(void) {
  InternalRegs.Steps = 0;
  InternalRegs.Tiks = 0;
}

void Encoder_ChangeDirrection(Encoder_Direction_Sign _Sign) {
  InternalRegs.Sign = _Sign;
}

void Encoder_LEToBE(short *Source, short *Dest) {
  uint8_t HighPart = (*Source >> 8) & 0xFF;
  uint8_t LowPart = *Source & 0xFF;
  *Dest = HighPart | LowPart << 8;
}

void Encoder_GetValue(short *StepConv, uint8_t *Terminal, uint8_t *LowPart,
                      uint8_t *HighPart) {
  if (*StepConv <= 0) {
    *Terminal |= MSH_Core_TerminalState_NEGATIVE_COUNTER;
  } else {
    *Terminal |= MSH_Core_TerminalState_POSITIVE_COUNTER;
  }
  *LowPart = *StepConv & 0x7f;
  *HighPart = (*StepConv >> 7) & 0x7f;
}

short Encoder_GetStepsCount(void) { return InternalRegs.Steps; }

void Encoder_ResetDebufCnt(void) { ItCount = 0; }

void Encoder_SetMode(Encoder_Mode mode)
{
  EncMode =  mode; 
}
