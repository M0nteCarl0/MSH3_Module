//#include "stm32f10x.h"
#include "stm32f4xx.h"
#ifdef __cplusplus
 extern "C" {
 #endif   

 typedef enum TraceBase
 {
   TraceBase_Microsec = 42,
   TraceBase_Milisec = 42000,
   
 }TraceBase;

 void Timer_InitMilisecond(void);
 
 void Timer_TraceInit(void);
 void Timer_TraceBase(TraceBase Base);
 void Timer_TraceBegin(void);
 void Timer_TraceEnd(void);
 uint32_t Timer_TraceGetTime(void);
 
 void Timer_WaitMilisec(uint16_t Time);
 void Timer_WaitMicrosec(uint16_t Time);
 void  TIM6_DAC_IRQHandler(void);
 void TIM3_IRQHandler(void);
 #ifdef __cplusplus
 }
 #endif   