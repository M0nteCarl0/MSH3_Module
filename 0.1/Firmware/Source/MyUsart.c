//------------------------------------------------------------------------------
//    Novikov
//    10.02.2015
//------------------------------------------------------------------------------
#include "MyUsart.h"
#include "Commands.h"
#include "stm32f4xx.h"
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
MyStack m_StackR;
MyStack m_StackT;
MyCommand m_CommandR;

int m_FlgCheckCRC = 1; // 1 - Control CRC;   0 - Dont Make and Control CRC
int   DeviceID_Number;
uint8_t SendBufferInPC[1024];
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void InitCommand(MyCommand *Comd) {
  Comd->JC = 0;
  Comd->Bytes = 0;
  Comd->Flg = 1;
};
//------------------------------------------------------------------------------
/*
int ReceiveCommandL (uint8_t *Buff, int *N, MyStack *Stack, MyCommand *Comd)
{
__disable_irq();
  


  uint8_t Data;
  int Ret = 0;
  int i;
  


  while (GetCharStack (&Data, Stack)){

    if (Comd->JC == 0){
      if (Data == COMMAND_ID){
        Comd->Data [Comd->JC] = Data;
        Comd->JC++;
      }
    }
    else{
      Comd->Data [Comd->JC] = Data;
      Comd->JC++;
    }
    


    if ((Comd->JC > 1) && (Comd->Data [1] == Comd->JC)){
      for (i = 0; i < Comd->JC; i++){
        Buff [i] = Comd->Data [i];
      }
      *N = Comd->JC;
      


      Comd->JC = 0;
      


      Ret = 1;
      break;
    }
    


    if (Comd->JC > 1){
      if (Comd->Data [1] < Comd->JC || Comd->Data [1] > NCOMD){
        Comd->JC = 0;
        break;
      }
    }
  }
  


__enable_irq();
  


  return Ret;
};
*/
//------------------------------------------------------------------------------

int ReceiveCommandL(uint8_t *Buff, int *N, MyStack *Stack, MyCommand *Comd) {
  __disable_irq();
  // int Ret =   ReceiveCommandFromInternalStack(Stack,Buff,N);

  uint8_t Data;
  int Ret = 0;
  int i;

  while (GetCharStack(&Data, Stack)) {

    if (Comd->JC == 0) {
      // ID
      if (Data == COMMAND_ID || CheckByteOldProtocolSign(Data)) {
        Comd->Data[Comd->JC] = Data;
        Comd->JC++;
      }

    } else {
      Comd->Data[Comd->JC] = Data;
      Comd->JC++;
    }

    // if(CheckByteOldProtocolSign(

    // ADDR
    if (Comd->JC == 2) {
      if (CheckByteOldProtocolSign(Comd->Data[0])) {
        Buff[0] = Comd->Data[0];
        Buff[1] = Comd->Data[1];
        Ret = 1;
        *N = 2;
        Comd->JC = 0;
        return 1;
        break;

      } else {
        if (Comd->Data[1] != DeviceID_Number) {
          Comd->JC = 0;
          break;
        }
      }
    }
    // HEADER
    if (Comd->JC == 5) {
      uint8_t CRC_Header;
      if (m_FlgCheckCRC) {
        CRC_Header = MakeCRC(Comd->Data, 4);
      } else {
        CRC_Header = Comd->Data[Comd->JC - 1];
      }
      // CHECK CRC
      if (CRC_Header != Comd->Data[Comd->JC - 1]) {
        Comd->JC = 0;
        break;
      } else {
        if (Comd->Data[3] == 5) {
          for (i = 0; i < Comd->JC; i++) {
            Buff[i] = Comd->Data[i];
          }
          *N = Comd->JC;
          Comd->JC = 0;
          Ret = 1;
          break;
        }
      }
    }
    // DATA
    if ((Comd->JC > 5) && (Comd->Data[3] == Comd->JC)) {
      //         int SUM = 0;
      //         for (i = 5; i < Comd->JC-1; i++){
      //            SUM += (int) Comd->Data [i];
      //         }
      //         uint8_t CRC_Data = (uint8_t) (SUM & 0x007F);

      // CHECK CRC
      if (0) {
        //         if (CRC_Data != Comd->Data [Comd->JC-1]){
        Comd->JC = 0;
        break;
      } else {
        for (i = 0; i < Comd->JC; i++) {
          Buff[i] = Comd->Data[i];
        }
        *N = Comd->JC;
        Comd->JC = 0;
        Ret = 1;
        break;
      }
    }
    // ERROR-1
    if (Comd->JC > 3) {
      if (Comd->Data[3] < Comd->JC || (int)Comd->Data[3] > NCOMD) {
        Comd->JC = 0;
        break;
      }
    }
    // ERROR-2
    if (Comd->JC > 2) {
      if (Comd->Data[Comd->JC - 1] & 0x80) {
        Comd->JC = 0;
        break;
      }
    }
  }

  __enable_irq();

  return Ret;
};
//------------------------------------------------------------------------------
int SendCommandL(uint8_t *Buff, int N, MyStack *Stack) {
  int Ret = 1;
  int i;
  ClearCharStack(Stack);
  for (i = 0; i < N; i++) {
    SendBufferInPC[i] = Buff[i];
    Ret = PutCharStack(Buff[i], Stack);
    if (Ret == 0)
      break;
  }

  return Ret;
};
//----------------------------------------------------------------------------
uint8_t MakeCRC(uint8_t *Data, int N)
//    ���������� CRC ����� ��������
{
  int Summ = 0;
  int i = 0;
  uint8_t mCRC = 0;
  int SummH = 0;
  for (i = 0; i < N; i++) {
    Summ += Data[i];
  }
  SummH = (Summ >> 7) & 0x7F;
  mCRC = (uint8_t)(SummH + (Summ & 0x7F));
  
  mCRC &= 0x7F;
  return mCRC;
};
//----------------------------------------------------------------------------
/*
uint8_t MakeCRC (uint8_t *Data, int N)
{
   uint8_t mCRC = 0xFF;
   for (int i = 0; i < N; i++){
      mCRC = mCRC ^ Data[i];
   }
   mCRC &= 0x7F;
   return mCRC;
};
*/
//----------------------------------------------------------------------------
void AddCRC(uint8_t *Data, int N) {
  if (N < 5)
    return;

  Data[4] = MakeCRC(Data, 4);

  if (N > 6) {
    Data[N - 1] = MakeCRC(&Data[5], N - 6);
  }
};
//----------------------------------------------------------------------------
int CheckCRC(uint8_t *Data, int N) {
  uint8_t mCRC;
  if (N < 5)
    return 0;

  if (N > 6) {
    mCRC = MakeCRC(&Data[5], N - 6);
    if (mCRC != Data[N - 1])
      return 0;
  }
  return 1;
};
//------------------------------------------------------------------------------
int ReceiveCommand(uint8_t *Buff, int *N) {

  int Res = 0;
  int Ret = ReceiveCommandL(Buff, N, &m_StackR, &m_CommandR);
  if (Ret && m_FlgCheckCRC && *N != 2) {
    if (CheckCRC(Buff, *N)) {
      Res = 1;
    }
  }

  if (Ret && *N == 2) {
    Res = 1;
  }

  return Res;
};
//------------------------------------------------------------------------------
int SendCommand(uint8_t *Buff, int N) {
  if (m_FlgCheckCRC)
    //Buff[0] = 0x96;
    AddCRC(Buff, N);
  return SendCommandL(Buff, N, &m_StackT);
};
//------------------------------------------------------------------------------
void InitInternalReceiveStack(void) { InitStack(&m_StackR); }
/****************************************************************************/
void InitIntrenalTranceiveStack(void) { InitStack(&m_StackT); }
/****************************************************************************/
void InitIntrenalReceiveAndTranceiveStack(void) {
  InitInternalReceiveStack();
  InitIntrenalTranceiveStack();
  SetCRCCheck(1);
}
/****************************************************************************/
void SetCRCCheck(int Status) { m_FlgCheckCRC = Status; }
/****************************************************************************/
void BindExternalReceiveStack(MyStack ReceiveStack) { m_StackR = ReceiveStack; }
/****************************************************************************/
void BindExternalTranceiveStack(MyStack TranceiveStack) {
  m_StackT = TranceiveStack;
}
/****************************************************************************/
MyStack GetReceiveStack(void) { return m_StackR; }
/****************************************************************************/
MyStack GetTransmitStack(void) { return m_StackT; }
/****************************************************************************/
int PutOneCharterToReceiveStack(uint8_t Charter) {
  return PutCharStack(Charter, &m_StackR);
}
/****************************************************************************/
int GetCharterFromTranceiveStack(uint8_t *Charter) {
  return GetCharStack(Charter, &m_StackT);
}
/****************************************************************************/
MyCommand GetComandBuffer(void) { return m_CommandR; }
/****************************************************************************/
void InitInternalCommand(void) { InitCommand(&m_CommandR); }
/****************************************************************************/
int SendCommandLegacy(uint8_t *Buff, int N) {
  return SendCommandL(Buff, N, &m_StackT);
}
/****************************************************************************/
int CheckByteOldProtocolSign(uint8_t Charter) {
  int Res = 0;
  switch (Charter) {
  case CommadID_Two_Byte_READ:
  case CommadID_Two_Byte_TERMINAL:
  case CommadID_Two_Byte_Status:
  case CommadID_Two_Byte_StepsHi:
  case CommadID_Two_Byte_StepsLo:
  case CommadID_Two_Byte_VelocityHi:
  case CommadID_Two_Byte_VelocityLo:
  case CommadID_Two_Byte_RampHi:
  case CommadID_Two_Byte_RampLo:
  case CommadID_Two_Byte_CounterHi:
  case CommadID_Two_Byte_CounterLo:
  case CommadID_Two_Byte_InitialVLo:
  case CommadID_Two_Byte_TimeDelayLo:
  case CommadID_Two_Byte_TurnOffHVLo: {
    Res = 1;
    break;
  }
  }
  return Res;
};
/****************************************************************************/
int ChecIs8ByteCharter(uint8_t Charter) { return Charter & 0x80; }
/****************************************************************************/
void Filter7BitTraffc(uint8_t *Data, int N) {
  for (int i = 1; i < N; i++) {
    Data[i] = Data[i] & 0x7f;
  }
}
/****************************************************************************/
int ReceiveCommandFromInternalStack(MyStack *Stack, uint8_t *Buff, int *N) {

  int flag = 0;
  /* 1  -> Extarct all bytes from Stack
     2 - > Analize by protocol
     3  ->  Pull buffer with data
  */

  uint8_t Data;
  int DataCounter = 0;
  while (GetCharStack(&Data, Stack)) {
    Buff[DataCounter++] = Data;
  }
  Stack->Begin = GetRecivedBytesCount();
  *N = GetRecivedBytesCount();
  ;
  ResetRecivedBytesCount();
  if (NewProtocolCheck(Buff, &DataCounter)) {
    *N = DataCounter;
    return 1;
  }

#if 0  
   if(OldProtocolCheck(Buff,&DataCounter)
  {
    return 1;
  }

#endif
  return flag;
}

int NewProtocolCheck(uint8_t *Buff, int *N) {
  int Flag = 1;
  if (!NewProtocolCheckHeader(Buff, N) || !NewProtocolCheckPayload(Buff, N)) {
    Flag = 0;
  }
  return Flag;
}

int OldProtocolCheck(uint8_t *Buff, int *N) {
  int flag = 1;
  if (*N != 2) {
    return 0;
  }

  if (!CheckByteOldProtocolSign(Buff[0])) {
    return 0;
  }
  return flag;
}

int NewProtocolCheckHeader(uint8_t *Buff, int *N) {
  int Flag = 1;
  uint8_t CRC_Header;
  if (*N < 5) {
    return 0;
  }
  if (Buff[0] != COMMAND_ID) {
    return 0;
  }

  if (Buff[1] != EXPO_ADR) {
    return 0;
  }

  if (Buff[3] != *N) {
    return 0;
  }

  if (m_FlgCheckCRC) {
    CRC_Header = MakeCRC(Buff, 4);
  } else {
    CRC_Header = Buff[*N - 1];
  }

  if (CRC_Header != Buff[4]) {
    return 0;
  }

  return Flag;
}

int NewProtocolCheckPayload(uint8_t *Buff, int *N) {}

int OldProtocolCheckHeader(uint8_t *Buff, int *N) {}

void ClearCommandCounter(MyCommand *Comd) {
  Comd->JC = 0;
  ;
}
 void SetDeviceID(int ID){
    DeviceID_Number  = ID;
 }
      
 
      