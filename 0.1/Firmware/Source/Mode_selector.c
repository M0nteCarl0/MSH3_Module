#include "Mode_selector.h"
Mode_selector_GPIO_asigment *_Assigment;

void Mode_selector_SetMode(Mode_selector_Mode Mode) {
  if (Mode == Mode_selector_Mode_TDI) {
    GPIO_SetBits(_Assigment->Port, _Assigment->PinTDI);
    GPIO_ResetBits(_Assigment->Port, _Assigment->PinCCD);
  } else {
    GPIO_ResetBits(_Assigment->Port, _Assigment->PinTDI);
    GPIO_SetBits(_Assigment->Port, _Assigment->PinCCD);
  }
}

Mode_selector_Mode Mode_selector_GetMode(void) {
  Mode_selector_Mode _Mode = Mode_selector_Mode_TDI;
  if (!GPIO_ReadOutputDataBit(_Assigment->Port, _Assigment->PinCCD)) {
    _Mode = Mode_selector_Mode_CCD;
  }
  return _Mode;
}

void Mode_selector_GPIO_Init(Mode_selector_GPIO_asigment *Assigment) {
  GPIO_InitTypeDef GPIO;
  _Assigment = Assigment;
  RCC_AHB1PeriphClockCmd(Assigment->Clock_pin, ENABLE);
  GPIO.GPIO_Mode = GPIO_Mode_OUT;
  GPIO.GPIO_Pin = Assigment->PinCCD | Assigment->PinTDI;
  GPIO.GPIO_PuPd = GPIO_PuPd_DOWN;
  GPIO.GPIO_Speed = GPIO_Speed_2MHz;
  GPIO.GPIO_OType = GPIO_OType_OD;
  GPIO_Init(Assigment->Port, &GPIO);
}

void Mode_selector_Init(void) {
  Mode_selector_GPIO_asigment Assigment;
  Assigment.Clock_pin = RCC_AHB1Periph_GPIOA;
  Assigment.Port = GPIOA;
  Assigment.PinCCD = GPIO_Pin_12;
  Assigment.PinTDI = GPIO_Pin_8;
  Mode_selector_GPIO_Init(&Assigment);
}
