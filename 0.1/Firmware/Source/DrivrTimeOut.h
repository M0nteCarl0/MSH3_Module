#include "stm32f4xx.h"
void  DrivrTimeOut_Init (void);
void DrivrTimeOut_SetTimeOut(int Time);
void DrivrTimeOut_StartCounter(void);
void DrivrTimeOut_StopCounter(void);
int  DrivrTimeOut_TimeisExpiried(void);