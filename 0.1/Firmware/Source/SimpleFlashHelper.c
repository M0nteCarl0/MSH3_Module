#include "SimpleFlashHelper.h"
/******************************************************************************/
bool SimpleFlashHelper_WriteToFlash(uint8_t *Buffer, uint32_t Size,
                                    uint32_t AddresBegin) {
  FLASH_Unlock();
  uint8_t BufferCh[128];
  uint8_t BufferCh1[128];
  FLASH_EraseSector(FLASH_Sector_6, VoltageRange_3);

  for (uint32_t im = 0; im < Size; im++) {
    FLASH_ProgramByte(AddresBegin + im * 8, Buffer[im]);
    BufferCh1[im] = Buffer[im];
  }

  FLASH_Lock();
  SimpleFlashHelper_ReadFromFlash(BufferCh, Size, AddresBegin);
}
/******************************************************************************/
bool SimpleFlashHelper_ReadFromFlash(uint8_t *Buffer, uint32_t Size,
                                     uint32_t AddresBegin) {

  for (uint32_t im = 0; im < Size; im++) {
    Buffer[im] = (uint8_t)(*(__IO uint32_t *)AddresBegin + im * 8);
  }
}
/******************************************************************************/
