#include "ColimatorJumpersControl.h"
void ColimatorJumpersControl_Init(void) {
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
  GPIO_InitTypeDef _GPIO;

  _GPIO.GPIO_Mode = GPIO_Mode_IN;
  _GPIO.GPIO_Pin = GPIO_Pin_2;
  _GPIO.GPIO_PuPd = GPIO_PuPd_UP;
  _GPIO.GPIO_Speed = GPIO_Speed_2MHz;
  _GPIO.GPIO_OType = GPIO_OType_PP;
  GPIO_Init(GPIOC, &_GPIO);
}

ColimatorJumpersSState ColimatorJumpersControl_GetJumperState(void) {
  ColimatorJumpersSState State = ColimatorJumpersState_MainDirection;
  if (GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_2) == RESET) {
    State = ColimatorJumpersState_ReveceDirection;
  }

  return State;
}
