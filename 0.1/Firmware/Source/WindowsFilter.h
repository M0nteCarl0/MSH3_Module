#include <stdint.h>
class MyMemmo
{
   public:
      uint8_t   *m_Addr;
      int       m_N;
      int       m_Begin;                                    // ��������� ������
      
      MyMemmo (){ m_Addr = 0; m_N = 0; m_Begin = 0;};
      MyMemmo (int N){Create (N); Set (0xFF);};
      void Create (int N);

      void     Put      (uint8_t Data);
      uint8_t  Get      (int I);
      int      GetAdd   (int I0, int I1);
      void     Copy     (uint8_t *Buff);
      void     Shift    (int Shift);
      void     Set      (uint8_t Data);
};