#include "Clock.h"
#include "LED.h"
#include <stdio.h>
/**
******************************************************************************
* @file    Clock.c
* @author  Molotaliev A.O(molotaliev@xprom.ru)
* @version V 0.5.1
* @date    16-february-2016/17-february-2016/9-March-2016
* @brief   This file provides CLK f(x) for stm32f405 dual stage start
*
*/

/****************************************************************************/

static WorkCLK DeviceClocks;

/*****************************************************************************/

void HSI_CLK(void) // 1-st Stage
{
  RCC_DeInit();
  RCC_HSICmd(ENABLE);
  RCC_SYSCLKConfig(RCC_SYSCLKSource_HSI);
  RCC_HCLKConfig(RCC_SYSCLK_Div1);
  RCC_PCLK1Config(RCC_HCLK_Div1);
  RCC_PCLK2Config(RCC_HCLK_Div1);
  FLASH_SetLatency(FLASH_Latency_14);
  // FLASH_DataCacheCmd(ENABLE);
  // FLASH_InstructionCacheCmd(ENABLE);
  // FLASH_PrefetchBufferCmd(ENABLE);
  // PWR_MainRegulatorModeConfig(PWR_Regulator_Voltage_Scale1);
}
/*****************************************************************************/
void HSE_BYPASS(void) // 2-nd Stage
{
  HSI_CLK();
  RCC_DeInit();
  RCC_HSICmd(ENABLE);
  while (RCC_GetFlagStatus(RCC_FLAG_HSIRDY) != SET) {
  };
  RCC_HSEConfig(RCC_HSE_OFF);
  //RCC_WaitForHSEStartUp();
  RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);

  FLASH_DataCacheCmd(DISABLE);
  FLASH_InstructionCacheCmd(DISABLE);
  FLASH_PrefetchBufferCmd(DISABLE);
  PWR_MainRegulatorModeConfig(PWR_Regulator_Voltage_Scale1);
  FLASH_SetLatency(FLASH_Latency_5);

  /*
  finput = 8 Mhz
  PLLM = 8
  PLLM = 336
  PLLP = 2
  PLLQ = 8
  


  


  FVCO = finput * (PLLN/PLLM) = 8 * (336/8) ->336
  FPLL = FVCO/PLLP = 336/2 ->168
  FUSB = FVCO/PLLQ = 336/8 ->42
  AHB  Clock = FPLL->168;
  APB1 Clock = FPLL/DIVAPB1 = 168/4 = 42 Mhz
  APB2 Clock = FPLL/DIVAPB2 = 168/4 = 42 Mhz
  


  


  */
  RCC_PLLConfig(RCC_PLLSource_HSI, 16, 336, 2, 8); // 168 mhz CORE
  /*
  finput = 16 Mhz
  PLLM = 8
  PLLM = 336
  PLLP = 2
  PLLQ = 8
  


  


  FVCO = finput * (PLLN/PLLM) = 16 * (336/16) ->336
  FPLL = FVCO/PLLP = 336/2 ->168
  FUSB = FVCO/PLLQ = 336/8 ->42
  AHB  Clock = FPLL->168;
  APB1 Clock = FPLL/DIVAPB1 = 168/4 = 42 Mhz
  APB2 Clock = FPLL/DIVAPB2 = 168/4 = 42 Mhz
  


  


  */

  // RCC_PLLConfig(RCC_PLLSource_HSE,16,336,2,8);//168 mhz CORE#endif

  // OnSVD2();
  RCC_PLLCmd(ENABLE);
  while (RCC_GetFlagStatus(RCC_FLAG_PLLRDY) != SET) {
  };
  OffSVD2();
  RCC_HCLKConfig(RCC_SYSCLK_Div1); // 168 mhz AHB(OTG HS,ETH,DMA,CRC,GPIOx)
  RCC_PCLK1Config(RCC_HCLK_Div8);  // 42 mhz APB1(DAC,PWR,I2Cx,SPIx)
  RCC_PCLK2Config(RCC_HCLK_Div8);  // 42 mhz APB2
  SYSCFG_CompensationCellCmd(ENABLE);
}
/******************************************************************************/
void InitClocs(Clocks CLK) {
  RCC_DeInit();
  uint32_t PLLSource = 0x0;
  if (CLK.OSC_TYPE == RCC_SYSCLKSource_HSI) {

    RCC_HSICmd(ENABLE);
    CLK.OSC_MHZ = 16;

    while (RCC_GetFlagStatus(RCC_FLAG_HSIRDY) != SET) {
    };
    PLLSource = RCC_PLLSource_HSI;
  } else {
    RCC_HSEConfig(RCC_HSE_ON);
    while (RCC_WaitForHSEStartUp() != SUCCESS) {
    };
    PLLSource = RCC_PLLSource_HSE;
  }
  DeviceClocks.OSC_Hertz = CLK.OSC_MHZ * 1000000;
  DeviceClocks.AHB =
      (DeviceClocks.OSC_Hertz * (CLK.PLLN / CLK.PLLM)) / CLK.PLLP;
  RCC_PLLConfig(PLLSource, CLK.PLLM, CLK.PLLN, CLK.PLLP, CLK.PLLQ);
  RCC_PLLCmd(ENABLE);
  while (RCC_GetFlagStatus(RCC_FLAG_PLLRDY) != SET) {
  };
  RCC_HCLKConfig(CLK.AHB_Divider);
  RCC_PCLK1Config(CLK.APB1_Divider);
  RCC_PCLK2Config(CLK.APB2_Divider);
  DeviceClocks.AHB /= GetAHBDicider(CLK.AHB_Divider);
  DeviceClocks.APB1 = DeviceClocks.AHB / GetAPBDicider(CLK.APB1_Divider);
  DeviceClocks.APB2 = DeviceClocks.AHB / GetAPBDicider(CLK.APB2_Divider);
}
/******************************************************************************/
uint32_t GetAHBDicider(uint32_t DivAllias) {
  uint32_t Divider;
  switch (DivAllias) {
  case RCC_SYSCLK_Div1: {
    Divider = 1;
    break;
  };
  case RCC_SYSCLK_Div2: {
    Divider = 2;
    break;
  };
  case RCC_SYSCLK_Div4: {
    Divider = 4;
    break;
  };
  case RCC_SYSCLK_Div8: {
    Divider = 8;
    break;
  };
  case RCC_SYSCLK_Div16: {
    Divider = 16;
    break;
  };
  case RCC_SYSCLK_Div64: {
    Divider = 64;
    break;
  };
  case RCC_SYSCLK_Div128: {
    Divider = 128;
    break;
  };
  case RCC_SYSCLK_Div256: {
    Divider = 256;
    break;
  };
  case RCC_SYSCLK_Div512: {
    Divider = 512;
    break;
  };
  }
  return Divider;
}
/******************************************************************************/
uint32_t GetAPBDicider(uint32_t DivAllias) {
  uint32_t Divider;
  switch (DivAllias) {
  case RCC_HCLK_Div1: {
    Divider = 1;
    break;
  };
  case RCC_HCLK_Div2: {
    Divider = 2;
    break;
  };
  case RCC_HCLK_Div4: {
    Divider = 4;
    break;
  };
  case RCC_HCLK_Div8: {
    Divider = 8;
    break;
  };
  case RCC_HCLK_Div16: {
    Divider = 16;
    break;
  };
  }
  return Divider;
}
/******************************************************************************/
WorkCLK GetWorkClocks(void) { return DeviceClocks; };
/******************************************************************************/
