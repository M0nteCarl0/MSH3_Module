#include "stm32f4xx.h"

#pragma once
#ifdef __cplusplus
extern "C" {
#endif   
void HV_Trigge_Init(void);
uint8_t  HV_Trigge_ReadIbput(void);
bool  HV_Trigge_RissingEdgeDeteced(void);
#ifdef __cplusplus
}
#endif   
