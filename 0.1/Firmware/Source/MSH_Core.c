#include "MSH_Core.h"
#include "ByteConversion.h"
#include "CCD_Trigger.h"
#include "ColimatorJumpersControl.h"
#include "Encoder.h"
#include "FlashExtended.hpp"
#include "FrameDurationTimeStamps.h"
#include "HV_Impulse.h"
#include "HV_Trigger.h"
#include "Indep_WD.h"
#include "LED.h"
#include "Mode_selector.h"
#include "MyUsartHard.h"
#include "SimpleFlashHelper.h"
#include "SteperConversion.h"
#include "Timer.h"
#include "Timer_TDI_Impulse.h"
#include "WindowsFilter.h"
#include "DrivrTimeOut.h"
#include "MyUsart.h"
#include "ColimatorJumpersControl.h"
using namespace Conversion;
ColimatorJumpersSState ColimatorJumperState;
MSH_Core_Params _MSH_Core_Params;
StatusU EngineStatus;
MSH_Core_Params_StateBU StateMS;
uint32_t AddresSPIN = 0x08040000;
cSPIN_RegsStruct_TypeDef cSPIN;
FlashIO _FlashEx;
Terminalu _TerminalBits;
int HVIntCounter = 0;
uint8_t LookAtSensor;
int32_t MotorPosition = 0;
bool IsZeroPointEmmit = false;
MSH_Core_mode_module ModuleMode;
static int ItCountAC = 0;
static int ItCountBC = 0;
static int MoveFlag = 0;
int ErrorStateCountert = 0;
int PositionTurnOffHV = 0;
int PositionBeginSoftBrake = 0;
int PositionToStop = 0;
bool HvIsTurnOff;
uint32_t CurrentSpeed = 0;
MyMemmo WindowFilterKy;
bool Trigered = false;
bool DriveFrame = true;
bool SoftBrakeStage = 0;
bool HardStopBrakeStatge = 0;
int DecFactor = 250;
MSH_Core_DeviceAdress _DeviceAdress;
MSH_Core_ParckingParametrs _ParkingSetup;
int PositionBeginBraking = 0;
int PositionEndBraking = 0;
int TotalMicroStepPath = 0;
int BrakeLatch = 0;
int SMType;
bool LatchRampHigh;
bool LatchRampLow;
bool VelocityInitLatch;
bool VelocityLowLatch;
bool VelocityHighLatch;
bool DellayLatch;
bool TurnOffHVLatch;
int DeviceIDState = 0;

MSH_Core_Params_MotionWordBU MotionWord;
//TODO �������� ������ ����������(�������� ��������!!) 12/07/2019



/*
Colimator:
Colimator speed end to end frame: 1500 / 6 = 250 Steps/s
Colimator speed end to end frame: 1500 / 20 = 75 Steps/s

Scaner:
Scaner speed end to end frame: 1500 / 6 = 250 Steps/s
Scaner speed end to end frame: 1500 / 20 = 75 Steps/s


*/



void MSH_Core_Init_Polling_Timer(void) {

  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
  TIM_TimeBaseInitTypeDef _TimerSetup;
  ;
  _TimerSetup.TIM_ClockDivision = TIM_CKD_DIV1;
  _TimerSetup.TIM_CounterMode = TIM_CounterMode_Up;
  _TimerSetup.TIM_Prescaler = 42;
  _TimerSetup.TIM_Period =  500-1;
  TIM_TimeBaseInit(TIM3, &_TimerSetup);
  TIM_Cmd(TIM3, ENABLE);
  TIM_ITConfig(TIM3, TIM_IT_Update, ENABLE);
  NVIC_InitTypeDef _SetupIRQ;
  _SetupIRQ.NVIC_IRQChannel = TIM3_IRQn;
  _SetupIRQ.NVIC_IRQChannelPreemptionPriority = 1;
  _SetupIRQ.NVIC_IRQChannelSubPriority = 1;
  _SetupIRQ.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&_SetupIRQ);
  WindowFilterKy.Create(16);
}

void MSH_Core_ControlLogic(void) {
  StateMS.Raw = MSH_CoreGetState();
  MotorPosition = cSPIN_Get_ABS_Position();
  CurrentSpeed = cSPIN_Get_CurrentSpeed();
  MSH_Core_PollingSensosrs();
}

void MSH_Core_HighVoltageOffLogic(void) {
  
  if (ModuleMode == MSH_Core_mode_module_Scaner ||
      ModuleMode == MSH_Core_mode_module_Collimator && ColimatorJumperState == ColimatorJumpersState_MainDirection ) {
    if (Encoder_GetStepsCount() == PositionTurnOffHV && !HvIsTurnOff) {
      if (EngineStatus.Bits.DIR == 1) {
        HvIsTurnOff = true;
        HV_Impulse_TurnOffHV();
      }
    }
  }
  
  
  if(ModuleMode == MSH_Core_mode_module_Collimator && ColimatorJumperState == ColimatorJumpersState_ReveceDirection)
  { 
    if (Encoder_GetStepsCount() == PositionTurnOffHV && !HvIsTurnOff) {
      if (EngineStatus.Bits.DIR == 0) {
        HvIsTurnOff = true;
        HV_Impulse_TurnOffHV();
      }
    }
    
  }
}

void MSH_Core_PollingSensosrs(void) {
  CheckSensors();
  MSH_Core_HighVoltageOffLogic();
  EngineStatus.Bits = cSPIN_Get_StatusBF();
  SensorWord Sense = GetStatusSensors();
  MSH_Core_Drive_TDI_Mode();
  MSH_Core_ResetEncoderStepsEvent();
  MSH_Core_SensorTriggerEvent(Sense);
  
}

void MSH_Core_PollingHVImpulse(void) {}

void MSH_Core_SetVelocityLow(uint8_t Low) {
  if((_MSH_Core_Params.State & 0x1) == ModuleMode && !VelocityLowLatch){
  _MSH_Core_Params.VelocityLow = Low;
   VelocityLowLatch = true;
  }
}

void MSH_Core_SetVelocityHigh(uint8_t High) {
if((_MSH_Core_Params.State & 0x1) == ModuleMode && !VelocityHighLatch){
  _MSH_Core_Params.VelocityHigh = High;
  VelocityHighLatch = true;
  }
}

void MSH_Core_SetTimeAccLow(uint8_t Low) {
 if((_MSH_Core_Params.State & 0x1) == ModuleMode){
  _MSH_Core_Params.TimeAccLow = Low;
  }
}

void MSH_Core_SetTimeAccHigh(uint8_t High) {
  if((_MSH_Core_Params.State & 0x1) == ModuleMode){
  _MSH_Core_Params.TimeAccLHigh = High;
  }
}

void MSH_Core_SetDellayAcc(uint8_t DellayAcc) {
  if((_MSH_Core_Params.State & 0x1) == ModuleMode && !DellayLatch){
  if (DellayAcc > 127) {
    DellayAcc = 127;
  }
  _MSH_Core_Params.DellayAcc = DellayAcc;
  DellayLatch = true;
  }
}

void MSH_Core_SetStepLimiterLow(uint8_t Low) {
  if((_MSH_Core_Params.State & 0x1) == ModuleMode){
  _MSH_Core_Params.StepLimiterLow = Low;
  }
}

void MSH_Core_SetStepLimiterHigh(uint8_t High) {
 if((_MSH_Core_Params.State & 0x1) == ModuleMode){
  _MSH_Core_Params.StepLimiterHigh = High;
  }
}

void MSH_CoreSetState(uint8_t State) { 
  int  DeviceSignatureW = State & 0x01;
  if(DeviceSignatureW == ModuleMode){
  _MSH_Core_Params.State = State;
   StateMS.Raw  = State;
  LatchRampHigh = false;
  LatchRampLow = false;
  VelocityInitLatch = false;
  VelocityLowLatch = false;
  VelocityHighLatch = false;
  DellayLatch = false;
  TurnOffHVLatch = false;
  }
}

void MSH_CoreSetTimeOnBN(uint8_t TimeOnBN) {
  if((_MSH_Core_Params.State & 0x1) == ModuleMode){
  _MSH_Core_Params.TimeOnBN = TimeOnBN;
  }
}

uint8_t MSH_CoreGetSensorsState(void) { return _MSH_Core_Params.SensorsState; }

uint8_t MSH_CoreGetState(void) { return _MSH_Core_Params.State; }

void MSH_CoreSetRampHigh(uint8_t High) {
   if((_MSH_Core_Params.State & 0x1) == ModuleMode && !LatchRampHigh){
  _MSH_Core_Params.RampHigh = High;
  LatchRampHigh = true;
   }
}

void MSH_CoreSetRampLow(uint8_t Low) {
   if((_MSH_Core_Params.State & 0x1) == ModuleMode && !LatchRampLow ){
  _MSH_Core_Params.RampLow = Low;
  LatchRampLow = true;
   }
}

uint8_t MSH_CoreGetRampHigh(void) { return _MSH_Core_Params.RampHigh; }

uint8_t MSH_CoreGetRampLow(void) { return _MSH_Core_Params.RampLow; };

void MSH_CoreSetCounterLo(uint8_t Lo) {
   if((_MSH_Core_Params.State & 0x1) == ModuleMode){
  _MSH_Core_Params.CounterLo = Lo;
   }
}

void MSH_CoreSetCounterHi(uint8_t Hi) {
   if((_MSH_Core_Params.State & 0x1) == ModuleMode){
  _MSH_Core_Params.CounterHi = Hi;
   }
}

uint8_t MSH_CoreGetCounterLo(void) { return _MSH_Core_Params.CounterLo; }

uint8_t MSH_CoreGetCounterHi(void) { return _MSH_Core_Params.CounterHi; }

uint8_t MSH_CoreGetTerminal(void) { return _MSH_Core_Params.Terminal; }

void MSH_CoreSetVelocityInit(uint8_t InitVel) {
   if(StateMS._Bits.DeviceType == ModuleMode && !VelocityInitLatch){
  _MSH_Core_Params.VelocityInit = InitVel;
   VelocityInitLatch = true;
   }
}

uint8_t MSH_CoreGetVelocityInit(void) { return _MSH_Core_Params.VelocityInit; }

uint8_t MSH_Core_GetStepLimiterLow(void) {
  return _MSH_Core_Params.StepLimiterLow;
}

uint8_t MSH_Core_GetStepLimiterHigh(void) {
  return _MSH_Core_Params.StepLimiterHigh;
}

uint8_t MSH_Core_GetVelocityLow(void) { return _MSH_Core_Params.VelocityLow; }

uint8_t MSH_Core_GetVelocityHigh(void) { return _MSH_Core_Params.VelocityHigh; }

uint8_t MSH_Core_GetDellayAcc(void) { return _MSH_Core_Params.DellayAcc; }

uint8_t MSH_GetTurnOffHVLo(void) { return _MSH_Core_Params.TurnOffHVLo; }

void MSH_SetTurnOffHVLo(uint8_t Low) {
   if(StateMS._Bits.DeviceType == ModuleMode && !TurnOffHVLatch){
  _MSH_Core_Params.TurnOffHVLo = Low;
  PositionTurnOffHV = 6000 + (58 * Low);
  TurnOffHVLatch = true;
  }
}

uint16_t MSH_Core_GetStepsValue(void) {
  uint8_t Low = MSH_Core_GetStepLimiterLow();
  uint8_t High = MSH_Core_GetStepLimiterHigh();
  uint16_t Val = 0;
  Bit7ToWord14(&Val, &Low, &High);
  return Val;
}

inline void MSH_Core_PreepareForRun(void) {
  cSPIN_Hard_HiZ();
  cSPIN_Reset_Pos();
  MSH_Core_ComputeAcceleraion();
  MSH_Core_WriteInitialAndTargetSpeeds();
  cSPIN_Hard_Stop();
}

float MSH_Core_ComputeAcceleraion(void) {
  float Acc = 0;
  float Dec = 0;
  uint8_t VelocityL = MSH_Core_GetVelocityLow();
  uint8_t VelocityH = MSH_Core_GetVelocityHigh();
  uint16_t Velocity = 0;
  uint8_t RampH = MSH_CoreGetRampHigh();
  uint8_t RampL = MSH_CoreGetRampLow();
  uint16_t Ramp = 0;
  uint16_t InitVelosity = MSH_CoreGetVelocityInit();
  Bit7ToWord14(&Ramp, &RampL, &RampH);
  Bit7ToWord14(&Velocity, &VelocityL, &VelocityH);
  if (Ramp == 0)
    Ramp = 1;
  if (cSPIN.SensorOffset == 0)
    cSPIN.SensorOffset = 1;
  float TimeSeconsds = (float)(Ramp) / 1000;
  Acc = (((float)(Velocity) - (float)(InitVelosity)) / TimeSeconsds);
  Dec =
      (((float)(Velocity * Velocity) - (float)(InitVelosity * InitVelosity))) /
      (2 * (float)cSPIN.SensorOffset);
  cSPIN_SetAcc(Acc);
  cSPIN_SetDec(Dec);
  return Acc;
}

void MSH_Core_DriveCommands(uint8_t StateWordW) {
  __disable_irq();
 
  int DeviceSignature = StateWordW & 0x1;
  DeviceIDState = DeviceSignature;
  int TDI_Mode =  (StateWordW >> 1) &  0x1;
  if(DeviceSignature == ModuleMode){
   StateMS.Raw = StateWordW;
  _MSH_Core_Params.State = StateWordW;

  uint16_t Velocity = 0;
  EngineStatus.Bits = cSPIN_Get_StatusBF();
  uint32_t A;
  uint32_t V1;
  uint32_t V2;
  float Tcoff = ((V2 - V1) / A) - 1;
  TotalMicroStepPath = 0;

   if(StateMS._Bits.External_Trigger == 0)
  {
  if ((StateMS._Bits.DriveEndFrame == 0 &&
       StateMS._Bits.DriveBeginFrame == 0) &&
      (EngineStatus.Bits.MOT_STATUS != MOT_STATUSE_Stopped)) {
      cSPIN_Hard_Stop();
      DrivrTimeOut_StopCounter();
      PositionToStop = 0;
        _MSH_Core_Params.Terminal &= ~MSH_Core_TerminalState_TERM_BEG;
       _MSH_Core_Params.Terminal &= ~MSH_Core_TerminalState_TERM_END;
       _MSH_Core_Params.Terminal &= ~MSH_Core_TerminalState_TERM_ON;
      MoveFlag = 0;
      SoftBrakeStage = 0;
      HardStopBrakeStatge = 0;
    }
  }
  
  
  if (StateMS._Bits.DriveBeginFrame) {
    if (EngineStatus.Bits.MOT_STATUS == MOT_STATUSE_Stopped) {
      uint32_t Steps = (uint32_t)MSH_Core_GetStepsValue();
      if (Steps > 58000) {
        Steps = 12000;
      }
      cSPIN_Hard_Stop();
     
      MSH_Core_PreepareForRun();
      MSH_Core_ControlTerminaleDriveState();
      uint8_t VelocityL = MSH_Core_GetVelocityLow();
      uint8_t VelocityH = MSH_Core_GetVelocityHigh();
      Bit7ToWord14(&Velocity, &VelocityL, &VelocityH);
      
      if(ColimatorJumperState == ColimatorJumpersState_ReveceDirection 
         &&  ModuleMode == MSH_Core_mode_module_Collimator){
             Encoder_ChangeDirrection(Encoder_Direction_Sign_CW);
             PositionToStop = cSPIN_GetScaledPositionToStop(Steps);
              cSPIN_Run(FWD, Speed_Steps_to_Par(Velocity));
       }
      else{
         PositionToStop = cSPIN_GetScaledPositionToStop(-Steps);
         Encoder_ChangeDirrection(Encoder_Direction_Sign_CWW);
         cSPIN_Run(REV, Speed_Steps_to_Par(Velocity));
      }
      
      
     
      DrivrTimeOut_StartCounter();
      MoveFlag = 1;
      SoftBrakeStage = 0;
      HardStopBrakeStatge = 0;
      MotionWord._Bits.DriveBeginFrame =  1;
      StateMS._Bits.DriveBeginFrame  = 0;
     _MSH_Core_Params.State = StateMS.Raw;
    }
  }
  
  if (StateMS._Bits.DriveEndFrame) {
    if (EngineStatus.Bits.MOT_STATUS == MOT_STATUSE_Stopped) {
      uint32_t Steps = (uint32_t)MSH_Core_GetStepsValue();
      cSPIN_Hard_Stop();
      MSH_Core_PreepareForRun();
      MSH_Core_ControlTerminaleDriveState();

      if (Steps > 58000) {
        Steps = 12000;
      }
     
      uint8_t VelocityL = MSH_Core_GetVelocityLow();
      uint8_t VelocityH = MSH_Core_GetVelocityHigh();
      Bit7ToWord14(&Velocity, &VelocityL, &VelocityH);
      
      
      if(ColimatorJumperState == ColimatorJumpersState_ReveceDirection 
         &&  ModuleMode == MSH_Core_mode_module_Collimator)
      
      {
         Encoder_ChangeDirrection(Encoder_Direction_Sign_CWW);
         PositionToStop = cSPIN_GetScaledPositionToStop(-Steps);
         cSPIN_Run(REV, Speed_Steps_to_Par(Velocity));
        
      }
      else
      {
        Encoder_ChangeDirrection(Encoder_Direction_Sign_CW);
        PositionToStop = cSPIN_GetScaledPositionToStop(Steps);
        cSPIN_Run(FWD, Speed_Steps_to_Par(Velocity));
    
      }
      
     
      DrivrTimeOut_StartCounter();
      MoveFlag = 1;
      SoftBrakeStage = 0;
      HardStopBrakeStatge = 0;
       StateMS._Bits.DriveEndFrame = 0;
      _MSH_Core_Params.State = StateMS.Raw;
    }
  }

 
 
  }
  __enable_irq();
}

void MSH_Core_WriteInitialAndTargetSpeeds(void) {
  uint8_t InitVelosity = MSH_CoreGetVelocityInit();
  uint8_t TargetVelocityL = MSH_Core_GetVelocityLow();
  uint8_t TargetVelocityH = MSH_Core_GetVelocityHigh();
  uint16_t TargetVelocity = 0;
  Bit7ToWord14(&TargetVelocity, &TargetVelocityL, &TargetVelocityH);
  cSPIN_SetMinSpeed((float)InitVelosity);
}

void MSH_Core_ControlTerminaleDriveStateStoped(void) {
  MSH_CoreSetTerminal(~MSH_Core_TerminalState_TERM_ON); //!!
}

void MSH_Core_ControlTerminaleDriveState(void) {
  MSH_CoreSetTerminal(MSH_Core_TerminalState_TERM_ON); //!!
}

void MSH_Core_ControlTerminaleBeginFramState(void) {
  MSH_CoreSetTerminal(MSH_Core_TerminalState_TERM_BEG); //!!
}

void MSH_Core_ControlTerminaleEndFramState(void) {
  MSH_CoreSetTerminal(MSH_Core_TerminalState_TERM_END); //!!
}

inline void MSH_Core_ResetEncoderStepsEvent(void) {
  static int RefenceCount = Encoder_GetReferenceCount();
  static int ZeroCrossPointCord;

          if (Encoder_GetReferenceCount() == 1 && !IsZeroPointEmmit) {
          FrameDurationTimeStamps_FixReferencePoint();
          Encoder_ResetStepsCount();
          IsZeroPointEmmit = true; 
          }

}


inline void MSH_Core_ResetEncoderStepsEventMotionFromBackward(void) {
  static int RefenceCount = Encoder_GetReferenceCount();
  static int ZeroCrossPointCord;
          if (Encoder_GetReferenceCount() == 7 && !ZeroCrossPointCord) {
          FrameDurationTimeStamps_FixReferencePoint();
          Encoder_ResetStepsCount();
          ZeroCrossPointCord = true;
          }
    
}


inline void MSH_Core_SensorTriggerEvent(SensorWord Sense) {

  if (EngineStatus.Bits.MOT_STATUS == MOT_STATUSE_Stopped) {
    _MSH_Core_Params.Terminal &= ~MSH_Core_TerminalState_TERM_ON;
     DrivrTimeOut_StopCounter();
  } else {
    _MSH_Core_Params.Terminal |= MSH_Core_TerminalState_TERM_ON;
  }
  
  if (EngineStatus.Bits.MOT_STATUS == MOT_STATUSE_Stopped &&
      Sense.Bits.DP0 == 1 && Sense.Bits.DP1 == 1) {
      MSH_Core_ControlTerminaleDriveStateStoped();
  }
  
  if(DrivrTimeOut_TimeisExpiried())
  {
     cSPIN_Hard_Stop(); 
  }
  
  if (EngineStatus.Bits.MOT_STATUS == MOT_STATUSE_Deceleration && !BrakeLatch) {
    PositionBeginBraking = cSPIN_Get_ABS_Position();
    BrakeLatch = 1;
  }
  
  if (EngineStatus.Bits.MOT_STATUS == MOT_STATUSE_Stopped && BrakeLatch) {
    BrakeLatch = 0;
    PositionEndBraking = cSPIN_Get_ABS_Position();
    TotalMicroStepPath = 0;
    if (PositionEndBraking > PositionBeginBraking) {
      TotalMicroStepPath = PositionEndBraking - PositionBeginBraking;
    }

    if (PositionEndBraking < PositionBeginBraking) {
      TotalMicroStepPath = PositionBeginBraking - PositionEndBraking;
    }
  }
  
  if (EngineStatus.Bits.MOT_STATUS != MOT_STATUSE_Stopped) {
    if ((MotorPosition >= PositionToStop && EngineStatus.Bits.DIR == FWD) ||
        (MotorPosition <= PositionToStop && EngineStatus.Bits.DIR == REV)) {
      HardStopBrakeStatge = 1;
      cSPIN_Hard_Stop();
   
    }
    
    if (Sense.Bits.DP0 == 1 && Sense.Bits.DP1 == 0 &&
        EngineStatus.Bits.DIR == 1) {
      if (ModuleMode == MSH_Core_mode_module_Scaner) {
        Timer_TDI_Impulse_StartImpulse(100);
      }
    }

      if (Sense.Bits.DP1 == 1) {
      MSH_Core_ControlTerminaleEndFramState();
      _MSH_Core_Params.Terminal |= MSH_Core_TerminalState_TERM_END;
    } else {
      _MSH_Core_Params.Terminal &= ~MSH_Core_TerminalState_TERM_END;
    }
    
    if (Sense.Bits.DP0 == 1) {
      _MSH_Core_Params.Terminal |= MSH_Core_TerminalState_TERM_BEG;
    } else {
      _MSH_Core_Params.Terminal &= ~MSH_Core_TerminalState_TERM_BEG;
    }
      
    
     if(ColimatorJumperState == ColimatorJumpersState_ReveceDirection 
         &&  ModuleMode == MSH_Core_mode_module_Collimator)
        {
           
      if (Sense.Bits.DP0 == 1 && Sense.Bits.DP1 == 0 &&
      EngineStatus.Bits.DIR == 1) {
      Encoder_ResetReferenceCount();
      IsZeroPointEmmit = false;
      Timer_TDI_Impulse_ResetLatch();
      
      
      if (!HardStopBrakeStatge) {
        if (cSPIN.SensorOffset > 1) {
          cSPIN_Soft_Stop();
        } else {
          cSPIN_Hard_Stop();
        }
      }
    }
    
    if (Sense.Bits.DP0 == 0 && Sense.Bits.DP1 == 1 &&
        EngineStatus.Bits.DIR == 0) {
      FrameDurationTimeStampsKy5_EndDrive();
      HV_Impulse_TurnOffHV();
      if (!HardStopBrakeStatge) {
        if (cSPIN.SensorOffset > 1) {
          cSPIN_Soft_Stop();
        } else {
          cSPIN_Hard_Stop();
        }
      }
    }
              
           
    }
    else
    {
        if (Sense.Bits.DP0 == 0 && Sense.Bits.DP1 == 1 &&
        EngineStatus.Bits.DIR == 1) {
      FrameDurationTimeStampsKy5_EndDrive();
      HV_Impulse_TurnOffHV();
      if (!HardStopBrakeStatge) {
        if (cSPIN.SensorOffset > 1) {
          cSPIN_Soft_Stop();
        } else {
          cSPIN_Hard_Stop();
        }
      }
    }
    
    if (Sense.Bits.DP0 == 1 && Sense.Bits.DP1 == 0 &&
        EngineStatus.Bits.DIR == 0) {
      Encoder_ResetReferenceCount();
      IsZeroPointEmmit = false;
      Timer_TDI_Impulse_ResetLatch();
      if (!HardStopBrakeStatge) {
        if (cSPIN.SensorOffset > 1) {
          cSPIN_Soft_Stop();
        } else {
          cSPIN_Hard_Stop();
        }
      }
    }
      
    }
    
   
  }
}

void MSH_Core_PrepareModule(void) {
  _FlashEx = FlashIO();
  MSH_Core_CheckInitilaSensor();
  if (MSH_Core_ReadSetupFromFlash(cSPIN)) {
    cSPIN_Hard_HiZ();
    cSPIN_Registers_Set(&cSPIN);
    MSH_Core_SetLookAtSensor(cSPIN.LookAtSwitch);
    cSPIN_Hard_Stop();
  }
}

void MSH_Core_WriteSetupInFlash(cSPIN_RegsStruct_TypeDef &cSPIN) {
  cSPIN.CheckWord = 0x9206;
  _FlashEx.Write(AddresSPIN, &cSPIN, 1);
}

bool MSH_Core_ReadSetupFromFlash(cSPIN_RegsStruct_TypeDef &cSPIN) {
  bool Res = false;
  _FlashEx.Read(AddresSPIN, &cSPIN, 1);
  if (cSPIN.CheckWord == 0x9206) {
    Res = true;
    cSPIN_Hard_Stop();
  }
  return Res;
}

void MSH_CoreSetTerminal(uint8_t Term) { _MSH_Core_Params.Terminal = Term; }

void MSH_Core_SetLookAtSensor(uint8_t LookAt) { LookAtSensor = LookAt; }

uint8_t MSH_Core_GetLookAtSensor(void) { return LookAtSensor; }

void MSH_Core_SetTerminalState_TERM_ON(void) {
  _TerminalBits._Bits.Terminal_ON = 1;
}

void MSH_Core_SetTerminalState_TERM_BEG(void) {
  _TerminalBits._Bits.Terminal_BEG = 1;
}

void MSH_Core_SetTerminalState_TERM_END(void) {
  _TerminalBits._Bits.Terminal_END = 1;
}

void MSH_Core_ResetTerminalState_TERM_ON(void) {
  _TerminalBits._Bits.Terminal_ON = 0;
}

void MSH_Core_ResetTerminalState_TERM_BEG(void) {
  _TerminalBits._Bits.Terminal_BEG = 0;
}

void MSH_Core_ResetTerminalState_TERM_END(void) {
  _TerminalBits._Bits.Terminal_END = 0;
}

void MSH_Core_DellayDriveTimerInit(void) {
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM7, ENABLE);
  TIM_DeInit(TIM7);
  TIM_TimeBaseInitTypeDef Timer_Intit;

  Timer_Intit.TIM_ClockDivision = TIM_CKD_DIV1; // 21Mhz
  Timer_Intit.TIM_CounterMode = TIM_CounterMode_Up;
  Timer_Intit.TIM_Period = 65535;
  Timer_Intit.TIM_Prescaler = 42000;
  Timer_Intit.TIM_RepetitionCounter = 0;
  TIM_TimeBaseInit(TIM7, &Timer_Intit);
}

inline void MSH_Core_DellayDriveEvent(void) {
  TIM_Cmd(TIM7, ENABLE);
}

inline void MSH_Core_DellayDriveEventAction(void) {
  if (EngineStatus.Bits.MOT_STATUS == MOT_STATUSE_Stopped) {
    __disable_interrupt();
    FrameDurationTimeStamps_FixBeginDrive();
    uint32_t Steps = (uint32_t)MSH_Core_GetStepsValue();
    PositionToStop = cSPIN_GetScaledPositionToStop(Steps);
    MSH_Core_PreepareForRun();
    MSH_Core_ControlTerminaleDriveState();
   
    uint16_t Velocity = 0;
    uint8_t VelocityL = MSH_Core_GetVelocityLow();
    uint8_t VelocityH = MSH_Core_GetVelocityHigh();
    Bit7ToWord14(&Velocity, &VelocityL, &VelocityH);
    
    if(ColimatorJumperState == ColimatorJumpersState_ReveceDirection 
         &&  ModuleMode == MSH_Core_mode_module_Collimator)
    {
       PositionToStop *= -1; 
       Encoder_ChangeDirrection(Encoder_Direction_Sign_CWW); 
       cSPIN_Run(REV, Speed_Steps_to_Par(Velocity));
    }
    else
    {
        Encoder_ChangeDirrection(Encoder_Direction_Sign_CW);
        cSPIN_Run(FWD, Speed_Steps_to_Par(Velocity));
    }
    
    DrivrTimeOut_StartCounter();
    __enable_interrupt();
  }
}

inline void MSH_Core_CaptureFrameExternalTrigger(void) {}

void EXTI9_5_IRQHandler(void) {
  if (EXTI_GetITStatus(EXTI_Line7)) {
    EXTI_ClearITPendingBit(EXTI_Line7);
    Encoder_CheckReferenceSatte();
  }
  if (EXTI_GetITStatus(EXTI_Line9)) {
    EXTI_ClearITPendingBit(EXTI_Line9);
    Encoder_CheckFiniteState();
  }
}

void MSH_Core_ResetCnt(void) { ItCountAC = 0; }

void MSH_Core_SetNegativeSign(void) {
  _MSH_Core_Params.Terminal |= MSH_Core_TerminalState_NEGATIVE_COUNTER;
  if (_MSH_Core_Params.Terminal & MSH_Core_TerminalState_ERROR_COUNTER) {
    _MSH_Core_Params.Terminal &= ~MSH_Core_TerminalState_POSITIVE_COUNTER;
  }
}

void MSH_Core_SetPositiveign(void) {
  _MSH_Core_Params.Terminal |= MSH_Core_TerminalState_POSITIVE_COUNTER;
  if (_MSH_Core_Params.Terminal & MSH_Core_TerminalState_ERROR_COUNTER) {
    _MSH_Core_Params.Terminal &= ~MSH_Core_TerminalState_NEGATIVE_COUNTER;
  }
}

void MSH_Core_CheckInitilaSensor(void) {
  CheckSensors();
  SensorWord Sense = GetStatusSensors();
   if(ColimatorJumperState == ColimatorJumpersState_ReveceDirection 
         &&  ModuleMode == MSH_Core_mode_module_Collimator)
  { 
   if (Sense.Bits.DP0 == 1 && Sense.Bits.DP1 == 0) {
   MSH_Core_ControlTerminaleEndFramState();
  }
  if (Sense.Bits.DP0 == 0 && Sense.Bits.DP1 == 1) {
    MSH_Core_ControlTerminaleBeginFramState();
    Encoder_ResetReferenceCount();
    IsZeroPointEmmit = false;
    Timer_TDI_Impulse_ResetLatch();
  }
    
  }
  else
  {
    
  if (Sense.Bits.DP0 == 0 && Sense.Bits.DP1 == 1) {
    MSH_Core_ControlTerminaleEndFramState();
  }
  if (Sense.Bits.DP0 == 1 && Sense.Bits.DP1 == 0) {
    MSH_Core_ControlTerminaleBeginFramState();
    Encoder_ResetReferenceCount();
    IsZeroPointEmmit = false;
    Timer_TDI_Impulse_ResetLatch();
  }
    
  }
  
}

MSH_Core_mode_module MSH_Core_GetModeModule(void) { return ModuleMode; }

void MSH_Core_CheckModePins(void) {
  if (GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_10) == SET ||
      GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_3) == SET) {
    ModuleMode = MSH_Core_mode_module_Collimator;
    _DeviceAdress = MSH_Core_DeviceAdress_Collimator;
     SetDeviceID(MSH_Core_module_ID_Number_Collimator);
  } else {
    ModuleMode = MSH_Core_mode_module_Scaner;
    _DeviceAdress = MSH_Core_DeviceAdress_Scanner;
    SetDeviceID(MSH_Core_module_ID_Number_Scaner);
  }
}

void MSH_Core_ModluleModeSelectorInit(void) {
  GPIO_InitTypeDef _ModeSelector;
  RCC_APB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
  _ModeSelector.GPIO_Mode = GPIO_Mode_IN;
  _ModeSelector.GPIO_Pin = GPIO_Pin_10;
  _ModeSelector.GPIO_PuPd = GPIO_PuPd_UP;
  _ModeSelector.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(GPIOA, &_ModeSelector);
  _ModeSelector.GPIO_Mode = GPIO_Mode_IN;
  _ModeSelector.GPIO_Pin = GPIO_Pin_3;
  _ModeSelector.GPIO_PuPd = GPIO_PuPd_UP;
  _ModeSelector.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(GPIOC, &_ModeSelector);
  MSH_Core_CheckModePins();
}

void MSH_Core_ExternalTriggerEvent(void) {
  if (StateMS._Bits.External_Trigger) {
    WindowFilterKy.Put(GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_8));
    int SumS0 = WindowFilterKy.GetAdd(0, 3);
    int SumS1 = WindowFilterKy.GetAdd(4, 7);
    if (SumS0 >= 3 && SumS1 <= 1) {
      if (!StateMS._Bits.CCD_Mode_CCD && StateMS._Bits.CCD_Mode_TDI &&
          !Trigered) {
        FrameDurationTimeStamps_InitCounter();
        Trigered = true;
        DriveFrame = false;
        HvIsTurnOff = false;
        MoveFlag = true;
        SoftBrakeStage = 0;
        HardStopBrakeStatge = 0;
        uint32_t Steps = 12000;
        PositionToStop = PositionTurnOffHV - DecFactor;
        PositionBeginSoftBrake = PositionTurnOffHV;
        if (_MSH_Core_Params.DellayAcc > 0) {
          TIM_SetCounter(TIM7, 0);
          TIM_Cmd(TIM7, ENABLE);
        }
      }
      if (StateMS._Bits.CCD_Mode_CCD && !StateMS._Bits.CCD_Mode_TDI &&
          !Trigered) {
        FrameDurationTimeStamps_InitCounter();
        Trigered = true;
        HvIsTurnOff = false;
        CCD_Trigger_Enable();
      }
    }
    if (SumS0 <= 1 && SumS1 >= 3) {
      if (!StateMS._Bits.CCD_Mode_CCD && StateMS._Bits.CCD_Mode_TDI &&
          Trigered) {
        FrameDurationTimeStamps_FixKy5_FallingEdge();
        Trigered = false;
        TestPointOff();
      }
      if (StateMS._Bits.CCD_Mode_CCD && !StateMS._Bits.CCD_Mode_TDI &&
          Trigered) {
        FrameDurationTimeStamps_FixKy5_FallingEdge();
        Trigered = false;
        CCD_Trigger_Disable();
      }
    }
  }
}

void TIM3_IRQHandler(void) {
  if (TIM_GetITStatus(TIM3, TIM_IT_Update) != RESET) {
    TIM_ClearITPendingBit(TIM3, TIM_IT_Update);
       if(EngineStatus.Bits.MOT_STATUS == MOT_STATUSE_Stopped){
        MSH_Core_ExternalTriggerEvent();
    }
  }
}

void MSH_Core_Drive_TDI_Mode(void) {
  if (!StateMS._Bits.CCD_Mode_CCD && StateMS._Bits.CCD_Mode_TDI && Trigered) {
    if (_MSH_Core_Params.DellayAcc == 0 && !DriveFrame) {
      OffSVD2();
      TestPointOn();
      MSH_Core_DellayDriveEventAction();
      DriveFrame = true;
    }
    if (_MSH_Core_Params.DellayAcc > 0 && !DriveFrame) {
      if (TIM_GetCounter(TIM7) == _MSH_Core_Params.DellayAcc*5) {
        TestPointOn();
        MSH_Core_DellayDriveEventAction();
        TIM_Cmd(TIM7, DISABLE);
        TIM_SetCounter(TIM7, 0);
        DriveFrame = true;
      }
    }
  }
}

MSH_Core_DeviceAdress MSH_Core_GetDeviceAddres(void) { return _DeviceAdress; }

void MSH_Core_EmeragancyStopMotor(void) {
  if (iWD_Fault_is_Detected()) {
    cSPIN_SW_HartStopEngine();
  }
  iWD_Init();
}

void MSH_Core_ModuleParking(void) {
  OnSVD2();
  DisableUSART();
  MSH_Core_SetupParkingSpeed();
  if(ColimatorJumperState == ColimatorJumpersState_ReveceDirection 
         &&  ModuleMode == MSH_Core_mode_module_Collimator)
  {
    Encoder_SetMode(Encoder_Mode_Invers);
    MSH_Core_ModuleParkingBeginFrame();
    MSH_Core_ModuleParkingEndFrame();
  }
  else
  {
    Encoder_SetMode(Encoder_Mode_Normal);
    MSH_Core_ModuleParkingEndFrame();
    MSH_Core_ModuleParkingBeginFrame();
  }
  
  EnableUSART();
}

void MSH_Core_ModuleParkingBeginFrame(void) {
  _ParkingSetup.Direction = 0;
  MSH_Core_MotionToSensor(&_ParkingSetup);
}

void MSH_Core_ModuleParkingEndFrame(void) {
  _ParkingSetup.Direction = 1;
  MSH_Core_MotionToSensor(&_ParkingSetup);
}

void MSH_Core_SetupParkingSpeed(void) {
  switch (ModuleMode) {
  case MSH_Core_mode_module_Collimator: {
    _ParkingSetup.SpeedBegin = 50;
     if(ColimatorJumperState == ColimatorJumpersState_ReveceDirection)
     {
       
       _ParkingSetup.SpeedBegin = 20;
       _ParkingSetup.SpeedEnd  = cSPIN.SpeedColimatorInv;
      _ParkingSetup.TimeAcceleration = 400;
      _ParkingSetup.TimeBrake = 500;
      _ParkingSetup.StepCount = 60000;
       
     }
     else
     {
       
      _ParkingSetup.SpeedEnd  = cSPIN.SpeedColimator;
      _ParkingSetup.TimeAcceleration = 400;
      _ParkingSetup.TimeBrake = 500;
      _ParkingSetup.StepCount = 60000;
       
     }
    
    break;
  }
  case MSH_Core_mode_module_Scaner: {
    _ParkingSetup.SpeedBegin = 50;
    _ParkingSetup.SpeedEnd  = cSPIN.SpeedScaner;
    _ParkingSetup.TimeAcceleration = 400;
    _ParkingSetup.TimeBrake = 500;
    _ParkingSetup.StepCount = 60000;
    break;
  }
  }
}

void MSH_Core_MotionToSensor(MSH_Core_ParckingParametrs *Params) {
  uint16_t MotorState = 0;
  cSPIN_Hard_HiZ();
  int Offset = cSPIN.SensorOffset;
  IsZeroPointEmmit = true;
  if (Offset == 0)
    Offset = 1;
  int StepCount = _ParkingSetup.StepCount;
  
  if (Params->Direction == REV) {
    StepCount *= -1;
    Encoder_ChangeDirrection(Encoder_Direction_Sign_CWW);
  }
  cSPIN_RunEx(&Params->SpeedBegin, &Params->SpeedEnd, &Params->TimeAcceleration,
              &Params->TimeBrake, &Params->Direction, &Offset);
     DrivrTimeOut_StartCounter();
  do {
    MSH_Core_ResetEncoderStepsEvent();
    MotorState = (cSPIN_Get_Status() >> 5) & 0x3;
    CheckSensors();
    SensorWord Sense = GetStatusSensors();
    EngineStatus.Bits = cSPIN_Get_StatusBF();
    if( ModuleMode == MSH_Core_mode_module_Collimator 
       && ColimatorJumperState == ColimatorJumpersState_MainDirection 
       || ModuleMode == MSH_Core_mode_module_Scaner)
    {
    if (Params->Direction == 1) {
      if (Sense.Bits.DP0 == 0 && Sense.Bits.DP1 == 1 &&
          EngineStatus.Bits.DIR == 1) {
        MSH_Core_ControlTerminaleEndFramState();
        Encoder_ResetReferenceCount();
        if (cSPIN.SensorOffset > 1) {
          cSPIN_Soft_Stop();
        } else {
          cSPIN_Hard_Stop();
        }
      }
    }
    if (Params->Direction == 0) {
      MSH_Core_ResetEncoderStepsEventMotionFromBackward();
      if (Sense.Bits.DP0 == 1 && Sense.Bits.DP1 == 0 &&
          EngineStatus.Bits.DIR == 0) {
        MSH_Core_ControlTerminaleBeginFramState();
        Encoder_ResetReferenceCount();
        IsZeroPointEmmit = false;
        Timer_TDI_Impulse_ResetLatch();
        if (cSPIN.SensorOffset > 1) {
          cSPIN_Soft_Stop();
        } else {
          cSPIN_Hard_Stop();
        }
      }
    }
    }
    
    if(ModuleMode == MSH_Core_mode_module_Collimator 
       && ColimatorJumperState == ColimatorJumpersState_ReveceDirection)
    {
      if (Params->Direction == 1) {
      if (Sense.Bits.DP0 == 1 && Sense.Bits.DP1 == 0 &&
          EngineStatus.Bits.DIR == 1) {
        MSH_Core_ControlTerminaleBeginFramState();
        MSH_Core_ResetEncoderStepsEventMotionFromBackward();
        if (cSPIN.SensorOffset > 1) {
          cSPIN_Soft_Stop();
        } else {
          cSPIN_Hard_Stop();
        }
      }
    }
    if (Params->Direction == 0) {
       Encoder_ResetReferenceCount();
      if (Sense.Bits.DP0 == 0 && Sense.Bits.DP1 == 1 &&
          EngineStatus.Bits.DIR == 0) {
        MSH_Core_ControlTerminaleEndFramState();
        Encoder_ResetReferenceCount();
        IsZeroPointEmmit = false;
        Timer_TDI_Impulse_ResetLatch();
        if (cSPIN.SensorOffset > 1) {
          cSPIN_Soft_Stop();
        } else {
          cSPIN_Hard_Stop();
        }
      }
    }
       
    }
    
    if(DrivrTimeOut_TimeisExpiried()){
        cSPIN_Hard_Stop();
    }
    
    if ((cSPIN_Get_ABS_Position() >= StepCount && Params->Direction == FWD) ||
        (cSPIN_Get_ABS_Position() <= StepCount && Params->Direction == REV)) {
      cSPIN_Hard_Stop();
    }
    iWD_RefreshCountet();
  } while (MotorState != 0);
    DrivrTimeOut_StopCounter();
    Timer_WaitMilisec(500);
};

void MSH_Core_MotionControl(void) {
  uint16_t MotorState = (cSPIN_Get_Status() >> 5) & 0x3;
  if (MotorState == 0) {
    float Dec = 0;
    uint8_t VelocityL = MSH_Core_GetVelocityLow();
    uint8_t VelocityH = MSH_Core_GetVelocityHigh();
    uint16_t Velocity = 0;
    uint8_t RampH = MSH_CoreGetRampHigh();
    uint8_t RampL = MSH_CoreGetRampLow();
    uint16_t Ramp = 0;
    uint16_t InitVelosity = MSH_CoreGetVelocityInit();
    Bit7ToWord14(&Ramp, &RampL, &RampH);
    Bit7ToWord14(&Velocity, &VelocityL, &VelocityH);
  }
}

int  CheckModeBits(uint8_t Control)
{
  
}
void MSH_Core_CheckColimatorJumpers(void)
{
  
  ColimatorJumperState = ColimatorJumpersControl_GetJumperState();
  
}
