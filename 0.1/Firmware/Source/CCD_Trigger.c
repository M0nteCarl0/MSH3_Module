#include "CCD_Trigger.h"
void CCD_Trigger_InitGPIOInitDeffault(void) {
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
  GPIO_InitTypeDef _GPIO;
  GPIO_StructInit(&_GPIO);
  _GPIO.GPIO_Mode = GPIO_Mode_OUT;
  _GPIO.GPIO_Pin = GPIO_Pin_8;
  _GPIO.GPIO_PuPd = GPIO_PuPd_DOWN;
  _GPIO.GPIO_Speed = GPIO_Speed_2MHz;
  GPIO_Init(GPIOA, &_GPIO);
  void CCD_Trigger_Disable();
}

/******************************************************************************/
void CCD_Trigger_Enable(void) { GPIO_SetBits(GPIOA, GPIO_Pin_8); }
/******************************************************************************/
void CCD_Trigger_Disable(void) { GPIO_ResetBits(GPIOA, GPIO_Pin_8); }
/******************************************************************************/
