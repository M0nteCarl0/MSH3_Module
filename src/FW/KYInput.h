#pragma once
#include "stm32f4xx.h"
#ifndef _KYIO
#define _KYIO



typedef struct KY_Assigment
{ 
  uint32_t  RCC_Sensor;
  GPIO_TypeDef* GPIO_Port;
  uint32_t  InputPin;
  uint32_t  State;
}KY_Assigment;

typedef struct KY_BackyState
{
  uint8_t HVEmited:1;
  uint8_t KySynchroDetected:1;
}KY_BackyState;

#ifdef __cplusplus
 extern "C" {
#endif 
void InitKYInput(KY_Assigment KYInput);
void InitKYOut(KY_Assigment KYInput);
uint32_t ReadKYInput(KY_Assigment KYInput);
void WriteKYOut(KY_Assigment KYOut,uint8_t Val);
void InitKYIO(void);
void CheckSignalFromKY(void);
bool KyTrigerd(void);
void TurnOnHV(void);
void TurnOffHV(void); 
bool HVisOn(void);
uint32_t GetKYOutputState(void);

void KyPollingEvent(void);
//void  EXTI9_5_IRQHandler(void);

void SetKYBackyStateHvEmited(void);
void ResetKYBackyStateHvEmited(void);
bool IsKYBackyStateHvEmited(void);

void SetKYBackyStateKySynchroDetected(void);
void ResetKYBackyStateKySynchroDetected(void);

bool IsKYBackyStateKySynchroDetected(void);


void KyRaiseEdgeSynchroEvent(void);
void KYFailingEdgeSynchroEvent(void);
void KyEdgeActionProvider(void);
void CompareCurentAndLastStateKyInput(void);

void SaveCurrentKyPinState(void);
void StorePreviousState(void);
#ifdef __cplusplus
 }
#endif 
 
#endif