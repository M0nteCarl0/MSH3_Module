#include "HV_Trigger.h"
void HV_Trigge_Init(void) {

  GPIO_InitTypeDef _HV_Trigge;
  RCC_APB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
  _HV_Trigge.GPIO_Mode = GPIO_Mode_IN;
  _HV_Trigge.GPIO_Pin = GPIO_Pin_8;
  _HV_Trigge.GPIO_PuPd = GPIO_PuPd_NOPULL;
  _HV_Trigge.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(GPIOC, &_HV_Trigge);
}

uint8_t HV_Trigge_ReadIbput(void) {
  return GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_8);
}

bool HV_Trigge_RissingEdgeDeteced(void) { return HV_Trigge_ReadIbput() != 0; }
