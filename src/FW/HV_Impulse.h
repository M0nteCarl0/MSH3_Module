#include "stm32f4xx.h"
#pragma once
typedef struct  HV_Impulse_Assigment
{ 
  uint32_t  RCC_HV_Impulse;
  GPIO_TypeDef* GPIO_Port;
  uint32_t  InputPin;
  uint32_t  State;
}HV_Impulse_Assigment;
#ifdef __cplusplus
 extern "C" {
#endif   
void  HV_Impulse_InitDeffault(void);
void  HV_Impulse_Init(HV_Impulse_Assigment Assigment);
void  HV_Impulse_TimerInit(void);
void  HV_Impulse_TurnOffHV(void);
void HV_Impulse_Begin(void);
void HV_Impulse_End(void);
void HV_Impulse_Monitor (void);
void  TIM8_UP_TIM13_IRQHandler(void);
#ifdef __cplusplus
 }
#endif   