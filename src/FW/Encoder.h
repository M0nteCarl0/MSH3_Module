#include "Encoder_deinitions.h"
#ifdef __cplusplus
 extern "C" {
 #endif   
void Encoder_CheckFiniteState(void);
void Encoder_CheckReferenceSatte(void);
void Encoder_CheckTiks(void);
void Encoder_Init(Encoder_InitTypedef*  Encoder_InitType);
int Encoder_GetSteps(void);
Encoder_Direction  Encoder_GetDirection(void);
uint32_t Encoder_ReadPins(void);
void Encoder_Feel_Counter(void);
uint16_t Encoder_GetReferenceCount(void);
void Encoder_ResetReferenceCount(void);
void Encoder_ResetStepsCount(void);
void Encoder_ChangeDirrection(Encoder_Direction_Sign _Sign);

void Encoder_InitHWRead(void);
void EXTI15_10_IRQHandler(void);
void  Encoder_LegacyRead(void); 
void Encoder_LEToBE(short* Source,short* Dest);
short Encoder_GetStepsCount(void);
void  Encoder_ResetDebufCnt(void);
void Encoder_GetValue(short* StepConv,uint8_t* Terminal,uint8_t* LowPart,uint8_t* HighPart);
void  Encoder_ResetDebufCnt(void);
#ifdef __cplusplus
 }
 #endif   