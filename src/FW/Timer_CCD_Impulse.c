#include "Timer_CCD_Impulse.h"
Timer_TDI_Impulse_GPIO _GPIO;
bool ImpulseEnd; 
/******************************************************************************/
void Timer_TDI_Impulse_Init(void)
{
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4,ENABLE);
  TIM_TimeBaseInitTypeDef _TimerSetup;
  _TimerSetup.TIM_ClockDivision     = TIM_CKD_DIV1;
  _TimerSetup.TIM_CounterMode       = TIM_CounterMode_Up;
  _TimerSetup.TIM_Period            = 0;
  _TimerSetup.TIM_Prescaler         = 4200;
  _TimerSetup.TIM_RepetitionCounter = 0;
   TIM_TimeBaseInit(TIM4,&_TimerSetup);
   TIM_ClearITPendingBit(TIM4,TIM_IT_Update);
   TIM_Cmd(TIM4,DISABLE);
}
/******************************************************************************/
void Timer_TDI_Impulse_StartImpulse( uint32_t Time)
{

  GPIO_SetBits(_GPIO.Port,_GPIO.Pin);
  TIM_SetAutoreload(TIM4,Time);
  ImpulseEnd = false;
  TIM_Cmd(TIM4,ENABLE);
}
/******************************************************************************/
bool Timer_TDI_Impulse_ImpulseRnded(void)
{
 return ImpulseEnd;
}
/******************************************************************************/
void TIM4_IRQHandler(void)
{
  if(TIM_GetITStatus(TIM4,TIM_IT_Update)!=RESET)
  {  
     ImpulseEnd = true;
     GPIO_ResetBits(_GPIO.Port,_GPIO.Pin);
     TIM_ClearITPendingBit(TIM4,TIM_IT_Update);
  };
}
/******************************************************************************/
void Timer_TDI_Impulse_InitGPIO(Timer_CCD_Impulse_GPIO* GPIO)
{
  
  
  
}
/******************************************************************************/
void Timer_TDI_Impulse_InitGPIODefault (void)
{
  
  
  
}
