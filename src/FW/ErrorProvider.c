#include "ErrorProvider.h"
#include <stdio.h>

void NMI_Handler(void) {}

void HardFault_Handler(void) {}

void MemManage_Handler(void) {}

void BusFault_Handler(void) {}

void UsageFault_Handler(void) {}

void SetSensorWarning(void) {}

void ResetSensorWarning(void) {}

void SetEngineWarning(void) {}

void ResetEngineWarning(void) {}

uint8_t GetWarningWord(void) { return 0; };

void CheckEngine(void) {}
