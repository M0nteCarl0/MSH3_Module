//------------------------------------------------------------------------------
//    Novikov
//    10.02.2015
//------------------------------------------------------------------------------
#ifndef __MYUSARTHARD_H
#define __MYUSARTHARD_H

#include "stm32f4xx.h"
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
#ifdef __cplusplus
extern "C" {
#endif
  
void Init_UsartHard    ();

void My_Initiate_Usart (void);
void GPIO_Configuration(void);
void NVIC_Configuration(void);
void  USART3_IRQHandler(void);
void EnableRS485(void);
void DisableRS485(void);
void ReceiveData(void);
void SendData(void);
void TransferComplete(void);
void DisableUSART(void);
void EnableUSART(void);
//#define USARTy_IRQHandler        USART3_IRQHandler

#ifdef __cplusplus
}
#endif
//------------------------------------------------------------------------------


extern USART_TypeDef           * USARTy;

#endif