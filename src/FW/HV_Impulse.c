#include "HV_Impulse.h"
#include "FrameDurationTimeStamps.h"
HV_Impulse_Assigment LAssigment;
bool Trigerd = false;

void HV_Impulse_InitDeffault(void) {
  LAssigment.GPIO_Port = GPIOB;
  LAssigment.InputPin = GPIO_Pin_5;
  LAssigment.RCC_HV_Impulse = RCC_AHB1Periph_GPIOB;
  HV_Impulse_Init(LAssigment);
  HV_Impulse_TimerInit();
  HV_Impulse_End();
}

void HV_Impulse_Begin(void) {
  GPIO_WriteBit(LAssigment.GPIO_Port, LAssigment.InputPin, Bit_RESET);
}

void HV_Impulse_End(void) {
  GPIO_WriteBit(LAssigment.GPIO_Port, LAssigment.InputPin, Bit_SET);
}

void HV_Impulse_Monitor(void) {}

void HV_Impulse_Init(HV_Impulse_Assigment Assigment) {
  RCC_AHB1PeriphClockCmd(Assigment.RCC_HV_Impulse, ENABLE);
  GPIO_InitTypeDef _GPIO;
  GPIO_StructInit(&_GPIO);
  _GPIO.GPIO_Mode = GPIO_Mode_OUT;
  _GPIO.GPIO_Pin = Assigment.InputPin;
  _GPIO.GPIO_PuPd = GPIO_PuPd_UP;
  _GPIO.GPIO_Speed = GPIO_Speed_2MHz;
  GPIO_Init(Assigment.GPIO_Port, &_GPIO);
  LAssigment = Assigment;
}

void HV_Impulse_TimerInit(void) {
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM13, ENABLE);
  TIM_TimeBaseInitTypeDef _TimerSetup;
  _TimerSetup.TIM_ClockDivision = TIM_CKD_DIV1;
  _TimerSetup.TIM_CounterMode = TIM_CounterMode_Up;
  _TimerSetup.TIM_Period = 500;
  _TimerSetup.TIM_Prescaler = 42;
  _TimerSetup.TIM_RepetitionCounter = 0;
  TIM_TimeBaseInit(TIM13, &_TimerSetup);
  TIM_ClearITPendingBit(TIM13, TIM_IT_Update);
  TIM_Cmd(TIM13, DISABLE);
  NVIC_InitTypeDef NVIC_InitStructure;
  // NVIC_PriorityGroupConfig (NVIC_PriorityGroup_1);
  NVIC_InitStructure.NVIC_IRQChannel = TIM8_UP_TIM13_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0; // 1;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
}

void HV_Impulse_TurnOffHV(void) {
  if (!Trigerd) {
    TIM_ClearITPendingBit(TIM13, TIM_IT_Update);
    FrameDurationTimeStamps_FixMsh3_hv_off();
    TIM_ITConfig(TIM13, TIM_IT_Update, ENABLE);
    NVIC_EnableIRQ(TIM8_UP_TIM13_IRQn);
    HV_Impulse_Begin();
    TIM_Cmd(TIM13, ENABLE);
    Trigerd = true;
  }
}

void TIM8_UP_TIM13_IRQHandler(void) {
  if (TIM_GetITStatus(TIM13, TIM_IT_Update) != RESET) {
    TIM_ClearITPendingBit(TIM13, TIM_IT_Update);
    HV_Impulse_End();
    TIM_Cmd(TIM13, DISABLE);
    Trigerd = false;
  }
}
