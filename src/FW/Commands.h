
#include "MyUsart.h"
#include "MyUsartHard.h"
enum CommadID
{
	CommadID_READ_HW_SW				  = 0x40,
   CommadID_WRITE_DRIVER_SETUP	  = 0x1,
   CommadID_REBOOT_MODULE          = 0x2,
   CommadID_DriveSteps             = 0x3,		
   CommadID_READ_DRIVER_SETUP		  = 0x41,
   CommadID_READ_DRIVER_ABS_POS	  = 0x44,
   CommadID_READ_EVENT_TIMES	     = 0x45,
   
};

enum CommadID_Two_Byte
{
    CommadID_Two_Byte_READ         = 0x81,
    CommadID_Two_Byte_TERMINAL     = 0xd7,   
    CommadID_Two_Byte_Status       = 0xd6,    
    CommadID_Two_Byte_StepsHi      = 0xd8,
    CommadID_Two_Byte_StepsLo      = 0xd9,
    CommadID_Two_Byte_VelocityHi   = 0xda,
    CommadID_Two_Byte_VelocityLo   = 0xdb,
    CommadID_Two_Byte_RampHi       = 0xdc,
    CommadID_Two_Byte_RampLo       = 0xdd, 
    CommadID_Two_Byte_CounterHi    = 0xde,
    CommadID_Two_Byte_CounterLo    = 0xdf,
    CommadID_Two_Byte_InitialVLo   = 0xbd,
    CommadID_Two_Byte_TimeDelayLo  = 0xbe,
    CommadID_Two_Byte_TurnOffHVLo  = 0xb8,
};
#ifdef __cplusplus
 extern "C" {
 #endif   
 uint16_t   Command_MakeCRC16 (uint8_t *Block, int Len);
 
 void       Command_ReadVersion(uint8_t *Buff, int N);
 void       Command_WriteSetupDriver(uint8_t *Buff, int N);
 void       Command_DriveMotion(uint8_t *Buff, int N);
 void       Command_ReadSetupDriver(uint8_t *Buff, int N);
 void       Command_RebootDevice(uint8_t *Buff, int N);;  
 void       Command_ProcessCommands(void);
 void       Command_CommandParser(uint8_t *Buff,uint8_t ComandID,int N);  
 void       Command_TwoCommandParser(uint8_t *Buff,int N);
 void       Command_TwoBytesRead(uint8_t *Buff);
 void       Command_MakeCRC_LegacyProtocol(uint8_t *Buff,int N);
 void       Command_IntToByte(int* Source,uint8_t* B1,uint8_t* B2,uint8_t* B3,uint8_t* B4,uint8_t* B5);
 void       Command_ByteToInt(int* Dest,uint8_t* B1,uint8_t* B2,uint8_t* B3,uint8_t* B4,uint8_t* B5);
 #ifdef  __cplusplus  
 }
 #endif

