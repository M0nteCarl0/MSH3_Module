#include "stm32f4xx.h"
#ifdef __cplusplus
extern "C" {
#endif 
typedef struct LatchEvents
{
  uint32_t EventID_Map;
} LatchEvents;


typedef struct EventsTimeStamps
{
  uint32_t BeginDrive;
  uint32_t ReferencePoint;
  uint32_t Msh3_hv_off;
  uint32_t Ky5_FallingEdge;
  uint32_t Ky5_EndDrive;
}EventsTimeStamps;

typedef enum LatchEventID
{
  LatchEventID_CLEAN                = 0x0,
  LatchEventID_BEGIN_DRIVE          = 0x1,
  LatchEventID_REFERENCE_POINT      = 0x2,
  LatchEventID_MSH_HV_OFF           = 0x4,
  LatchEventID_MSH_KY5_FALLING_EDGE = 0x8,
  LatchEventID_MSH_DRIVE_END        = 0x10,
}LatchEventID;


void  FrameDurationTimeStamps_InitCounter(void);
void  FrameDurationTimeStamps_FixBeginDrive(void);
void  FrameDurationTimeStamps_FixReferencePoint(void);
void  FrameDurationTimeStamps_FixMsh3_hv_off(void);
void  FrameDurationTimeStamps_FixKy5_FallingEdge(void);
void  FrameDurationTimeStampsKy5_EndDrive(void);
#ifdef __cplusplus
 }
#endif   