#include "MySPIN.h"
#include "Timer.h"
uint32_t ScaleFactor = 0;
uint32_t TimeouOperations = 10;
/******************************************************************************/
void cSPIN_Regs_Struct_Reset(cSPIN_RegsStruct_TypeDef *cSPIN_RegsStruct) {
  cSPIN_RegsStruct->ABS_POS = 0;
  cSPIN_RegsStruct->EL_POS = 0;
  cSPIN_RegsStruct->MARK = 0;
  cSPIN_RegsStruct->ACC = 0xAB;
  cSPIN_RegsStruct->DEC = 0xAA;
  cSPIN_RegsStruct->MAX_SPEED = 0xFF;
  cSPIN_RegsStruct->MIN_SPEED = 0x0;
  cSPIN_RegsStruct->FS_SPD = 0xFFFF;
  cSPIN_RegsStruct->TVAL_HOLD = 0xA;
  cSPIN_RegsStruct->TVAL_RUN = 0x2F;
  cSPIN_RegsStruct->TVAL_ACC = 0x2F;
  cSPIN_RegsStruct->TVAL_DEC = 0x2F;
  cSPIN_RegsStruct->T_FAST = 0x1;
  cSPIN_RegsStruct->TON_MIN = 10;
  cSPIN_RegsStruct->TOFF_MIN = 10;
  cSPIN_RegsStruct->OCD_TH = 0x8;
  cSPIN_RegsStruct->STEP_MODE = 0xF; // 1-16 Step
  cSPIN_RegsStruct->ALARM_EN = 0xFF;
  cSPIN_RegsStruct->CONFIG = 0x2E88;
}
/******************************************************************************/
void cSPIN_Registers_Set(cSPIN_RegsStruct_TypeDef *cSPIN_RegsStruct) {
  __disable_interrupt();
  cSPIN_Hard_HiZ();
  __disable_interrupt();
  cSPIN_Set_Param(cSPIN_ABS_POS, cSPIN_RegsStruct->ABS_POS);
  cSPIN_Set_Param(cSPIN_EL_POS, cSPIN_RegsStruct->EL_POS);
  cSPIN_Set_Param(cSPIN_MARK, cSPIN_RegsStruct->MARK);
  cSPIN_Set_Param(cSPIN_ACC, cSPIN_RegsStruct->ACC);
  cSPIN_Set_Param(cSPIN_DEC, cSPIN_RegsStruct->DEC);
  cSPIN_Set_Param(cSPIN_MAX_SPEED, cSPIN_RegsStruct->MAX_SPEED);
  cSPIN_Set_Param(cSPIN_MIN_SPEED, cSPIN_RegsStruct->MIN_SPEED);
  cSPIN_Set_Param(cSPIN_FS_SPD, cSPIN_RegsStruct->FS_SPD);
  cSPIN_Set_Param(cSPIN_TVAL_HOLD, cSPIN_RegsStruct->TVAL_HOLD);
  cSPIN_Set_Param(cSPIN_TVAL_RUN, cSPIN_RegsStruct->TVAL_RUN);
  cSPIN_Set_Param(cSPIN_TVAL_ACC, cSPIN_RegsStruct->TVAL_ACC);
  cSPIN_Set_Param(cSPIN_TVAL_DEC, cSPIN_RegsStruct->TVAL_DEC);
  cSPIN_Set_Param(cSPIN_T_FAST, cSPIN_RegsStruct->T_FAST);
  cSPIN_Set_Param(cSPIN_TON_MIN, cSPIN_RegsStruct->TON_MIN);
  cSPIN_Set_Param(cSPIN_TOFF_MIN, cSPIN_RegsStruct->TOFF_MIN);
  cSPIN_Set_Param(cSPIN_OCD_TH, cSPIN_RegsStruct->OCD_TH);
  cSPIN_Set_Param(cSPIN_STEP_MODE, cSPIN_RegsStruct->STEP_MODE);
  ScaleFactor = cSPIN_Get_ScalebyStep(cSPIN_RegsStruct->STEP_MODE);
  cSPIN_Set_Param(cSPIN_ALARM_EN, cSPIN_RegsStruct->ALARM_EN);
  cSPIN_Set_Param(cSPIN_CONFIG, cSPIN_RegsStruct->CONFIG);
  __enable_interrupt();
}
/******************************************************************************/
void cSPIN_ChipSelect(void) { GPIO_WriteBit(GPIOA, GPIO_Pin_4, Bit_RESET); }
/******************************************************************************/
void cSPIN_ChipDeselect(void) { GPIO_WriteBit(GPIOA, GPIO_Pin_4, Bit_SET); }
/******************************************************************************/
void cSPIN_Peripherals_Init(void) {
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1, ENABLE);
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource5, GPIO_AF_SPI1);
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource6, GPIO_AF_SPI1);
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource7, GPIO_AF_SPI1);
  GPIO_InitTypeDef SPI_GPIO;
  SPI_InitTypeDef Driver_SPI;
  SPI_StructInit(&Driver_SPI);
  GPIO_StructInit(&SPI_GPIO);
  SPI_GPIO.GPIO_Mode = GPIO_Mode_AF;
  SPI_GPIO.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7 | GPIO_Pin_5;
  GPIO_Init(GPIOA, &SPI_GPIO);
  SPI_GPIO.GPIO_Mode = GPIO_Mode_OUT;
  SPI_GPIO.GPIO_Pin = GPIO_Pin_4;
  SPI_GPIO.GPIO_OType = GPIO_OType_PP;
  SPI_GPIO.GPIO_PuPd = GPIO_PuPd_DOWN;
  SPI_GPIO.GPIO_Speed = GPIO_Speed_2MHz;
  GPIO_Init(GPIOA, &SPI_GPIO);
  SPI_GPIO.GPIO_Mode = GPIO_Mode_OUT;
  SPI_GPIO.GPIO_Pin = GPIO_Pin_5;
  SPI_GPIO.GPIO_OType = GPIO_OType_PP;
  SPI_GPIO.GPIO_PuPd = GPIO_PuPd_DOWN;
  SPI_GPIO.GPIO_Speed = GPIO_Speed_2MHz;
  GPIO_Init(GPIOC, &SPI_GPIO);
  Driver_SPI.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_2;
  Driver_SPI.SPI_Mode = SPI_Mode_Master;
  Driver_SPI.SPI_FirstBit = SPI_FirstBit_MSB;
  Driver_SPI.SPI_NSS = SPI_NSS_Soft;
  Driver_SPI.SPI_CRCPolynomial = 7;
  Driver_SPI.SPI_DataSize = SPI_DataSize_8b;
  Driver_SPI.SPI_CPOL = SPI_CPOL_High;
  Driver_SPI.SPI_CPHA = SPI_CPHA_2Edge;
  Driver_SPI.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
  SPI_Init(SPI1, &Driver_SPI);
  SPI_NSSInternalSoftwareConfig(SPI1, SPI_NSSInternalSoft_Set);
  SPI_Cmd(SPI1, ENABLE);
  cSPIN_ChipDeselect();
}
/******************************************************************************/
void inline cSPIN_Set_Param(cSPIN_Registers_TypeDef param, uint32_t value) {
  /* Send SetParam operation code to cSPIN */
  cSPIN_Write_Byte((uint8_t)cSPIN_SET_PARAM | (uint8_t)param);
  switch (param) {
  case cSPIN_ABS_POS:;
  case cSPIN_MARK:;
    /* Send parameter - byte 2 to cSPIN */
    cSPIN_Write_Byte((uint8_t)(value >> 16));
  case cSPIN_EL_POS:;
  case cSPIN_ACC:;
  case cSPIN_DEC:;
  case cSPIN_MAX_SPEED:;
  case cSPIN_MIN_SPEED:;
  case cSPIN_FS_SPD:;

  case cSPIN_CONFIG:;
  case cSPIN_STATUS:
    /* Send parameter - byte 1 to cSPIN */
    cSPIN_Write_Byte((uint8_t)(value >> 8));
  default:
    /* Send parameter - byte 0 to cSPIN */
    cSPIN_Write_Byte((uint8_t)(value));
    cSPIN_Write_Byte((uint8_t)(0x0));
  }
}

/******************************************************************************/
uint8_t cSPIN_Write_Byte(uint8_t byte) {
  cSPIN_ChipSelect();
  Timer_TraceBegin();
  uint32_t Time = 0;
  do {
    Time = Timer_TraceGetTime();
  } while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE) != SET ||
           (Time >= TimeouOperations));
  Timer_TraceEnd();
  Time = 0;
  SPI_I2S_SendData(SPI1, byte);

  Timer_TraceBegin();
  do {
    Time = Timer_TraceGetTime();
  } while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE) != SET ||
           (Time >= TimeouOperations));
  Timer_TraceEnd();
  uint8_t Rx = SPI_I2S_ReceiveData(SPI1);
  cSPIN_ChipDeselect();
  return Rx;
}
/******************************************************************************/
void cSPIN_Registers_ReadAll(cSPIN_RegsStruct_TypeDef *cSPIN_RegsStruct) {
  __disable_interrupt();

  cSPIN_RegsStruct->ABS_POS = cSPIN_Get_Param(cSPIN_ABS_POS);
  cSPIN_RegsStruct->EL_POS = cSPIN_Get_Param(cSPIN_EL_POS);
  cSPIN_RegsStruct->MARK = cSPIN_Get_Param(cSPIN_MARK);
  cSPIN_RegsStruct->ACC = cSPIN_Get_Param(cSPIN_ACC);
  cSPIN_RegsStruct->DEC = cSPIN_Get_Param(cSPIN_DEC);
  cSPIN_RegsStruct->MAX_SPEED = cSPIN_Get_Param(cSPIN_MAX_SPEED);
  cSPIN_RegsStruct->MIN_SPEED = cSPIN_Get_Param(cSPIN_MIN_SPEED);
  cSPIN_RegsStruct->FS_SPD = cSPIN_Get_Param(cSPIN_FS_SPD);
  cSPIN_RegsStruct->TVAL_HOLD = cSPIN_Get_Param(cSPIN_TVAL_HOLD);
  cSPIN_RegsStruct->TVAL_RUN = cSPIN_Get_Param(cSPIN_TVAL_RUN);
  cSPIN_RegsStruct->TVAL_ACC = cSPIN_Get_Param(cSPIN_TVAL_ACC);
  cSPIN_RegsStruct->TVAL_DEC = cSPIN_Get_Param(cSPIN_TVAL_DEC);
  cSPIN_RegsStruct->T_FAST = cSPIN_Get_Param(cSPIN_T_FAST);
  cSPIN_RegsStruct->TON_MIN = cSPIN_Get_Param(cSPIN_TON_MIN);
  cSPIN_RegsStruct->TOFF_MIN = cSPIN_Get_Param(cSPIN_TOFF_MIN);
  cSPIN_RegsStruct->OCD_TH = cSPIN_Get_Param(cSPIN_OCD_TH);
  cSPIN_RegsStruct->STEP_MODE = cSPIN_Get_Param(cSPIN_STEP_MODE);
  cSPIN_RegsStruct->ALARM_EN = cSPIN_Get_Param(cSPIN_ALARM_EN);
  cSPIN_RegsStruct->CONFIG = cSPIN_Get_Param(cSPIN_CONFIG);
  __enable_interrupt();
}
/******************************************************************************/
uint32_t inline cSPIN_Get_Param(cSPIN_Registers_TypeDef param) {
  uint32_t temp = 0;
  uint32_t rx = 0;

  /* Send GetParam operation code to cSPIN */
  temp = cSPIN_Write_Byte((uint8_t)cSPIN_GET_PARAM | (uint8_t)param);
  /* MSB which should be 0 */
  temp = temp << 24;
  rx |= temp;
  switch (param) {
  case cSPIN_ABS_POS:;
  case cSPIN_MARK:;
  case cSPIN_SPEED:
    temp = cSPIN_Write_Byte((uint8_t)(0x00));
    temp = temp << 16;
    rx |= temp;
  case cSPIN_EL_POS:;
  case cSPIN_ACC:;
  case cSPIN_DEC:;
  case cSPIN_MAX_SPEED:;
  case cSPIN_MIN_SPEED:;
  case cSPIN_FS_SPD:;

  case cSPIN_CONFIG:;
  case cSPIN_STATUS:
    temp = cSPIN_Write_Byte((uint8_t)(0x00));
    temp = temp << 8;
    rx |= temp;
  default:
    temp = cSPIN_Write_Byte((uint8_t)(0x00));
    rx |= temp;
  }

  return rx;
}

/**
 * @brief  Issues cSPIN Run command.
 * @param  direction Movement direction (FWD, REV)
 * @param  speed over 3 bytes
 * @retval None
 */
void cSPIN_Run(cSPIN_Direction_TypeDef direction, uint32_t speed) {
  /* Send RUN operation code to cSPIN */
  cSPIN_Write_Byte((uint8_t)cSPIN_RUN | (uint8_t)direction);
  /* Send speed - byte 2 data cSPIN */
  cSPIN_Write_Byte((uint8_t)(speed >> 16));
  /* Send speed - byte 1 data cSPIN */
  cSPIN_Write_Byte((uint8_t)(speed >> 8));
  /* Send speed - byte 0 data cSPIN */
  cSPIN_Write_Byte((uint8_t)(speed));
}

/**
 * @brief  Issues cSPIN Step Clock command.
 * @param  direction Movement direction (FWD, REV)
 * @retval None
 */
void cSPIN_Step_Clock(cSPIN_Direction_TypeDef direction) {
  /* Send StepClock operation code to cSPIN */
  cSPIN_Write_Byte((uint8_t)cSPIN_STEP_CLOCK | (uint8_t)direction);
}

/**
 * @brief  Issues cSPIN Move command.
 * @param  direction Movement direction
 * @param  n_step number of steps
 * @retval None
 */
void cSPIN_Move(cSPIN_Direction_TypeDef direction, uint32_t n_step) {
  // uint32_t Scale = (uint32_t) cSPIN_Get_STEP_MODE_SCALE();
  uint32_t ScaledSteps = n_step * ScaleFactor;
  /* Send Move operation code to cSPIN */
  cSPIN_Write_Byte((uint8_t)cSPIN_MOVE | (uint8_t)direction);
  /* Send n_step - byte 2 data cSPIN */
  cSPIN_Write_Byte((uint8_t)(ScaledSteps >> 16));
  /* Send n_step - byte 1 data cSPIN */
  cSPIN_Write_Byte((uint8_t)(ScaledSteps >> 8));
  /* Send n_step - byte 0 data cSPIN */
  cSPIN_Write_Byte((uint8_t)(ScaledSteps));
}

/**
 * @brief  Issues cSPIN Go To command.
 * @param  abs_pos absolute position where requested to move
 * @retval None
 */
void cSPIN_Go_To(uint32_t abs_pos) {
  /* Send GoTo operation code to cSPIN */
  cSPIN_Write_Byte(cSPIN_GO_TO);
  /* Send absolute position parameter - byte 2 data to cSPIN */
  cSPIN_Write_Byte((uint8_t)(abs_pos >> 16));
  /* Send absolute position parameter - byte 1 data to cSPIN */
  cSPIN_Write_Byte((uint8_t)(abs_pos >> 8));
  /* Send absolute position parameter - byte 0 data to cSPIN */
  cSPIN_Write_Byte((uint8_t)(abs_pos));
}

/**
 * @brief  Issues cSPIN Go To Dir command.
 * @param  direction movement direction
 * @param  abs_pos absolute position where requested to move
 * @retval None
 */
void cSPIN_Go_To_Dir(cSPIN_Direction_TypeDef direction, uint32_t abs_pos) {
  /* Send GoTo_DIR operation code to cSPIN */
  cSPIN_Write_Byte((uint8_t)cSPIN_GO_TO_DIR | (uint8_t)direction);
  /* Send absolute position parameter - byte 2 data to cSPIN */
  cSPIN_Write_Byte((uint8_t)(abs_pos >> 16));
  /* Send absolute position parameter - byte 1 data to cSPIN */
  cSPIN_Write_Byte((uint8_t)(abs_pos >> 8));
  /* Send absolute position parameter - byte 0 data to cSPIN */
  cSPIN_Write_Byte((uint8_t)(abs_pos));
}

/**
 * @brief  Issues cSPIN Go Until command.
 * @param  action
 * @param  direction movement direction
 * @param  speed
 * @retval None
 */
void cSPIN_Go_Until(cSPIN_Action_TypeDef action,
                    cSPIN_Direction_TypeDef direction, uint32_t speed) {

  /* Send GoUntil operation code to cSPIN */
  cSPIN_Write_Byte((uint8_t)cSPIN_GO_UNTIL | (uint8_t)action |
                   (uint8_t)direction);
  /* Send speed parameter - byte 2 data to cSPIN */
  cSPIN_Write_Byte((uint8_t)(speed >> 16));
  /* Send speed parameter - byte 1 data to cSPIN */
  cSPIN_Write_Byte((uint8_t)(speed >> 8));
  /* Send speed parameter - byte 0 data to cSPIN */
  cSPIN_Write_Byte((uint8_t)(speed));
}

/**
 * @brief  Issues cSPIN Release SW command.
 * @param  action
 * @param  direction movement direction
 * @retval None
 */
void cSPIN_Release_SW(cSPIN_Action_TypeDef action,
                      cSPIN_Direction_TypeDef direction) {
  ;
  /* Send ReleaseSW operation code to cSPIN */
  cSPIN_Write_Byte((uint8_t)cSPIN_RELEASE_SW | (uint8_t)action |
                   (uint8_t)direction);
}

/**
 * @brief  Issues cSPIN Go Home command. (Shorted path to zero position)
 * @param  None
 * @retval None
 */
void cSPIN_Go_Home(void) {

  /* Send GoHome operation code to cSPIN */
  cSPIN_Write_Byte(cSPIN_GO_HOME);
}

/**
 * @brief  Issues cSPIN Go Mark command.
 * @param  None
 * @retval None
 */
void cSPIN_Go_Mark(void) {

  /* Send GoMark operation code to cSPIN */
  cSPIN_Write_Byte(cSPIN_GO_MARK);
}

/**
 * @brief  Issues cSPIN Reset Pos command.
 * @param  None
 * @retval None
 */
void cSPIN_Reset_Pos(void) {

  /* Send ResetPos operation code to cSPIN */
  cSPIN_Write_Byte(cSPIN_RESET_POS);
}

/**
 * @brief  Issues cSPIN Reset Device command.
 * @param  None
 * @retval None
 */
void cSPIN_Reset_Device(void) {

  /* Send ResetDevice operation code to cSPIN */
  cSPIN_Write_Byte(cSPIN_RESET_DEVICE);
}

/**
 * @brief  Issues cSPIN Soft Stop command.
 * @param  None
 * @retval None
 */
void cSPIN_Soft_Stop(void) {

  /* Send SoftStop operation code to cSPIN */
  cSPIN_Write_Byte(cSPIN_SOFT_STOP);
}

/**
 * @brief  Issues cSPIN Hard Stop command.
 * @param  None
 * @retval None
 */
void cSPIN_Hard_Stop(void) {

  /* Send HardStop operation code to cSPIN */
  cSPIN_Write_Byte(cSPIN_HARD_STOP);
}

/**
 * @brief  Issues cSPIN Soft HiZ command.
 * @param  None
 * @retval None
 */
void cSPIN_Soft_HiZ(void) {

  /* Send SoftHiZ operation code to cSPIN */
  cSPIN_Write_Byte(cSPIN_SOFT_HIZ);
}

/**
 * @brief  Issues cSPIN Hard HiZ command.
 * @param  None
 * @retval None
 */
void cSPIN_Hard_HiZ(void) {
  /* Send HardHiZ operation code to cSPIN */

  cSPIN_Write_Byte(cSPIN_HARD_HIZ);
}

/**
 * @brief  Issues cSPIN Get Status command.
 * @param  None
 * @retval Status Register content
 */
uint16_t cSPIN_Get_Status(void) {
  uint16_t temp = 0;
  uint16_t rx = 0;

  /* Send GetStatus operation code to cSPIN */
  cSPIN_Write_Byte(cSPIN_GET_STATUS);
  /* Send zero byte / receive MSByte from cSPIN */
  temp = cSPIN_Write_Byte((uint8_t)(0x00));
  temp = temp << 8;
  rx |= temp;
  /* Send zero byte / receive LSByte from cSPIN */
  temp = cSPIN_Write_Byte((uint8_t)(0x00));
  rx |= temp;

  return rx;
}
/******************************************************************************/
void DebugDrive(void) {
  cSPIN_RegsStruct_TypeDef cSPIN_RegsStruct;
  cSPIN_Regs_Struct_Reset(&cSPIN_RegsStruct);
  cSPIN_Soft_HiZ();
  cSPIN_Registers_Set(&cSPIN_RegsStruct);
};
/******************************************************************************/
StatusBF cSPIN_Get_StatusBF(void) {
  StatusU State;
  State.Raw = cSPIN_Get_Status();
  return State.Bits;
}
/******************************************************************************/
void cSPIN_SetAcc(float Acc) {
  // uint32_t Scale = (uint32_t) cSPIN_Get_STEP_MODE_SCALE();
  // Acc*=ScaleFactor;
  uint16_t RegValue = (uint16_t)((Acc * 0.068719476736) + 0.5);
  cSPIN_Set_Param(cSPIN_ACC, RegValue);
}
/******************************************************************************/
void cSPIN_SetDec(float Dec) {
  // uint32_t Scale = (uint32_t) cSPIN_Get_STEP_MODE_SCALE();
  // Dec*=ScaleFactor;
  uint16_t RegValue = (uint16_t)((Dec * 0.068719476736) + 0.5);
  cSPIN_Set_Param(cSPIN_DEC, RegValue);
}
/******************************************************************************/
void cSPIN_SetMaxSpeed(float Speed) {
  // uint32_t Scale =(uint32_t) cSPIN_Get_STEP_MODE_SCALE();
  // Speed*=ScaleFactor;
   //if(Speed >= 15625); Speed = 15625;
  uint16_t RegValue = (uint16_t)((Speed * 0.065536) + 0.5);
  cSPIN_Set_Param(cSPIN_MAX_SPEED, RegValue);
}
/******************************************************************************/
void cSPIN_SetMinSpeed(float Speed) {
  // uint32_t Scale = (uint32_t)cSPIN_Get_STEP_MODE_SCALE();
  // Speed*=ScaleFactor;
   //if(Speed >= 976); Speed = 975;
  uint16_t RegValue = (uint16_t)((Speed * 4.194304) + 0.5);
  cSPIN_Set_Param(cSPIN_MIN_SPEED, RegValue);
}
/******************************************************************************/
void cSPIN_SetFullStepSpeedThershold(float Speed) {
  // float Scale =(float)cSPIN_Get_STEP_MODE_SCALE();
  // Speed*=ScaleFactor;
  uint16_t RegValue = (uint16_t)(Speed / 0.065536);
  cSPIN_Set_Param(cSPIN_FS_SPD, RegValue);
}
/******************************************************************************/
void cSPIN_PrepareDevice(void) {
  GPIO_WriteBit(GPIOC, GPIO_Pin_5, Bit_RESET);
  Timer_WaitMilisec(5);
  GPIO_WriteBit(GPIOC, GPIO_Pin_5, Bit_SET);
}
/******************************************************************************/
void cSPIN_SetCurrentHold(float Current) {
  uint16_t RegValue = (uint16_t)(Current / 0.03125);
  cSPIN_Set_Param(cSPIN_TVAL_HOLD, RegValue);
}
/******************************************************************************/
void cSPIN_SetCurrentAcc(float Current) {
  uint16_t RegValue = (uint16_t)(Current / 0.03125);
  cSPIN_Set_Param(cSPIN_TVAL_ACC, RegValue);
}
/******************************************************************************/
void cSPIN_SetCurrentRun(float Current) {
  uint16_t RegValue = (uint16_t)(Current / 0.03125);
  cSPIN_Set_Param(cSPIN_TVAL_RUN, RegValue);
}
/******************************************************************************/
void cSPIN_SetCurrentDec(float Current) {
  uint16_t RegValue = (uint16_t)(Current / 0.03125);
  cSPIN_Set_Param(cSPIN_TVAL_DEC, RegValue);
}
/******************************************************************************/
STEP_MODE_SCALE_FACTOR cSPIN_Get_STEP_MODE_SCALE_FACTOR(STEP_MODE StepMode) {
  STEP_MODE_SCALE_FACTOR Scale;
  switch (StepMode) {

  case STEP_MODE_FULL: {

    Scale = STEP_MODE_SCALE_FACTOR_FULL;
    break;
  }

  case STEP_MODE_HALF: {

    Scale = STEP_MODE_SCALE_FACTOR_HALF;
    break;
  }
  case STEP_MODE_QUARTER: {

    Scale = STEP_MODE_SCALE_FACTOR_QUARTER;
    break;
  }
  case STEP_MODE_EIGHTIN: {
    Scale = STEP_MODE_SCALE_FACTOR_EIGHTIN;
    break;
  }
  case STEP_MODE_SIXTY: {
    Scale = STEP_MODE_SCALE_FACTOR_SIXTY;
    break;
  }
  }

  return Scale;
}

/******************************************************************************/
inline uint32_t cSPIN_Get_Config(void) {
  uint32_t Res = cSPIN_Get_Param(cSPIN_CONFIG);

  return Res;
}
/******************************************************************************/
inline STEP_MODE cSPIN_Get_STEP_MODE(void) {
  STEP_MODEU _SMod;
  STEP_MODE Mode;
  _SMod.Raw = cSPIN_Get_Config();
  Mode = (STEP_MODE)(_SMod._Bits.STEP_SELL);
  return Mode;
}
/******************************************************************************/
STEP_MODE_SCALE_FACTOR cSPIN_Get_STEP_MODE_SCALE(void) {
  STEP_MODE Mode = cSPIN_Get_STEP_MODE();
  return cSPIN_Get_STEP_MODE_SCALE_FACTOR(Mode);
}
/******************************************************************************/
uint32_t cSPIN_Get_ScalebyStep(uint32_t Config) {
  STEP_MODEU _SMod;
  _SMod.Raw = Config;
  STEP_MODE Mode = (STEP_MODE)(_SMod._Bits.STEP_SELL);
  return (uint32_t)cSPIN_Get_STEP_MODE_SCALE_FACTOR(Mode);
}
/******************************************************************************/
uint32_t cSPIN_Get_ScalebyStepV(void) { return ScaleFactor; }
/******************************************************************************/
int cSPIN_Get_ABS_Position(void) {
  int Result;
  cSPIN_Write_Byte((uint8_t)cSPIN_GET_PARAM | (uint8_t)cSPIN_ABS_POS);
  uint16_t LSB = cSPIN_Write_Byte(0);
  uint8_t MMSB = cSPIN_Write_Byte(0);
  uint8_t MSB = cSPIN_Write_Byte(0);
  if (LSB & 0x20) // check 22 bit
  {
    LSB |= 0xFFC0;
  }
  Result = (int)LSB << 16 | (int)MMSB << 8 | (int)MSB;
  return Result;
}

/******************************************************************************/
uint32_t cSPIN_Get_CurrentSpeed(void) {
  uint32_t ReadedSpeed = cSPIN_Get_Param(cSPIN_SPEED);
  uint32_t ConvertedSpeed = ((ReadedSpeed / 67.108864) - 0.5);
  return ConvertedSpeed;
}
/******************************************************************************/
int cSPIN_GetScaledPositionToStop(int uSteps) {
  return (int)ScaleFactor * uSteps;
}

/******************************************************************************/
void cSPIN_SW_Set(void) { GPIO_SetBits(GPIOC, GPIO_Pin_4); }
/******************************************************************************/
void cSPIN_SW_Resete(void) { GPIO_ResetBits(GPIOC, GPIO_Pin_4); }
/******************************************************************************/
void cSPIN_Peripherals_GPIO_Init(void) {

  GPIO_InitTypeDef SPI_GPIO;

  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1, ENABLE);
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);

  GPIO_PinAFConfig(GPIOA, GPIO_PinSource5, GPIO_AF_SPI1);
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource6, GPIO_AF_SPI1);
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource7, GPIO_AF_SPI1);

  GPIO_StructInit(&SPI_GPIO);
  SPI_GPIO.GPIO_Mode = GPIO_Mode_AF;
  SPI_GPIO.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7 | GPIO_Pin_5;

  GPIO_Init(GPIOA, &SPI_GPIO);

  SPI_GPIO.GPIO_Mode = GPIO_Mode_OUT;
  SPI_GPIO.GPIO_Pin = GPIO_Pin_4;
  SPI_GPIO.GPIO_OType = GPIO_OType_PP;
  SPI_GPIO.GPIO_PuPd = GPIO_PuPd_DOWN;
  SPI_GPIO.GPIO_Speed = GPIO_Speed_2MHz;
  GPIO_Init(GPIOA, &SPI_GPIO);

  SPI_GPIO.GPIO_Mode = GPIO_Mode_OUT;
  SPI_GPIO.GPIO_Pin = GPIO_Pin_4;
  SPI_GPIO.GPIO_OType = GPIO_OType_PP;
  SPI_GPIO.GPIO_PuPd = GPIO_PuPd_DOWN;
  SPI_GPIO.GPIO_Speed = GPIO_Speed_2MHz;
  GPIO_Init(GPIOC, &SPI_GPIO);

  SPI_GPIO.GPIO_Mode = GPIO_Mode_OUT;
  SPI_GPIO.GPIO_Pin = GPIO_Pin_5;
  SPI_GPIO.GPIO_OType = GPIO_OType_PP;
  SPI_GPIO.GPIO_PuPd = GPIO_PuPd_DOWN;
  SPI_GPIO.GPIO_Speed = GPIO_Speed_2MHz;
  GPIO_Init(GPIOC, &SPI_GPIO);
}
/******************************************************************************/
void cSPIN_SW_HartStopEngine(void) {
  __enable_interrupt();
  Timer_InitMilisecond();
  cSPIN_SW_Set();
  Timer_WaitMilisec(50);
  cSPIN_SW_Resete();
  __disable_interrupt();
}
/******************************************************************************/
void cSPIN_Peripherals_InitSPI(void) {

  SPI_InitTypeDef Driver_SPI;
  SPI_StructInit(&Driver_SPI);
  Driver_SPI.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_8;
  Driver_SPI.SPI_Mode = SPI_Mode_Master;
  Driver_SPI.SPI_FirstBit = SPI_FirstBit_MSB;
  Driver_SPI.SPI_NSS = SPI_NSS_Soft;
  Driver_SPI.SPI_CRCPolynomial = 7;
  Driver_SPI.SPI_DataSize = SPI_DataSize_8b;
  Driver_SPI.SPI_CPOL = SPI_CPOL_High;
  Driver_SPI.SPI_CPHA = SPI_CPHA_2Edge;
  Driver_SPI.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
  SPI_Init(SPI1, &Driver_SPI);
  SPI_NSSInternalSoftwareConfig(SPI1, SPI_NSSInternalSoft_Set);
  SPI_Cmd(SPI1, ENABLE);
  cSPIN_ChipDeselect();
  ;
};
/******************************************************************************/
void cSPIN_RunEx(float *SpeedBegin, float *SpeedEnd, int *TimeAcc,
                 int *TimeBrake, int *Direction, int *StepsToDecelearation) {
  cSPIN_Reset_Pos();
  cSPIN_Hard_HiZ();
  float ACC;
  float DEC;
  if (cSPIN_RunEx_CheckSpeedRanges(SpeedBegin, SpeedEnd) &&
      cSPIN_RunEx_CheckAcceleration(SpeedBegin, SpeedEnd, TimeAcc, TimeBrake,
                                    &ACC, &DEC)) {
    cSPIN_RunEx_ComputeDeceleration(SpeedBegin, SpeedEnd, StepsToDecelearation,
                                    &DEC);
    cSPIN_SetMinSpeed(*SpeedBegin + 1);
    cSPIN_SetAcc(ACC);
    cSPIN_SetDec(DEC);
    uint32_t EndSpeedW = Speed_Steps_to_Par(*SpeedEnd);
    cSPIN_Run((cSPIN_Direction_TypeDef)*Direction, EndSpeedW + 1);
  }
}
/******************************************************************************/
bool cSPIN_RunEx_CheckSpeedRanges(float *SpeedBegin, float *SpeedEnd) {

  int Flag = 1;

  if (*SpeedBegin >= 976) {
    *SpeedBegin = 976;
  }

  if (*SpeedEnd >= 15625) {
    *SpeedEnd = 15625;
  }
  if (*SpeedBegin > *SpeedEnd) {
    Flag = 0;
  }

  return Flag;
}
/******************************************************************************/
bool cSPIN_RunEx_CheckAcceleration(float *SpeedBegin, float *SpeedEnd,
                                   int *TimeAcc, int *TimeBrake, float *ACC,
                                   float *DEC) {

  int Flag = 1;
  *ACC = 0;
  *DEC = 0;
  if (TimeAcc > 0) {
    *ACC = (*SpeedEnd - *SpeedBegin) / ((float)*TimeAcc / 1000);
    if (*ACC == 0) {
      Flag = 0;
    }
  } else {
    *ACC = (*SpeedEnd - *SpeedBegin) / ((float)1 / 1000);
  }

  if (TimeBrake > 0) {
    *DEC = *SpeedEnd / ((float)*TimeBrake / 1000);
    if (*DEC == 0) {
      Flag = 0;
    }

  } else {
    *DEC = *SpeedEnd / ((float)1 / 1000);
    ;
  }

  if (*DEC >= 59590) {
    *DEC = 59590;
  }

  if (*DEC < 14.55) {
    *DEC = 14.55;
  }

  if (*ACC >= 59590) {
    *ACC = 59590;
  }

  if (*ACC < 14.55) {
    *ACC = 14.55;
  }

  return Flag;
}
/******************************************************************************/
bool cSPIN_RunEx_ComputeDeceleration(float *SpeedBegin, float *SpeedEnd,
                                     int *StepCount, float *DEC) {
  bool Result = false;
  if (*SpeedBegin > 0 && *SpeedEnd > 0 && *StepCount > 0) {
    *DEC = (((float)(*SpeedEnd * *SpeedEnd) -
             (float)(*SpeedBegin * *SpeedBegin))) /
           (2 * (float)*StepCount);
  } else {
    *DEC = 0;
  }
  return Result;
}
/******************************************************************************/
