#include "stm32f4xx.h"

typedef enum ColimatorJumpersSState
{
  ColimatorJumpersState_MainDirection,
  ColimatorJumpersState_ReveceDirection,
};

void ColimatorJumpersControl_Init(void);
ColimatorJumpersSState ColimatorJumpersControl_GetJumperState(void);