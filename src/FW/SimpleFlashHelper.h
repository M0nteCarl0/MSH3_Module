#include "stm32f4xx.h"

#ifdef __cplusplus
 extern "C" {
 #endif   
bool SimpleFlashHelper_WriteToFlash(uint8_t* Buffer,uint32_t Size,uint32_t AddresBegin);
bool SimpleFlashHelper_ReadFromFlash(uint8_t* Buffer,uint32_t Size,uint32_t AddresBegin);
#ifdef __cplusplus
 }
 #endif   
                                   