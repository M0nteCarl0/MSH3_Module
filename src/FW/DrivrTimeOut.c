#include "DrivrTimeOut.h"
int TimeOutms = 0;
int latch = 0;
void DrivrTimeOut_Init(void) {

  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM12, ENABLE);
  TIM_DeInit(TIM12);
  TIM_TimeBaseInitTypeDef Timer_Intit;
  Timer_Intit.TIM_ClockDivision = TIM_CKD_DIV1; // 36Mhz
  Timer_Intit.TIM_CounterMode = TIM_CounterMode_Up;
  Timer_Intit.TIM_Period = 65535;
  Timer_Intit.TIM_Prescaler = 42000;
  Timer_Intit.TIM_RepetitionCounter = 0;
  TIM_TimeBaseInit(TIM12, &Timer_Intit);
  TIM_ClearITPendingBit(TIM12, TIM_IT_Update);
}

void DrivrTimeOut_SetTimeOut(int Time) { TimeOutms = Time; }

void DrivrTimeOut_StartCounter(void) {
  TIM_SetCounter(TIM12, 0);
  TIM_Cmd(TIM12, ENABLE);
  latch = 0;
}

void DrivrTimeOut_StopCounter(void) {
  if(!latch)
  {
    TIM_Cmd(TIM12, DISABLE);
  }
}

int DrivrTimeOut_TimeisExpiried(void) {
  int flag = 0;
  if (TIM_GetCounter(TIM12) == TimeOutms) {
    DrivrTimeOut_StopCounter();
    flag = 1;
  }
  return flag;
}
