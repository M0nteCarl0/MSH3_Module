#include "CCD_Trigger.h"
#include "Clock.h"
#include "ColimatorJumpersControl.h"
#include "Commands.h"
#include "Encoder.h"
#include "HV_Impulse.h"
#include "HV_Trigger.h"
#include "LED.h"
#include "MSH_Core.h"
#include "Mode_selector.h"
#include "MySPIN.h"
#include "MyUsart.h"
#include "MyUsartHard.h"
#include "SafetySystem.h"
#include "Sensors.h"
#include "Timer.h"
#include "Timer_TDI_Impulse.h"
#include "stm32f4xx.h"
#include "DrivrTimeOut.h"
int main() {
  __disable_interrupt();
  InitLED();
  HSE_BYPASS();
  Encoder_InitHWRead();
  cSPIN_Peripherals_GPIO_Init();
  MSH_Core_EmeragancyStopMotor();
  SafetySystem_PVD_Init();
  InitLED();
  Init_UsartHard();
  InitIntrenalReceiveAndTranceiveStack();
  InitInternalCommand();
  cSPIN_Peripherals_InitSPI();
  InitSensrors();
  Timer_TDI_Impulse_Init();
  HV_Impulse_InitDeffault();
  CCD_Trigger_InitGPIOInitDeffault();
  __enable_interrupt();
  Timer_InitMilisecond();
  Timer_TraceInit();
  HV_Trigge_Init();
  cSPIN_PrepareDevice();
  DebugDrive();
  MSH_Core_DellayDriveTimerInit();
  MSH_Core_Init_Polling_Timer();
  MSH_Core_ModluleModeSelectorInit();
  MSH_Core_PrepareModule();
  DrivrTimeOut_Init();
  DrivrTimeOut_SetTimeOut(12000);
  MSH_Core_ModuleParking();
  InitTestpoint();
  ColimatorJumpersControl_Init();
  Command_ProcessCommands();
};
