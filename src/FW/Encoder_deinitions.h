#include "stm32f4xx.h"
enum  Encoder_Direction
{
  Encoder_Direction_CW = 0,
  Encoder_Direction_CWW = 1
};


enum  Encoder_Direction_Sign
{
  Encoder_Direction_Sign_CW = 0,
  Encoder_Direction_Sign_CWW = 1
};


typedef  enum  Encoder_ReadMode
{
  Encoder_ReadMode_HW = 0,
  Encoder_ReadMode_EXTI_Steps = 1,
  Encoder_ReadMode_EXTI_Ticks = 2,
}Encoder_ReadMode;

typedef struct  Encoder_InitTypedef
{
  uint32_t Clock_pins;
  GPIO_TypeDef* Port;
  uint32_t Pin_DIR;
  uint32_t Pin_A;
  uint32_t Pin_B;
  Encoder_ReadMode ReadMode;
}Encoder_InitTypedef;

typedef  struct Encoder_FiniteMachine
{
  uint32_t CurrentState;
  uint32_t LastState;
  
}Encoder_FiniteMachine;

typedef  struct Encoder_FiniteMachineReference
{
  uint8_t CurrentState;
  uint8_t LastState;
  
}Encoder_FiniteMachineReference;

typedef  struct Encoder_BF
 {
   uint8_t Pin_A:1;
   uint8_t Pin_B:1;
   uint8_t Reserved:6;
 }Encoder_BF;


typedef  union Encoder_StateU
{
  Encoder_BF Bits;
  uint8_t Raw;
}Encoder_StateU;

typedef  struct Encoder_InternalRegisters
{
 Encoder_InitTypedef* Pins;
 Encoder_FiniteMachine State;
 Encoder_FiniteMachineReference RefferenceState;
 Encoder_StateU LState;
 short  Steps;
 short  Tiks;
// int  StepsTest;
 uint16_t ReferenceCount;
 uint8_t  ReferenceEdgeControl;
 Encoder_Direction  Direction;
 Encoder_Direction_Sign  Sign;
}Encoder_InternalRegisters;

enum Encoder_FiniteMachine_stetes
{
  Encoder_FiniteMachine_stetes_1 = 0,//00
  Encoder_FiniteMachine_stetes_2 = 1,//01
  Encoder_FiniteMachine_stetes_3 = 3,//11
  Encoder_FiniteMachine_stetes_4 = 2,//10
};
