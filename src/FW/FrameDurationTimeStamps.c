#include "FrameDurationTimeStamps.h"
#include "Timer.h"
LatchEvents Events;
EventsTimeStamps TimeStamps;
/*****************************************************************************/
void FrameDurationTimeStamps_InitCounter(void) {
  Events.EventID_Map = LatchEventID_CLEAN;
  TimeStamps.BeginDrive = 0;
  TimeStamps.ReferencePoint = 0;
  TimeStamps.Ky5_EndDrive = 0;
  TimeStamps.Ky5_FallingEdge = 0;
  TimeStamps.Msh3_hv_off = 0;

  Timer_TraceBegin();
}
/*****************************************************************************/
void FrameDurationTimeStamps_FixBeginDrive(void) {
  uint32_t EventFlag = Events.EventID_Map & LatchEventID_BEGIN_DRIVE;
  if (!EventFlag) {
    TimeStamps.BeginDrive = Timer_TraceGetTime();
    Events.EventID_Map |= LatchEventID_BEGIN_DRIVE;
  }
}
/*****************************************************************************/
void FrameDurationTimeStamps_FixReferencePoint(void) {
  uint32_t EventFlag = Events.EventID_Map & LatchEventID_REFERENCE_POINT;
  if (!EventFlag) {
    TimeStamps.ReferencePoint = Timer_TraceGetTime();
    Events.EventID_Map |= LatchEventID_REFERENCE_POINT;
  }
}
/*****************************************************************************/
void FrameDurationTimeStamps_FixMsh3_hv_off(void) {

  uint32_t EventFlag = Events.EventID_Map & LatchEventID_MSH_HV_OFF;
  if (!EventFlag) {
    TimeStamps.Msh3_hv_off = Timer_TraceGetTime();
    Events.EventID_Map |= LatchEventID_MSH_HV_OFF;
  }
}
/*****************************************************************************/
void FrameDurationTimeStamps_FixKy5_FallingEdge(void) {

  uint32_t EventFlag = Events.EventID_Map & LatchEventID_MSH_KY5_FALLING_EDGE;
  if (!EventFlag) {
    TimeStamps.Ky5_FallingEdge = Timer_TraceGetTime();
    Events.EventID_Map |= LatchEventID_MSH_KY5_FALLING_EDGE;
  }
}
/*****************************************************************************/
void FrameDurationTimeStampsKy5_EndDrive(void) {
  uint32_t EventFlag = Events.EventID_Map & LatchEventID_MSH_DRIVE_END;
  if (!EventFlag) {
    TimeStamps.Ky5_EndDrive = Timer_TraceGetTime();
    Events.EventID_Map |= LatchEventID_MSH_DRIVE_END;
  }
}
/*****************************************************************************/
