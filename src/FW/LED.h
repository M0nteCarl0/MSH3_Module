#pragma once
#include "stm32f4xx.h"
#ifndef _LED
#define _LED

enum LED_Definition
{
  
  LED_Definition_VD2 = GPIO_Pin_8,
  LED_Definition_VD3 = GPIO_Pin_8,
  LED_Definition_VD4 = GPIO_Pin_8,
  LED_Definition_VD5 = GPIO_Pin_7,
  LED_Definition_VD6 = GPIO_Pin_6,
};

void InitLED(void);
void BlinkyLED(void);
void SynchroLed(void);
void ResetSynchroLed(void);
void EndAcc(void);
void BeginAcc(void);
void DeviceOn(void);

void InitTestpoint(void);
void TestPointOn(void);
void TestPointOff(void);

void OnDP1Led(void);
void OffDP1Led(void);

void OnDP2Led(void);
void OffDP2Led(void);

void OnSVD1(void);
void OnSVD2(void);
void OnSVD3(void);

void OffSVD1(void);
void OffSVD2(void);
void OffSVD3(void);



void OnDP3Led(void);
void OffDP3Led(void);

void OnKYEvent(void);
void OffKYEvent(void);

void DetectorReadyLedOn(void);
void DetectorReadyLedOff(void);
void LED_ModuleHeartBeat(void);

#endif