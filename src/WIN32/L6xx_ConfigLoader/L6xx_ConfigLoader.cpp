// L6xx_ConfigLoader.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "ConfigParcer.h"
#include "MXCOMM.H"
#include "MyCommand.h"
#include <Windows.h>
#include <intrin.h>
#include <stdint.h>
#include <string>
#include <tchar.h>
int Steps;
int Dir;
std::string _Name;
ConfigParcer _Parcer;
MyCommand _Command;
cSPIN_RegsStruct_TypeDef _DevRegsW;
cSPIN_RegsStruct_TypeDef _DevRegsR;
void ConfigMSHOp(char *argv[],int deviceid);
void ReaWritedConfigFromDevice(int deviceid);
void RebootDevice(void);
void ReadAbsolutePostion(void);
MSH_Core_module_ID_Number GetModuleByNumber(int Number)
{
   MSH_Core_module_ID_Number Num;

   switch(Number)
   {
    case 1:{
    Num = MSH_Core_module_ID_Number_Collimator;
    break;
    }


    case 2:{
     Num = MSH_Core_module_ID_Number_Scaner;
    break;
    }

    default:{
      Num = MSH_Core_module_ID_Number_Scaner;
      break;

    }



   }


   return Num;


}



/*******************************************************************************************************************************************************/
void TraceInfo_cSPIN_RegsStruct(cSPIN_RegsStruct_TypeDef _DevRegs) {

 FILE* Trace = fopen("MSH3_FlashSource.ini","w");
  printf(
      "ABS_POS=%i\nEL_POS=%i\nMARK=%i\nSPEED=%i\nACC=%i\nDEC="
      "%i\nMAX_SPEED=%i\nMIN_SPEED=%i\nFS_SPD=%i\nTVAL_HOLD="
      "%i\nTVAL_RUN=%i\nTVAL_ACC=%i\nTVAL_DEC=%i\nT_FAST="
      "0x%x\nTON_MIN=%i\nTOFF_MIN=%i\nADC_OUT=%i\nOCD_TH=%i\nSTEP_MODE"
      "=%i\nALARM_EN=%i\nGATECFG1=%i\nGATECFG2=%i\nCONFIG"
      "=0x%x\nSTATUS=%i\nLookAtSwitch=%i\nStepsDeceleration=%i\n",
      _DevRegs.ABS_POS, _DevRegs.EL_POS, _DevRegs.MARK, _DevRegs.SPEED,
      _DevRegs.ACC, _DevRegs.DEC, _DevRegs.MAX_SPEED, _DevRegs.MIN_SPEED,
      _DevRegs.FS_SPD, _DevRegs.TVAL_HOLD, _DevRegs.TVAL_RUN, _DevRegs.TVAL_ACC,
      _DevRegs.TVAL_DEC, _DevRegs.T_FAST, _DevRegs.TON_MIN, _DevRegs.TOFF_MIN,
      _DevRegs.ADC_OUT, _DevRegs.OCD_TH, _DevRegs.STEP_MODE, _DevRegs.ALARM_EN,
      _DevRegs.GATECFG1, _DevRegs.GATECFG2, _DevRegs.CONFIG, _DevRegs.STATUS,
      _DevRegs.LookAtSwitch, _DevRegs.SensorOffset);

      fprintf(Trace,"ABS_POS=%i\nEL_POS=%i\nMARK=%i\nSPEED=%i\nACC=%i\nDEC="
      "%i\nMAX_SPEED=%i\nMIN_SPEED=%i\nFS_SPD=%i\nTVAL_HOLD="
      "%i\nTVAL_RUN=%i\nTVAL_ACC=%i\nTVAL_DEC=%i\nT_FAST="
      "0x%x\nTON_MIN=%i\nTOFF_MIN=%i\nADC_OUT=%i\nOCD_TH=%i\nSTEP_MODE"
      "=%i\nALARM_EN=%i\nGATECFG1=%i\nGATECFG2=%i\nCONFIG"
      "=0x%x\nSTATUS=%i\nLookAtSwitch=%i\nStepsDeceleration=%i\n",
      _DevRegs.ABS_POS, _DevRegs.EL_POS, _DevRegs.MARK, _DevRegs.SPEED,
      _DevRegs.ACC, _DevRegs.DEC, _DevRegs.MAX_SPEED, _DevRegs.MIN_SPEED,
      _DevRegs.FS_SPD, _DevRegs.TVAL_HOLD, _DevRegs.TVAL_RUN, _DevRegs.TVAL_ACC,
      _DevRegs.TVAL_DEC, _DevRegs.T_FAST, _DevRegs.TON_MIN, _DevRegs.TOFF_MIN,
      _DevRegs.ADC_OUT, _DevRegs.OCD_TH, _DevRegs.STEP_MODE, _DevRegs.ALARM_EN,
      _DevRegs.GATECFG1, _DevRegs.GATECFG2, _DevRegs.CONFIG, _DevRegs.STATUS,
      _DevRegs.LookAtSwitch, _DevRegs.SensorOffset);
     
      fclose(Trace);
}
/***********************************************************************/
int _tmain(int argc, _TCHAR *argv[]) {

  switch (argc) {
  case 1: {

    break;
  }

  case 4: {
    if (atoi(argv[1]) == 1){
      int DeviceID = atoi(argv[2]);
      DeviceID = GetModuleByNumber(DeviceID);
      ConfigMSHOp(argv, DeviceID);
  }
  break;
  }
  case 3: {
    if (atoi(argv[1]) == 2) {
      int DeviceID = atoi(argv[2]);
      DeviceID = GetModuleByNumber(DeviceID);
      ReaWritedConfigFromDevice(DeviceID);
      break;
    }
  }
  }

  system("Pause");
  return 0;
}
/***********************************************************************/
void ConfigMSHOp(char *argv[],int deviceid) {
  BYTE SW[4];
  BYTE HW[4];
  BYTE Name[4];
  int ABS_POS;
  int EL_POS;
  int MARK;
  int ACC;
  int DEC;
  int MAX_SPEED;
  int MIN_SPEED;
  int FS_SPD;
  int TVAL_HOLD;
  int TVAL_RUN;
  int TVAL_ACC;
  int TVAL_DEC;
  int T_FAST;
  int TON_MIN;
  int TOFF_MIN;
  int OCD_TH;
  int STEP_MODE;
  int ALARM_EN;
  int GATECFG1;
  int GATECFG2;
  int CONFIG;
  int LookAtSwitch;
  int SensorOffset;

  _Command.InitComm();
  _Command.DeviceID = deviceid;
  if (_Command.IsFTDIConected()) {
    if (_Command.DeviceConectedToBox()) {
      _Command.ReadVersion(Name, HW, SW);
      printf("Device SW:%i.%i\nDevice HW:%i.%i\n", SW[0], SW[1], HW[0], HW[1]);
      if (_Parcer.Open(argv[3])) {
        _Parcer.SetDelimetrToken("=");
        _Parcer.GetIntParam("ABS_POS", ABS_POS);
        _Parcer.GetIntParam("EL_POS", EL_POS);
        _Parcer.GetIntParam("MARK", MARK);
        _Parcer.GetIntParam("ACC", ACC);
        _Parcer.GetIntParam("DEC", DEC);
        _Parcer.GetIntParam("MAX_SPEED", MAX_SPEED);
        _Parcer.GetIntParam("MIN_SPEED", MIN_SPEED);
        _Parcer.GetIntParam("FS_SPD", FS_SPD);
        _Parcer.GetIntParam("TVAL_HOLD", TVAL_HOLD);
        _Parcer.GetIntParam("TVAL_RUN", TVAL_RUN);
        _Parcer.GetIntParam("TVAL_ACC", TVAL_ACC);
        _Parcer.GetIntParam("TVAL_DEC", TVAL_DEC);
        _Parcer.GetIntParamHex("T_FAST", T_FAST);
        _Parcer.GetIntParam("TON_MIN", TON_MIN);
        _Parcer.GetIntParam("TOFF_MIN", TOFF_MIN);
        _Parcer.GetIntParam("OCD_TH", OCD_TH);
        _Parcer.GetIntParam("STEP_MODE", STEP_MODE);
        _Parcer.GetIntParam("ALARM_EN", ALARM_EN);
        _Parcer.GetIntParam("GATECFG1", GATECFG1);
        _Parcer.GetIntParam("GATECFG2", GATECFG2);
        _Parcer.GetIntParamHex("CONFIG", CONFIG);
        _Parcer.GetIntParam("LookAtLimSwitch", LookAtSwitch);
        _Parcer.GetIntParam("StepsDeceleration", SensorOffset);
        _DevRegsW.ABS_POS = (uint32_t)ABS_POS;
        _DevRegsW.EL_POS = (uint16_t)EL_POS;
        _DevRegsW.MARK = (uint32_t)MARK;
        _DevRegsW.ACC = (uint16_t)ACC;
        _DevRegsW.DEC = (uint16_t)DEC;
        _DevRegsW.MAX_SPEED = (uint16_t)MAX_SPEED;
        _DevRegsW.MIN_SPEED = (uint16_t)MIN_SPEED;
        _DevRegsW.FS_SPD = (uint16_t)FS_SPD;
        _DevRegsW.TVAL_HOLD = (uint8_t)TVAL_HOLD;
        _DevRegsW.TVAL_RUN = (uint8_t)TVAL_RUN;
        _DevRegsW.TVAL_ACC = (uint8_t)TVAL_ACC;
        _DevRegsW.TVAL_DEC = (uint8_t)TVAL_DEC;
        _DevRegsW.T_FAST = (uint8_t)T_FAST;
        _DevRegsW.TON_MIN = (uint8_t)TON_MIN;
        _DevRegsW.TOFF_MIN = (uint8_t)TOFF_MIN;
        _DevRegsW.OCD_TH = (uint8_t)OCD_TH;
        _DevRegsW.STEP_MODE = (uint8_t)STEP_MODE;
        _DevRegsW.ALARM_EN = (uint8_t)ALARM_EN;
        _DevRegsW.GATECFG1 = (uint16_t)GATECFG1;
        _DevRegsW.GATECFG2 = (uint8_t)GATECFG2;
        _DevRegsW.CONFIG = (uint16_t)CONFIG;
        _DevRegsW.LookAtSwitch = (uint8_t)LookAtSwitch;
        _DevRegsW.SensorOffset = (uint8_t)SensorOffset;
        TraceInfo_cSPIN_RegsStruct(_DevRegsW);
        _Command.WriteDriverConfig(_DevRegsW);
        Sleep(20000);
        // ommand.m_Comm.Close();
        // ommand.InitComm();
        printf("Reed Writed data from regs driver \n");
        _Command.ReadDriverConfig(&_DevRegsR);
        cout << "Readed From Device\n";
        TraceInfo_cSPIN_RegsStruct(_DevRegsR);
        printf("Reset module to Apply Setup begin\n");
        _Command.ResetModule();
        Sleep(10000);
        printf("Reset module to Apply Setup Completed\n");
      } else {
        cout << "Can  not open Config file\n";
      }
    }
  } else {

    cout << "Device not connec to Box \n";
  }
}
/***********************************************************************/
void ReaWritedConfigFromDevice(int deviceid) {

  BYTE SW[4];
  BYTE HW[4];
  BYTE Name[4];

  _Command.InitComm();
  _Command.DeviceID = deviceid;
  if (_Command.IsFTDIConected()) {
    if (_Command.DeviceConectedToBox()) {
      _Command.ReadVersion(Name, HW, SW);
      printf("Device SW:%i.%i\nDevice HW:%i.%i\n", SW[0], SW[1], HW[0], HW[1]);
      printf("Reed Writed data from regs driver \n");
      _Command.ReadDriverConfig(&_DevRegsR);
      cout << "Readed From Device\n";
      TraceInfo_cSPIN_RegsStruct(_DevRegsR);
    } else {
      cout << "Device not connec to Box \n";
    }
  } else {
    cout << "Device not connec to Box \n";
  }
}
/***********************************************************************/
void RebootDevice(void) {

  BYTE SW[4];
  BYTE HW[4];
  BYTE Name[4];
  _Command.InitComm();

  if (_Command.IsFTDIConected()) {
    if (_Command.DeviceConectedToBox()) {
      _Command.ReadVersion(Name, HW, SW);
      printf("Device SW:%i.%i\nDevice HW:%i.%i\n", SW[0], SW[1], HW[0], HW[1]);
      printf("Reeting device .....\n");
      _Command.ResetModule();
      Sleep(10000);
      printf("Reset Done\n");
    } else {
      cout << "Device not connec to Box \n";
    }
  } else {
    cout << "Device not connec to Box \n";
  }
}
/***********************************************************************/
void ReadAbsolutePostion(void) {

  BYTE SW[4];
  BYTE HW[4];
  BYTE Name[4];
  int Abs = 0;
  _Command.InitComm();

  if (_Command.IsFTDIConected()) {

    if (_Command.DeviceConectedToBox()) {
      cout << "Engine  position in Steps from Motor Driver:" << Abs << "\n";
    }
  }
}

/***********************************************************************/
void FaultDeviceAction(void) {
  _Command.InitComm();

  if (_Command.IsFTDIConected()) {

    if (_Command.DeviceConectedToBox()) {
      _Command.BreakMosuleCommand();
    }
  }
}
/***********************************************************************/
