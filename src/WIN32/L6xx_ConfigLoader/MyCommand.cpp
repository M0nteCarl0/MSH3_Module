//----------------------------------------------------------------------------
//    Create by Novikov
//    data 25.12.2012
//----------------------------------------------------------------------------
//#include <process.h>
#include "MyCommand.h"
#include <stdio.h>
#include <string>
/*!
 * \brief ����� ��� ������ � ���������
 * \details
 * \author ���������� ���������
 * \version 0.1
 * \date 2.09.2015
 */

//----------------------------------------------------------------------------
MyCommand::MyCommand() {
  m_FlgDebugS = false;
  m_FlgDebugR = false;
  m_FlgCheckCRCwrite = true;
  m_FlgCheckCRCread = true;
};
//----------------------------------------------------------------------------
MyCommand::~MyCommand(){};
//----------------------------------------------------------------------------
void MyCommand::InitComm() {
  UINT nSpeed = 115200;
  UINT nDataBits = 8;
  UINT nParity = 1;
  UINT nStopBits = 2;
  int TimeOut = 0;

  bool Ret = m_Comm.Configure(nSpeed, nDataBits, nParity, nStopBits);
  if (Ret)
    printf("Comm Configurate = %d  OK!\n\n", Ret);
  else
    printf("Comm Configurate = %d  Error\n\n", Ret);

  m_Comm.SetTimeout(120);
};

//----------------------------------------------------------------------------
//    WRITE       WRITE       WRITE       WRITE       WRITE       WRITE
//----------------------------------------------------------------------------
BYTE MyCommand::MakeCRC(BYTE *Data, int N)
//    ���������� CRC ����� ��������
{
  int Summ = 0;
  for (int i = 0; i < N; i++) {
    Summ += Data[i];
  }
  int SummH = (Summ >> 7) & 0x7F;

  BYTE mCRC = (BYTE)(SummH + (Summ & 0x7F));

  mCRC &= 0x7F;
  return mCRC;
};

void MyCommand::AddCRC(BYTE *Data, int N) {
  if (N < 5)
    return;

  Data[4] = MakeCRC(Data, 4);

  if (N > 6) {
    Data[N - 1] = MakeCRC(&Data[5], N - 6);
  }
};
//----------------------------------------------------------------------------
bool MyCommand::CheckCRC(BYTE *Data, int N) {
  if (N < 5)
    return false;

  BYTE CRC = MakeCRC(Data, 4);
  if (CRC != Data[4])
    return false;

  if (N > 6) {
    CRC = MakeCRC(&Data[5], N - 6);
    if (CRC != Data[N - 1])
      return false;
  }
  return true;
};
//----------------------------------------------------------------------------
int MyCommand::CheckErrorWR(BYTE *DataS, int NS, BYTE *DataR, int NR) {
  if (NR < 5)
    return 0x10;
  //   if (NS != NR)              return 0x20;
  if (DataR[3] != NR)
    return 0x30;

  return 0;
};
//----------------------------------------------------------------------------
int MyCommand::WriteCommand(BYTE *DataS, int NS) {
  DataS[0] = mID;
  DataS[1] = DeviceID;

  DataS[3] = NS; // Amount of bytes

  if (m_FlgCheckCRCwrite)
    AddCRC(DataS, NS);

  int NR = NS;
  DWORD NumRec;

  bool Ret = m_Comm.WRcommand(DataS, NS, m_DataR, NR, &NumRec, m_FlgDebugS,
                              m_FlgDebugR);

  if (!Ret)
    return 0x01;
  if (NR != NumRec)
    return 0x02;
  if (m_FlgCheckCRCread && !CheckCRC(m_DataR, NR))
    return 0x03;

  int Er = CheckErrorWR(DataS, NS, m_DataR, NR);
  return Er;
};
//----------------------------------------------------------------------------
int MyCommand::ReadCommand(BYTE *DataS, int NS, BYTE *DataR, int NR) {
  DataS[0] = mID;
  DataS[1] = DeviceID;

  DataS[3] = NS; // Amount of bytes

  if (m_FlgCheckCRCwrite)
    AddCRC(DataS, NS);

  DWORD NumRec;

  bool Ret =
      m_Comm.WRcommand(DataS, NS, DataR, NR, &NumRec, m_FlgDebugS, m_FlgDebugR);

  if (!Ret)
    return 0x01;
  if (NR != NumRec)
    return 0x02;
  if (m_FlgCheckCRCread && !CheckCRC(m_DataR, NR))
    return 0x03;

  int Er = CheckErrorWR(DataS, NS, DataR, NR);
  return Er;
};

//----------------------------------------------------------------------------
//    READ     READ     READ     READ     READ     READ     READ     READ READ
//    READ     READ     READ     READ
//----------------------------------------------------------------------------
int MyCommand::ReadVersion(BYTE *Name, BYTE *Hard, BYTE *Soft) {
  int NS = 5;        // Amount of Bytes  SEND
  m_DataS[2] = 0x40; // Commanda

  int NR = 14; // Amount of Bytes  RECEIVE 5+8+1=14

  int Er = ReadCommand(m_DataS, NS, m_DataR, NR);

  BYTE SB = 0x01;
  Name[2] = Name[3] = 0;
  Name[0] = m_DataR[5];
  if (m_DataR[8] & SB)
    Name[0] |= 0x80;
  SB = SB << 1;

  Name[1] = m_DataR[6];
  if (m_DataR[8] & SB)
    Name[1] |= 0x80;
  SB = SB << 1;

  Name[0] = m_DataR[5];
  Name[1] = m_DataR[6];
  Name[2] = m_DataR[7];
  Name[3] = m_DataR[8];

  Hard[0] = m_DataR[9];
  Hard[1] = m_DataR[10];
  Soft[0] = m_DataR[11];
  Soft[1] = m_DataR[12];

  return Er;
};

/*******************************************************************/
bool MyCommand::IsFTDIConected(void) { return m_Comm.CommMounted(); }
/*******************************************************************/
void MyCommand::WriteDriverConfig(cSPIN_RegsStruct_TypeDef Config) {
  int NS = 68; // Amount of Bytes  SEND
  m_DataS[2] = CommadID_WRITE_DRIVER_SETUP;
  ConvSPIN_RegsStruct_TypeDefToByte(&Config, &m_DataS[5]);
  WriteCommand(m_DataS, NS);
}

/*******************************************************************/
void MyCommand::ReadDriverConfig(cSPIN_RegsStruct_TypeDef *Config) {
  int NS = 5;
  int NR = 68;
  m_DataS[2] = CommadID_READ_DRIVER_SETUP;
  ReadCommand(m_DataS, NS, m_DataR, NR);
  ConvcSPIN_ByteToRegsStruct_TypeDef(Config, &m_DataR[5]);
}
/*******************************************************************/
void MyCommand::ResetModule(void) {
  int NS = 5;
  m_DataS[2] = CommadID_REBOOT_MODULE;
  WriteCommand(m_DataS, NS);
}
/*******************************************************************/
int MyCommand::WriteLegacy(BYTE *DataS, int NS) {
  return m_Comm.Write((char *)DataS, NS);
}
/*******************************************************************/
int MyCommand::ReadLegacy(BYTE *DataS, int NS) {
  bool Res = false;
  DWORD Received = 0;
  m_Comm.Read((char *)DataS, NS, &Received);
  if (NS == Received) {
    Res = true;
  }

  return Res;
}
/*******************************************************************/
void MyCommand::ConvSPIN_RegsStruct_TypeDefToByte(
    cSPIN_RegsStruct_TypeDef *Source, uint8_t *Dest) {

  DwordToByte(&Source->ABS_POS, &Dest[0], &Dest[1], &Dest[2]);
  DwordToByte((uint32_t *)&Source->EL_POS, &Dest[3], &Dest[4], &Dest[5]);
  DwordToByte(&Source->MARK, &Dest[6], &Dest[7], &Dest[8]);
  DwordToByte(&Source->SPEED, &Dest[9], &Dest[10], &Dest[11]);
  DwordToByte((uint32_t *)&Source->ACC, &Dest[12], &Dest[13], &Dest[14]);
  DwordToByte((uint32_t *)&Source->DEC, &Dest[15], &Dest[16], &Dest[17]);

  DwordToByte((uint32_t *)&Source->MAX_SPEED, &Dest[18], &Dest[19], &Dest[20]);
  DwordToByte((uint32_t *)&Source->MIN_SPEED, &Dest[21], &Dest[22], &Dest[23]);
  DwordToByte((uint32_t *)&Source->FS_SPD, &Dest[24], &Dest[25], &Dest[26]);

  ByteTo7Bit(&Source->TVAL_HOLD, &Dest[27], &Dest[28]);
  ByteTo7Bit(&Source->TVAL_RUN, &Dest[29], &Dest[30]);
  ByteTo7Bit(&Source->TVAL_ACC, &Dest[31], &Dest[32]);
  ByteTo7Bit(&Source->TVAL_DEC, &Dest[33], &Dest[34]);
  ByteTo7Bit(&Source->T_FAST, &Dest[35], &Dest[36]);
  ByteTo7Bit(&Source->TON_MIN, &Dest[37], &Dest[38]);
  ByteTo7Bit(&Source->TOFF_MIN, &Dest[39], &Dest[40]);
  ByteTo7Bit(&Source->ADC_OUT, &Dest[41], &Dest[42]);
  ByteTo7Bit(&Source->OCD_TH, &Dest[43], &Dest[44]);
  ByteTo7Bit(&Source->STEP_MODE, &Dest[45], &Dest[46]);
  ByteTo7Bit(&Source->ALARM_EN, &Dest[47], &Dest[48]);

  DwordToByte((uint32_t *)&Source->GATECFG1, &Dest[49], &Dest[50], &Dest[51]);
  ByteTo7Bit(&Source->GATECFG2, &Dest[52], &Dest[53]);

  DwordToByte((uint32_t *)&Source->CONFIG, &Dest[54], &Dest[55], &Dest[56]);
  DwordToByte((uint32_t *)&Source->STATUS, &Dest[57], &Dest[58], &Dest[59]);
  Dest[60] = (uint8_t)Source->LookAtSwitch & 0x7f;
  Dest[61] = (uint8_t)Source->SensorOffset & 0x7f;
}
/***********************************************************************************************/
void MyCommand::ConvcSPIN_ByteToRegsStruct_TypeDef(
    cSPIN_RegsStruct_TypeDef *Source, uint8_t *Dest) {

  ByteToDword(&Source->ABS_POS, &Dest[0], &Dest[1], &Dest[2]);
  ByteToDword((uint32_t *)&Source->EL_POS, &Dest[3], &Dest[4], &Dest[5]);
  ByteToDword(&Source->MARK, &Dest[6], &Dest[7], &Dest[8]);
  ByteToDword(&Source->SPEED, &Dest[9], &Dest[10], &Dest[11]);
  ByteToDword((uint32_t *)&Source->ACC, &Dest[12], &Dest[13], &Dest[14]);
  ByteToDword((uint32_t *)&Source->DEC, &Dest[15], &Dest[16], &Dest[17]);

  ByteToDword((uint32_t *)&Source->MAX_SPEED, &Dest[18], &Dest[19], &Dest[20]);
  ByteToDword((uint32_t *)&Source->MIN_SPEED, &Dest[21], &Dest[22], &Dest[23]);
  ByteToDword((uint32_t *)&Source->FS_SPD, &Dest[24], &Dest[25], &Dest[26]);

  Bit7ToByte(&Source->TVAL_HOLD, &Dest[27], &Dest[28]);
  Bit7ToByte(&Source->TVAL_RUN, &Dest[29], &Dest[30]);
  Bit7ToByte(&Source->TVAL_ACC, &Dest[31], &Dest[32]);
  Bit7ToByte(&Source->TVAL_DEC, &Dest[33], &Dest[34]);
  Bit7ToByte(&Source->T_FAST, &Dest[35], &Dest[36]);
  Bit7ToByte(&Source->TON_MIN, &Dest[37], &Dest[38]);
  Bit7ToByte(&Source->TOFF_MIN, &Dest[39], &Dest[40]);
  Bit7ToByte(&Source->ADC_OUT, &Dest[41], &Dest[42]);
  Bit7ToByte(&Source->OCD_TH, &Dest[43], &Dest[44]);
  Bit7ToByte(&Source->STEP_MODE, &Dest[45], &Dest[46]);
  Bit7ToByte(&Source->ALARM_EN, &Dest[47], &Dest[48]);

  ByteToDword((uint32_t *)&Source->GATECFG1, &Dest[49], &Dest[50], &Dest[51]);
  Bit7ToByte(&Source->GATECFG2, &Dest[52], &Dest[53]);
  ByteToDword((uint32_t *)&Source->CONFIG, &Dest[54], &Dest[55], &Dest[56]);
  ByteToDword((uint32_t *)&Source->STATUS, &Dest[57], &Dest[58], &Dest[59]);
  Source->LookAtSwitch = Dest[60];
  Source->SensorOffset = Dest[61];
}
/************************************************************************/
void MyCommand::ByteTo7Bit(uint8_t *Source, uint8_t *Dest1, uint8_t *Dest2) {
  *Dest1 = *Source & 0x7F;
  *Dest2 = (*Source >> 7);
};
/************************************************************************/
void MyCommand::Bit7ToByte(uint8_t *Source, uint8_t *Dest1, uint8_t *Dest2) {
  *Source = *Dest1 & 0x7F | (*Dest2 << 7);
};

/******************************************************************************/
void MyCommand::ByteToDword(uint32_t *Destination, uint8_t *Source1,
                            uint8_t *Source2, uint8_t *Source3) {
  *Destination = ((uint32_t)*Source1 & 0x7F) |
                 (((uint32_t)*Source2 & 0x7F) << 7) |
                 (((uint32_t)*Source3 & 0x7F) << 14);
};

/******************************************************************************/
void MyCommand::DwordToByte(uint32_t *Source, uint8_t *B1, uint8_t *B2,
                            uint8_t *B3) {

  *B1 = *Source & 0x7f;
  *B2 = *Source >> 7 & 0x7f;
  *B3 = *Source >> 14 & 0x7f;
}
/************************************************************************/
void MyCommand::DriveMotion(int *Steps, int Dir) {

  int NS = 10;
  uint32_t _Steps = *Steps;
  uint8_t _Dir = Dir;
  m_DataS[2] = CommadID_DriveSteps;
  DwordToByte(&_Steps, &m_DataS[5], &m_DataS[6], &m_DataS[7]);
  m_DataS[8] = _Dir;
  WriteCommand(m_DataS, NS);
}
/************************************************************************/
bool MyCommand::DeviceConectedToBox(void) {
  bool Res = false;
  BYTE SW[4];
  BYTE HW[4];
  BYTE Name[4];
  ReadVersion(Name, HW, SW);
  std::string _Name = (char *)Name;
  if (_Name.compare("MS") == 0) {
    Res = true;
  }
  return Res;
}
/************************************************************************/
void MyCommand::ReadAbsolutePostion(int *Postion) {
  int NS = 5;
  int NR = 10;
  m_DataS[2] = CommadID_READ_DRIVER_ABS_POS;
  ReadCommand(m_DataS, NS, m_DataR, NR);
  ByteToInt(Postion, &m_DataR[5], &m_DataR[6], &m_DataR[7], &m_DataR[8],
            &m_DataR[9]);
};
/************************************************************************/
void MyCommand::BreakMosuleCommand(void) {

  int NS = 5;
  m_DataS[2] = CommadID_BraekModule;
  WriteCommand(m_DataS, NS);
}

/************************************************************************/
void MyCommand::IntToByte(int *Source, uint8_t *B1, uint8_t *B2, uint8_t *B3,
                          uint8_t *B4, uint8_t *B5) {
  *B1 = *Source & 0x7f;
  *B2 = (*Source >> 7) & 0x7f;
  *B3 = (*Source >> 14) & 0x7f;
  *B4 = (*Source >> 21) & 0x7f;
  *B5 = (*Source >> 28) & 0x7f;
}
/************************************************************************/
void MyCommand::ByteToInt(int *Dest, uint8_t *B1, uint8_t *B2, uint8_t *B3,
                          uint8_t *B4, uint8_t *B5) {
  *Dest = *B1 | *B2 << 7 | *B3 << 14 | *B4 << 21 | *B5 << 28;
}
/************************************************************************/
