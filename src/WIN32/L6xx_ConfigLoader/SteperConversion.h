#include "SteperDefinition.h"
#include <stdint.h>
#pragma once
#ifndef __SCONV__
#define __SCONV__

void ConvSPIN_RegsStruct_TypeDefToByte(cSPIN_RegsStruct_TypeDef *Source,
                                       uint8_t *Dest);
void ConvcSPIN_ByteToRegsStruct_TypeDef(cSPIN_RegsStruct_TypeDef *Source,
                                        uint8_t *Dest);

void ByteTo7Bit(uint8_t *Source, uint8_t *Dest1, uint8_t *Dest2);
void Bit7ToByte(uint8_t *Source, uint8_t *Dest1, uint8_t *Dest2);
void ByteToDword(uint32_t *Destination, uint8_t *Source1, uint8_t *Source2,
                 uint8_t *Source3);
void DwordToByte(uint32_t *Source, uint8_t *B1, uint8_t *B2, uint8_t *B3);
#endif
