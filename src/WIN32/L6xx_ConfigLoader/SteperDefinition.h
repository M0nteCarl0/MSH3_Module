#include <stdint.h>
#pragma once
typedef struct cSPIN_RegsStruct_TypeDef {
  uint32_t ABS_POS;   // 4(44);
  uint16_t EL_POS;    // 4/(40)
  uint32_t MARK;      // 4
  uint32_t SPEED;     // 4
  uint16_t ACC;       // 2(28)
  uint16_t DEC;       // 2
  uint16_t MAX_SPEED; // 2
  uint16_t MIN_SPEED; // 2
  uint16_t FS_SPD;    // 2(21)
  uint8_t TVAL_HOLD;  // 1
  uint8_t TVAL_RUN;   // 1
  uint8_t TVAL_ACC;   // 1
  uint8_t TVAL_DEC;   // 1(16)
  uint16_t RESERVED_3;
  uint8_t T_FAST;     // 1
  uint8_t TON_MIN;    // 1
  uint8_t TOFF_MIN;   // 1
  uint8_t RESERVED_2; // 1(11)
  uint8_t ADC_OUT;    // 1
  uint8_t OCD_TH;     // 1
  uint8_t RESERVED_1; // 0
  uint8_t STEP_MODE;  // 1
  uint8_t ALARM_EN;   // 1(7)
  uint16_t GATECFG1;  // 2
  uint8_t GATECFG2;   // 1
  uint16_t CONFIG;    // 2
  uint16_t STATUS;    // 2
  uint8_t LookAtSwitch;
  uint8_t SensorOffset;
} cSPIN_RegsStruct_TypeDef;
