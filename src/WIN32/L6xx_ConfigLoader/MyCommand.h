//----------------------------------------------------------------------------
//    Create by Novikov
//    data 25.12.2012
//----------------------------------------------------------------------------
#ifndef __MYCOMMAND_H__
#define __MYCOMMAND_H__

//#include "stdafx.h"
#include "SteperDefinition.h"
#include "mxcomm.h"
#include <stdint.h>

/*!
 * \brief ����� ��� ������ � ���������
 * \details
 * \author ���������� ���������
 * \version 0.1
 * \date 2.09.2015
 */

#define NCOM 128
#define mID 0x96
//#define mADDR 0x07

/********************************************************************************/
 typedef enum  MSH_Core_module_ID_Number
{
  MSH_Core_module_ID_Number_Scaner     = 0xA,
  MSH_Core_module_ID_Number_Collimator = 0xB,
}MSH_Core_module_ID_Number;




enum CommadID {
  CommadID_READ_HW_SW = 0x40,
  CommadID_WRITE_DRIVER_SETUP = 0x1,
  CommadID_REBOOT_MODULE = 0x2,
  CommadID_DriveSteps = 0x3,
  CommadID_BraekModule = 0x4,
  CommadID_READ_DRIVER_SETUP = 0x41,
  CommadID_READ_DRIVER_ABS_POS = 0x44,

};
/********************************************************************************/
enum CommadID_Two_Byte {
  CommadID_Two_Byte_READ = 0x81,
  CommadID_Two_Byte_TERMINAL = 0xd7,
  CommadID_Two_Byte_Status = 0xd6,
  CommadID_Two_Byte_StepsHi = 0xd8,
  CommadID_Two_Byte_StepsLo = 0xd9,
  CommadID_Two_Byte_VelocityHi = 0xda,
  CommadID_Two_Byte_VelocityLo = 0xdb,
  CommadID_Two_Byte_RampHi = 0xdc,
  CommadID_Two_Byte_RampLo = 0xdd,
  CommadID_Two_Byte_CounterHi = 0xde,
  CommadID_Two_Byte_CounterLo = 0xdf,
  CommadID_Two_Byte_InitialVLo = 0xbd,
  CommadID_Two_Byte_TimeDelayLo = 0xbe,
  CommadID_Two_Byte_TurnOffHVLo = 0xb8,
};

/********************************************************************************/
class MyCommand {
public:
  bool m_FlgDebugS;
  bool m_FlgDebugR;
  bool m_FlgCheckCRCwrite;
  bool m_FlgCheckCRCread;

  MxComm m_Comm;

  BYTE m_DataS[NCOM];
  BYTE m_DataR[NCOM];
  bool FTDIConected;
  int DeviceID;
  MyCommand();
  virtual ~MyCommand();

  void InitComm();

  BYTE MakeCRC(BYTE *Data, int N);
  void AddCRC(BYTE *Data, int N);
  bool CheckCRC(BYTE *Data, int N);

  int CheckErrorWR(BYTE *DataS, int NS, BYTE *DataR, int NR);
  int WriteCommand(BYTE *DataS, int NS);
  int WriteLegacy(BYTE *DataS, int NS);
  int ReadCommand(BYTE *DataS, int NS, BYTE *DataR, int NR);
  int ReadLegacy(BYTE *DataS, int NS);

  int ReadVersion(BYTE *Name, BYTE *Hard, BYTE *Soft);
  void WriteDriverConfig(cSPIN_RegsStruct_TypeDef Config);
  void ReadDriverConfig(cSPIN_RegsStruct_TypeDef *Config);
  void ResetModule(void);
  void DriveMotion(int *Steps, int Dir);
  bool IsFTDIConected(void);
  bool DeviceConectedToBox(void);
  void ReadAbsolutePostion(int *Postion);
  void ConvSPIN_RegsStruct_TypeDefToByte(cSPIN_RegsStruct_TypeDef *Source,
                                         uint8_t *Dest);
  void ConvcSPIN_ByteToRegsStruct_TypeDef(cSPIN_RegsStruct_TypeDef *Source,
                                          uint8_t *Dest);
  void ByteTo7Bit(uint8_t *Source, uint8_t *Dest1, uint8_t *Dest2);
  void Bit7ToByte(uint8_t *Source, uint8_t *Dest1, uint8_t *Dest2);
  void ByteToDword(uint32_t *Destination, uint8_t *Source1, uint8_t *Source2,
                   uint8_t *Source3);
  void DwordToByte(uint32_t *Source, uint8_t *B1, uint8_t *B2, uint8_t *B3);
  void IntToByte(int *Source, uint8_t *B1, uint8_t *B2, uint8_t *B3,
                 uint8_t *B4, uint8_t *B5);
  void ByteToInt(int *Dest, uint8_t *B1, uint8_t *B2, uint8_t *B3, uint8_t *B4,
                 uint8_t *B5);
  void BreakMosuleCommand(void);
};
//----------------------------------------------------------------------------
#endif
