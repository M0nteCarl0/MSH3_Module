#include <stdint.h>
#pragma once
#ifndef Conversion_HELPER
#define Conversion_HELPER
namespace Conversion
{
   void WordToByte(uint16_t &Source,uint8_t &Dest1, uint8_t &Dest2);
   void ByteToWord(uint16_t &Source,uint8_t &Dest1, uint8_t &Dest2);
   void QWordToByte(uint64_t &Source,uint8_t &Dest1, uint8_t &Dest2,uint8_t& Dest3,uint8_t& Dest4, uint8_t &Dest5, uint8_t &Dest6,uint8_t& Dest7,uint8_t& Dest8);
   void ByteToQWord(uint64_t &Source,uint8_t &Dest1, uint8_t &Dest2,uint8_t& Dest3,uint8_t& Dest4, uint8_t &Dest5, uint8_t &Dest6,uint8_t& Dest7,uint8_t& Dest8);
   void DWordToByte(uint32_t &Source,uint8_t &Dest1, uint8_t &Dest2,uint8_t& Dest3,uint8_t& Dest4);
   void ByteToDWord(uint32_t &Source,uint8_t &Dest1, uint8_t &Dest2,uint8_t& Dest3,uint8_t& Dest4);
   void ByteTo7Bit(uint8_t &Source,uint8_t &Dest1,uint8_t &Dest2);
   void Bit7ToByte(uint8_t &Source,uint8_t &Dest1,uint8_t &Dest2);
   void WordTo7Bit(uint16_t &Source,uint8_t &Dest1,uint8_t &Dest2,uint8_t &Dest3,uint8_t &Dest4);
   void Bit7ToWord(uint16_t &Source,uint8_t &Dest1,uint8_t &Dest2,uint8_t &Dest3,uint8_t &Dest4);
   void DWordTo7Bit(uint32_t &Source,uint8_t &Dest1, uint8_t &Dest2,uint8_t& Dest3,uint8_t& Dest4,uint8_t &Dest5, uint8_t &Dest6,uint8_t& Dest7,uint8_t& Dest8);
   void Bit7ToDWord(uint32_t &Source,uint8_t &Dest1, uint8_t &Dest2,uint8_t& Dest3,uint8_t& Dest4,uint8_t &Dest5, uint8_t &Dest6,uint8_t& Dest7,uint8_t& Dest8);
   void QWordTo7Bit(uint64_t &Source,uint8_t &Dest1, uint8_t &Dest2,uint8_t& Dest3,uint8_t& Dest4,uint8_t &Dest5, uint8_t &Dest6,uint8_t& Dest7,uint8_t& Dest8,uint8_t &Dest9, uint8_t &Dest10,uint8_t& Dest11,uint8_t& Dest12,uint8_t &Dest13, uint8_t &Dest14,uint8_t& Dest15,uint8_t& Dest16);
   void Bit7ToQWord(uint64_t &Source,uint8_t &Dest1, uint8_t &Dest2,uint8_t& Dest3,uint8_t& Dest4,uint8_t &Dest5, uint8_t &Dest6,uint8_t& Dest7,uint8_t& Dest8,uint8_t &Dest9, uint8_t &Dest10,uint8_t& Dest11,uint8_t& Dest12,uint8_t &Dest13, uint8_t &Dest14,uint8_t& Dest15,uint8_t& Dest16);
   void FloatTo7Bit(float &Source,uint8_t &Dest1, uint8_t &Dest2,uint8_t& Dest3,uint8_t& Dest4,uint8_t &Dest5, uint8_t &Dest6,uint8_t& Dest7,uint8_t& Dest8);
   void Bit7ToFloat(float &Source,uint8_t &Dest1, uint8_t &Dest2,uint8_t& Dest3,uint8_t& Dest4,uint8_t &Dest5, uint8_t &Dest6,uint8_t& Dest7,uint8_t& Dest8);
   void FAST_WordToByte(uint16_t &Source,uint8_t &Dest1, uint8_t &Dest2);
   void FAST_GenericTo7Bit(uint64_t &Source,uint8_t* Buff,uint8_t N);
   void FAST_ByteToWord(uint16_t &Dest,uint8_t &Source1, uint8_t &Source2);
   void FAST_7BitToZGeneric(uint64_t &Dest,uint8_t* Source,uint8_t N);
};
#endif