#pragma once
#include <fstream>
#include <iostream>
#include <stdint.h>
using namespace std;
#ifndef _CONFIG_PARCE
#define _CONFIG_PARCE
//
//  autor: Alexander Molotaliev, molotaliev@roentgenprom.ru
//
//  date: 9.10.2015
//
class ConfigParcer {
public:
  ConfigParcer(void);
  ConfigParcer(const char *FileName);
  bool Open(const char *Filename);
  ~ConfigParcer(void);
  void SetDelimetrToken(const char *DelimToken);
  void SetIntParam(const char *ParamName, int &Value);
  void SetFloatParam(const char *ParamName, float &Value);
  void SetDoubleParam(const char *ParamName, double &Value);
  void SetBoolParam(const char *ParamName, bool &Value);
  void GetIntParam(const char *ParamName, int &Value);
  void GetFloatParam(const char *ParamName, float &Value);
  void GetDoubleParam(const char *ParamName, double &Value);
  void GetBoolParam(const char *ParamName, bool &Value);
  void Getuint16_tParam(const char *ParamName, uint16_t &Value);
  void GetIntParamHex(const char *ParamName, int &Value);
  bool GetIsConfigFileOpen(void);

private:
  template <typename T> void SetTemplateParam(const char *ParamName, T &Value);
  template <typename T> void GetTemplateParam(const char *ParamName, T &Value);
  template <typename T>
  void GetTemplateParamHex(const char *ParamName, T &Value);
  fstream _File;
  string _Token;
  string _Delimetr;
  bool _IsConfigFileOpen;
  string _ConfigFileNamel;
};
#endif
